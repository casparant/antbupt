#ifndef _MONK_H_
#define _MONK_H_

/**
 * color definition
 * \033[1;40;34m %s \033[0m
 */

#define BUCKET          0
#define POOL_EMPTY      1
#define POOL_FULL       2
#define WELL            3

#define BUCKET_TOTAL    3
#define POOL_TOTAL      10

#endif  /* _MONK_H_ */
