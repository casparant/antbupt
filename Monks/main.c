/*
 * Project: Monks
 * File: main.c
 * Author: Caspar Zhang <casparant@gmail.com>
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/sem.h>
#include "apue.h"
#include "cslib.h"
#include "PV.h"
#include "monk.h"

/*
 * Function: static void sig_monk(int)
 * Return: void
 * Param: int
 *
 * The handling function of SIGUSR1 and SIGUSR2
 */
static void sig_monk(int signo);

int monk_fetch_nr, monk_fill_nr;

int main()
{
    int             randNum;
    int             semid;
    unsigned short  sem_arr[4] = {BUCKET_TOTAL, POOL_TOTAL, 0, 1};

    /*
     * Initialize 4 semaphores :
     * 0 - BUCKET ; init = 3
     * 1 - POOL_EMPTY ; init = 10
     * 2 - POOL_FULL ; init = 0
     * 3 - WELL ; init = 1(mutex)
     */
    if ((semid = semget(SEM_KEY, 4, 0666|IPC_CREAT)) < 0)
    {
        err_sys("semget error");
    }
    if (semctl(semid, 3, SETALL, sem_arr) < 0)
    {
        err_sys("semctl error");
    }

    /* Initalize random seed */
    Randomize();
    while (1)
    {
        /* Set signal handling function */
        if (signal(SIGUSR1, sig_monk) == SIG_ERR)
        {
            err_sys("can't catch SIGUSR1");
        }
        if (signal(SIGUSR2, sig_monk) == SIG_ERR)
        {
            err_sys("can't catch SIGUSR2");
        }
        if (signal(SIGCHLD, SIG_IGN) == SIG_ERR) // avoiding zombia
        {
            err_sys("can't catch SIGCHLD");
        }
        /* Send different signal depends on different number */
        if ((randNum = RandomInteger(1, 2)) == 1)
        {
            monk_fill_nr++;
            if (kill(getpid(), SIGUSR1) == -1)
            {
                err_sys("can't send SIGUSR1");
            }
        }
        else if (randNum == 2)
        {
            monk_fetch_nr++;
            if (kill(getpid(), SIGUSR2) == -1)
            {
                err_sys("can't send SIGUSR2");
            }
        }
        else
        {
            err_sys("unexpected random number");
        }
        sleep(1);
    }
}

static void sig_monk(int signo)
{
    int pid;
    char arg[3];

//    printf("\033[0;40;34mreceived %s\n\033[0m", strsignal(signo));
    if ((pid = fork()) < 0)
    {
        err_sys("fork error");
    }
    else if (pid == 0) // child
    {
        if (signo == SIGUSR1)
        {
            sprintf(arg, "%d", monk_fill_nr);
            if (execlp("./monk_fill", "monk_fill", arg, (char *)0) < 0)
            {
                err_sys("execlp monk_fill error");
            }
        }
        else if (signo == SIGUSR2)
        {
            sprintf(arg, "%d", monk_fetch_nr);
            if (execlp("./monk_fetch", "monk_fetch", arg, (char *)0) < 0)
            {
                err_sys("execlp monk_fetch error");
            }
        }
    }
    /* parent */
}
