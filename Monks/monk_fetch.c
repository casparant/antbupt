/*
 * Project: Monks
 * File: monk_fetch.c
 * Author: Caspar Zhang <casparant@gmail.com>
 */
#include "PV.h"
#include "apue.h"
#include "monk.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/sem.h>

int main(int argc, char *argv[])
{
    int semid;
    int id, p, b;

    if (argc != 2)
    {
        err_exit(-1, "app must be followed with a parameter");
    }
    id = atoi(argv[1]);

    if ((semid = semget(SEM_KEY, 0, 0)) < 0)
    {
        err_sys("semget error");
    }

    if ((p = semctl(semid, POOL_FULL, GETVAL, 0)) < 0 ||
            (b = semctl(semid, BUCKET, GETVAL, 0)) < 0)
    {
        err_sys("semctl error");
    }
    printf("\033[1;40;34m Drinker%-3d: \033[1;40;31mP-%-2d|B-%-2d| \033[0;40;34mP(POOL_FULL) \033[0m | See the pool is empty or not.\n", id, p, b);
    if (P(semid, POOL_FULL) < 0)
    {
        err_sys("P(POOL_FULL) error");
    }

    if ((p = semctl(semid, POOL_FULL, GETVAL, 0)) < 0 ||
            (b = semctl(semid, BUCKET, GETVAL, 0)) < 0)
    {
        err_sys("semctl error");
    }
    printf("\033[1;40;34m Drinker%-3d: \033[1;40;31mP-%-2d|B-%-2d| \033[0;40;34mP(BUCKET)    \033[0m | Try to get a bucket.\n", id, p, b);
    if (P(semid, BUCKET) < 0)
    {
        err_sys("P(BUCKET) error");
    }

    if ((p = semctl(semid, POOL_FULL, GETVAL, 0)) < 0 ||
        (b = semctl(semid, BUCKET, GETVAL, 0)) < 0)
    {
        err_sys("semctl error");
    }
    printf("\033[1;40;34m Drinker%-3d: \033[1;40;31mP-%-2d|B-%-2d| \033[0;40;34mV(POOL_EMPTY)\033[0m | Drinking Water.\n", id, p, b);
    sleep(2);
    if (V(semid, POOL_EMPTY) < 0)
    {
        err_sys("V(POOL_EMPTY) error");
    }

    if ((p = semctl(semid, POOL_FULL, GETVAL, 0)) < 0 ||
        (b = semctl(semid, BUCKET, GETVAL, 0)) < 0)
    {
        err_sys("semctl error");
    }
    printf("\033[1;40;34m Drinker%-3d: \033[1;40;31mP-%-2d|B-%-2d| \033[0;40;34mV(BUCKET)    \033[0m | Return the bucket\n", id, p, b);
    if (V(semid, BUCKET) < 0)
    {
        err_sys("V(BUCKET) error");
    }

    return 0;
}
