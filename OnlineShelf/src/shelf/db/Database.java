/**
 * Author: Caspar Zhang <casparant@gmail.com>
 */

package shelf.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Collection;
import shelf.bean.Book;

public class Database
{
	Connection			con;
	Statement			st;
	String				sql;
	ResultSet			rs;
	private Collection	books;

	public Database()
	{
		books = new ArrayList();
	}

	private java.sql.Connection getConnection() throws ClassNotFoundException,
			SQLException
	{
		String url = "jdbc:mysql://localhost:3306/shelf";
		Class.forName("com.mysql.jdbc.Driver");
		con = DriverManager.getConnection(url, "study", "study");
		return con;
	}

	public Collection getAllBooks() throws SQLException,	ClassNotFoundException
	{
		con = this.getConnection();
		st = con.createStatement();
		sql = "SELECT * FROM books;";
		rs = st.executeQuery(sql);

		while (rs.next())
		{
			books.add(new Book(rs.getLong("id"), rs.getString("author"), rs
					.getString("title"), (rs.getInt("available") == 1) ? true
					: false));
		}
		rs.close();
		st.close();
		return books;
	}

	public long updateBook(Book book) throws SQLException, ClassNotFoundException
	{
		con = this.getConnection();
		st = con.createStatement();
		sql = "UPDATE books SET author = '" + book.getAuthor() + "', title = '"
				+ book.getTitle() + "', available = "
				+ (book.isAvailable() ? 1 : 0) + " WHERE id = " + book.getId()
				+ ";";
		st.executeUpdate(sql);
		st.close();
		con.close();

		return book.getId();
	}

	public void deleteBook(Book book) throws SQLException, ClassNotFoundException
	{
		con = this.getConnection();
		st = con.createStatement();
		sql = "DELETE FROM books WHERE id = " + book.getId();
		st.executeUpdate(sql);
		st.close();
		con.close();
	}

	public void addBook(Book book) throws SQLException, ClassNotFoundException
	{
		con = this.getConnection();
		st = con.createStatement();
		sql = "INSERT INTO books (author, title, available) VALUES ('"
				+ book.getAuthor() + "', '" + book.getTitle() + "', "
				+ (book.isAvailable() ? 1 : 0) + ");";
		st.execute(sql);
		st.close();
		con.close();
	}

	public Book loadBook(Book book) throws SQLException, ClassNotFoundException
	{
		Book bk = new Book();
		con = this.getConnection();
		st = con.createStatement();
		sql = "SELECT * FROM books WHERE id = " + book.getId();
		rs = st.executeQuery(sql);
		if (rs.next())
		{
			bk.setId(rs.getLong("id"));
			bk.setAuthor(rs.getString("author"));
			bk.setTitle(rs.getString("title"));
			bk.setAvailable((rs.getInt("available") == 1) ? true : false);
		}
		st.close();
		con.close();
		return bk;
	}

}
