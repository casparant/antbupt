/*
 * Generated by MyEclipse Struts
 * Template path: templates/java/JavaClass.vtl
 * Author: Caspar Zhang <casparant@gmail.com>
 * Modified From Original Author: Sascha Wolski
 */
package shelf.struts.action;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import shelf.db.Database;
import shelf.struts.form.BookEditForm;

/**
 * MyEclipse Struts Creation date: 05-24-2009
 * 
 * XDoclet definition:
 * 
 * @struts.action path="/bookEdit" name="bookEditForm" parameter="do"
 *                scope="request" validate="true"
 * @struts.action-forward name="showEdit" path="/bookEdit.jsp"
 * @struts.action-forward name="showList" path="/bookList.do" redirect="true"
 * @struts.action-forward name="showAdd" path="/bookAdd.jsp"
 */
public class BookEditAction extends DispatchAction
{
	public ActionForward newBook(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	{
		return mapping.findForward("showAdd");
	}

	public ActionForward addBook(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	{
		BookEditForm bookEditForm = (BookEditForm) form;
		Database db = new Database();

		try
		{
			db.addBook(bookEditForm.getBook());
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}

		return mapping.findForward("showList");
	}

	public ActionForward editBook(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	{
		BookEditForm bookEditForm = (BookEditForm) form;
		Database db = new Database();

		try
		{
			bookEditForm.setBook(db.loadBook(bookEditForm.getBook()));
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}

		return mapping.findForward("showEdit");
	}

	public ActionForward updateBook(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	{
		BookEditForm bookEditForm = (BookEditForm) form;
		Database db = new Database();

		try
		{
			db.updateBook(bookEditForm.getBook());
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}

		return mapping.findForward("showList");
	}

	public ActionForward deleteBook(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	{
		BookEditForm bookEditForm = (BookEditForm) form;
		Database db = new Database();

		try
		{
			db.deleteBook(bookEditForm.getBook());
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}

		return mapping.findForward("showList");
	}
}
