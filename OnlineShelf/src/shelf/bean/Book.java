/**
 * Author: Caspar Zhang <casparant@gmail.com>
 * Modified From Original Author: Sascha Wolski
 */
package shelf.bean;

import java.io.Serializable;

public class Book implements Serializable
{
	private static final long	serialVersionUID	= 1L;
	private long				id;
	private String				title;
	private String				author;
	private boolean				available;

	public Book()
	{
	}

	public Book(long id, String author, String title, boolean available)
	{
		this.id = id;
		this.author = author;
		this.title = title;
		this.available = available;
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getAuthor()
	{
		return author;
	}

	public void setAuthor(String author)
	{
		this.author = author;
	}

	public boolean isAvailable()
	{
		return available;
	}

	public void setAvailable(boolean available)
	{
		this.available = available;
	}

}
