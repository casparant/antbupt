<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>

<html>
	<head><title>书架</title></head>
	
	<body>
		<h2 align="center">书架</h2>
		<table border="1" align="center"><tbody>
			<%-- set the header --%>
			<tr>
				<td>书籍作者</td>
				<td>书籍名称</td>
				<td>在架可借</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<%-- check if book exists and display message or iterate over books --%>
			<logic:empty name="bookListForm" property="books">
			<tr>
				<td colspan="5">哇靠，Ant没书了T_T</td>
			</tr>
			</logic:empty>
			
			<logic:notEmpty name="bookListForm" property="books">
			<logic:iterate name="bookListForm" property="books" id="book">
			<tr>
				<%-- print out the book informations --%>
				<td><bean:write name="book" property="author" /></td>
				<td><bean:write name="book" property="title" /></td>
				<td><html:checkbox disabled="true" name="book" property="available" /></td>
				<%-- print out the edit and delete link for each book --%>
				<td><html:link action="bookEdit.do?do=editBook" paramName="book" paramProperty="id" paramId="id">修改本书</html:link></td>
				<td><html:link action="bookEdit.do?do=deleteBook" paramName="book" paramProperty="id" paramId="id">删除本书</html:link></td>
			</tr>
			</logic:iterate>
			</logic:notEmpty>
			<%-- print out the add link --%>
			<tr>
				<td colspan="5"><html:link action="bookEdit.do?do=newBook">添加一本新书</html:link></td>
			</tr>
			<%-- end interate --%>
		</tbody></table>
	</body>
</html>