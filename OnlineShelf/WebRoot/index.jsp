<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    <title>欢迎光临Ant的私人书架</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="library,online,struts">
	<meta http-equiv="description" content="This is a personal book shelf written based on struts 1.1 framework">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
  </head>
  
  <body>
  	<div align="center">
  		<p><h2>欢迎光临Ant的书架</h2></p>
    	<html:link action="bookList">点击查看Ant的书架</html:link>
   	</div>
   	<p><h3>简介</h3></p>
   	本图书馆系统为Ant的书架，开发人员：Caspar Zhang, Amelie Lee, Fan Xie.<br />
   	本系统在Struts1.1框架下开发，开发系统为Gentoo Linux + MyEclipse 7.1<br />
   	感谢Sascha Wolski提供系统原型，本系统在此基础上重构，并连接了数据库，实现了数据的持久化，
   	下一步可以考虑使用Hibernate框架。<br />
   	本系统目前实现的功能有：<br />
   	1. 列出Ant图书馆当前拥有的书籍以及书籍的状态<br />
   	2. 修改书籍状态（如借入，借出，丢了等）<br />
   	3. 删除书籍（二手书不想要了处理掉）<br />
   	4. 增加书籍（新购书籍）<br />
   	<p align="center">Copyright (C) 2009 by Caspar Zhang < casparant@gmail.com > </p>
  </body>
</html>
