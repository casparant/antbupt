<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<html>
	<head>
		<title>修改</title>
	</head>
	<body>
		<h2>修改</h2>
		<%-- create a html form --%>
		<html:form action="bookEdit">
			<%-- print out the form data --%>
			<table border="1">
				<tr>
					<td>作者：</td>
					<td><html:text property="author" /></td>
				</tr>
				<tr>
					<td>书名：</td>
					<td><html:text property="title" /></td>
				</tr>
				<tr>
					<td>可借：</td>
					<td><html:checkbox property="available" /></td>
				</tr>
				<tr>
					<td colspan="2"><html:submit>保存</html:submit></td>
				</tr>
			</table>
			<%-- hidden field that contains the id of the book --%>
			<html:hidden property="id" />
			<%-- set the parameter for the dispatch action --%>
			<html:hidden property="do" value="updateBook" />
		</html:form>
	</body>
</html>