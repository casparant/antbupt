#!/bin/bash
echo "------------ stub : add ------------"

echo -n "Host Name[localhost]: "
read hostname
if [ -z $hostname ]; then
    hostname="localhost"
fi

echo -n "Host Port[1099]: "
read portnum
if [ -z $portnum ]; then
    portnum="1099"
fi

while [ -z $username ]
do
    echo -n "Username: "
    read username
done

while [ -z $password ]
do
    echo -n "Password: "
    read password
done

while [ -z $otherside ]
do
    echo -n "Other Side Username: "
    read otherside
done

while [ -z $startTime ]
do
    echo -n "Start Time(Format: yyyy-MM-dd,hh-mm): "
    read startTime
done

while [ -z $endTime ]
do
    echo -n "End Time(Format: yyyy-MM-dd,hh-mm): "
    read endTime
done

while [ -z $title ]
do
    echo -n "One Line, without Space for Title: "
    read "title"
done

java -Djava.security.policy=java.policy agenda.client.AgendaClient $hostname $portnum add $username $password $otherside $startTime $endTime $title
