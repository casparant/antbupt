/**
 * 
 * Copyright(C) 2009 by Caspar Zhang <casparant@gmail.com>
 * 
 * @filename AgendaInterface.java
 * Modified from the sample HelloInterface.java of M. L. Liu
 * by Caspar Zhang <casparant@gmail.com>
 */
package agenda.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Vector;

import agenda.datastruct.AgendaDataStruct;

/**
 * This is a remote interface.
 * @author M. L. Liu
 * @modifier Caspar Zhang
 */
public interface AgendaInterface extends Remote 
{
    /**
     * This remote method returns a boolean value which stands for 
     * register succeeded or failed.
     * 
     * @return a boolean value.
     * @params String name - username passed in
     * @params String pwd  - password passwd in
     *
     */
    public boolean register(String name, String pwd) throws RemoteException;
    public int login(String name, String pwd)  throws RemoteException;
    public int add(String name, String other, long start, long end, String title) throws RemoteException;
    public Vector<AgendaDataStruct> query(String name, long start, long end) throws RemoteException;
    public boolean delete(String name, int id) throws RemoteException;
    public int clear(String name) throws RemoteException;
}
