/**
 *
 * Copyright(C) 2009 by Caspar Zhang <casparant@gmail.com>
 *
 * @filename AgendaImpl.java
 * Modified from the sample HelloImpl.java of M. L. Liu
 * by Caspar Zhang <casparant@gmail.com>
 */

package agenda.interfaces;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Vector;

import agenda.datastruct.AgendaDataStruct;

/**
 * This class implements the remote interface AgendaInterface.
 * @author M. L. Liu
 * @modifier Caspar Zhang
 */
@SuppressWarnings("serial")
public class AgendaImpl extends UnicastRemoteObject implements AgendaInterface
{  
    private HashMap<String, String> users = new HashMap<String, String>();
    private Vector<AgendaDataStruct> agenda = new Vector<AgendaDataStruct>();
    
    public AgendaImpl() throws RemoteException 
    {
        super();
    }

    /**
     * TODO The password is transferred in plaintext, it's not safe!
     */
    public boolean register(String name, String pwd) throws RemoteException 
    {
        String password = (String)users.get(name);
        if (password == null)
        {
            users.put(name, pwd);
            return true;
        }
        else
        {
            return false;
        }
    }

    public int login(String name, String pwd) throws RemoteException 
    {
    	String password = (String)users.get(name);
    	if (password == null)
    	{
    		return -1; 
    	}
    	else if (!pwd.equals(password))
    	{
    		return 0;
    	}
    	return 1;
    }
    
    public int add(String name, String other, long start, long end, String title) throws RemoteException
    {
        /* if the other side not registered yet. */
        if (users.get(other) == null)
        {
            return -2;
        }
    	for (AgendaDataStruct adstmp : agenda)
		{
    		if ((name.equals(adstmp.username) || name.equals(adstmp.otherside)) &&
    				(start <= adstmp.endTime && end >= adstmp.startTime))
    		{
    			return -1;
    		}	
		}
        AgendaDataStruct ads = new AgendaDataStruct();
    	ads.meetingid   = agenda.size()+1;
        ads.username    = name;
        ads.otherside   = other;
        ads.startTime   = start;
        ads.endTime     = end;
        ads.title       = title;
		agenda.add(ads);
    	return agenda.size();
    }
    
    public Vector<AgendaDataStruct> query(String name, long start, long end) throws RemoteException
    {
    	Vector<AgendaDataStruct> adsv = new Vector<AgendaDataStruct>();
    	for (AgendaDataStruct adstmp : agenda)
    	{
    		if ((name.equals(adstmp.username) || name.equals(adstmp.otherside)) && 
    				!(adstmp.startTime > end || adstmp.endTime < start))
    		{
    			adsv.add(adstmp);
    		}
    	}
    	return adsv;
    }
    
    public boolean delete(String name, int id) throws RemoteException
    {
        if (id > agenda.size())
        {
            return false;
        }
    	if (agenda.elementAt(id-1).username.equals(name) || agenda.elementAt(id-1).otherside.equals(name))
    	{
    		agenda.remove(id-1);
    		return true;
    	}
    	return false;
    }
    
    public int clear(String name) throws RemoteException
    {
    	int count = 0;
    	
    	for (int i = 0; i < agenda.size(); ++i)
    	{
    		AgendaDataStruct adstmp = agenda.elementAt(i);
    		if (name.equals(adstmp.username) || name.equals(adstmp.otherside))
    		{
    			agenda.remove(i);
    			count ++;
    		}
    	}
    	return count;
    }
}
