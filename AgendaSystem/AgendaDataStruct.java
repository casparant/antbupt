/**
 * Copyright(C) 2009 by Caspar Zhang <casparant@gmail.com>
 */
package agenda.datastruct;

import java.io.Serializable;

public class AgendaDataStruct implements Serializable
{
	private static final long serialVersionUID = -8194206844200139987L;
	public int meetingid = 0;
	public String username;
	public String otherside;
	public long startTime;
	public long endTime;
	public String title;
}
