#!/bin/bash
echo "------------ stub : query ------------"

echo -n "Host Name[localhost]: "
read hostname
if [ -z $hostname ]; then
    hostname="localhost"
fi

echo -n "Host Port[1099]: "
read portnum
if [ -z $portnum ]; then
    portnum="1099"
fi

while [ -z $username ]
do
    echo -n "Username: "
    read username
done

while [ -z $password ]
do
    echo -n "Password: "
    read password
done

while [ -z $startRange ]
do
    echo -n "Start Time Range(Format: yyyy-MM-dd,hh:mm): "
    read startRange
done

while [ -z $endRange ]
do
    echo -n "End Time Range(Format: yyyy-MM-dd,hh:mm): "
    read endRange
done

java -Djava.security.policy=java.policy agenda.client.AgendaClient $hostname $portnum query $username $password $startRange $endRange
