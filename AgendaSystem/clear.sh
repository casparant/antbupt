#!/bin/bash
echo "------------ stub : clear ------------"

echo -n "Host Name[localhost]: "
read hostname
if [ -z $hostname ]; then
    hostname="localhost"
fi

echo -n "Host Port[1099]: "
read portnum
if [ -z $portnum ]; then
    portnum="1099"
fi

while [ -z $username ]
do
    echo -n "Username: "
    read username
done

while [ -z $password ]
do
    echo -n "Password: "
    read password
done

java -Djava.security.policy=java.policy agenda.client.AgendaClient $hostname $portnum clear $username $password
