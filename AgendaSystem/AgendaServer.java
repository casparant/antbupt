/**
 * 
 * Copyright(C) 2009 by Caspar Zhang <casparant@gmail.com>
 * 
 * @filename AgendaServer.java
 * Modified from the sample AgendaServer.java of M. L. Liu
 * by Caspar Zhang <casparant@gmail.com>
 */
package agenda.server;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.net.InetAddress;  
import java.net.UnknownHostException;  
import agenda.interfaces.AgendaImpl;

/**
 * This class represents the object server for a distributed
 * object of class Agenda, which implements the remote 
 * interface AgendaInterface.  A security manager is
 * installed to safeguard stub downloading.
 * @author M. L. Liu
 * @modifier Caspar Zhang
 */
public class AgendaServer
{
    public static void main(String args[])
    {
        if (args.length != 2)
        {
            System.err.println("Usage: java AgendaServer <hostIP> <portNum>");
            System.exit(-1);
        }
        System.setProperty("java.rmi.server.hostname", args[0]);
        String portNum = args[1], registryURL;
        try
        {     
            int RMIPortNum = Integer.parseInt(portNum);
            
            /* start a security manager - this is needed if
             * stub downloading is in use for this application. */
            System.setSecurityManager(new RMISecurityManager());

            startRegistry(RMIPortNum);
            AgendaImpl exportedObj = new AgendaImpl();
            registryURL = "rmi://localhost:" + portNum + "/agenda";
            Naming.rebind(registryURL, exportedObj);
            System.out.println("Server registered. Registry contains:");
            /* list names currently in the registry */
            listRegistry(registryURL);
            System.out.println("Agenda Server ready.");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * This method starts a RMI registry on the local host, if
     * it doesn't already exists at the specified port number.
     */
    private static void startRegistry(int RMIPortNum) throws RemoteException
    {
        try
        {
            Registry registry = LocateRegistry.getRegistry(RMIPortNum);
            /* This call will throw an exception if the registry doesn't already exist*/
            registry.list();
        }
        catch (RemoteException e)
        {
            /* No valid registry at that port. */
            System.out.println("RMI registry cannot be located at port " + RMIPortNum);
            Registry registry = LocateRegistry.createRegistry(RMIPortNum);
            registry.list();
            System.out.println("RMI registry created at port " + RMIPortNum);
        }
    }

    /**
     * This method lists the names registered with a Registry 
     */
    private static void listRegistry(String registryURL)
        throws RemoteException, MalformedURLException
    {
        String[] names = Naming.list(registryURL);
        for (int i = 0; i < names.length; i++)
        {
            System.out.println("\t" + (i+1) + ": rmi:" + names[i]);
        }
    }
}
