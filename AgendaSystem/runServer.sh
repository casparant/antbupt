#!/bin/bash

echo "------------ stub : server ------------"

localip=`ifconfig eth0 | grep 'inet addr:' | cut -b 21-35 | cut -d ' ' -f 1`

echo -n "Enter Host IP[$localip]: "
read ip
if [ -z $ip ]; then
    ip=$localip
fi

echo -n "Enter Port[1099]: "
read portnum
if [ -z $portnum ]; then
    portnum="1099"
fi



pString=`ps | grep -i 'rmiregistry'`

if [ -z '$pString' ]; then
    rmiregistry $portnum &
fi

java -Djava.security.policy=java.policy -Djava.rmi.server.codebase=http://www.csc.calpoly.edu/~mliu/stubs/ agenda.server.AgendaServer $ip $portnum
