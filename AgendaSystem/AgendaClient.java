/**
 * Copyright(C) 2009 by Caspar Zhang <casparant@gmail.com>
 */
package agenda.client;

import java.net.MalformedURLException;
import java.util.Vector;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import agenda.interfaces.AgendaInterface;
import agenda.datastruct.AgendaDataStruct;

public class AgendaClient
{
	private String hostname;
	private String hostport;
	private String operation;
	private String username;
	private String password;
	private String start;
	private String end;
	private String other;
	private String title;
	private int meetingid;
	private AgendaInterface agenda;
	
	public AgendaClient(String args[])
	{
		hostname = new String(args[0]);
		hostport = new String(args[1]);
		operation = new String(args[2]);
		username = new String(args[3]);
		password = new String(args[4]);
		
		if (operation.equals("add"))
		{
			other = new String(args[5]);
			start = new String(args[6]);
			end = new String(args[7]);
			title = new String(args[8]);
		}
		else if (operation.equals("query"))
		{
			start = new String(args[5]);
			end = new String(args[6]);
		}
		else if (operation.equals("delete"))
		{
			meetingid = Integer.parseInt(args[5]);
		}
	}
	
	public void select()
	{
		try
		{
			if (operation.equals("register"))
			{
				register();
			}
			else if (operation.equals("add"))
			{
				add();
			}
			else if (operation.equals("query"))
			{
				query();
			}
			else if (operation.equals("delete"))
			{
				delete();
			}
			else if (operation.equals("clear"))
			{
				clear();
			}
		}
		catch (RemoteException e)
		{
			e.printStackTrace();
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}
	}
	
	public void getOperation()
	{
		
	}
    
    public void rmiProcess() throws RemoteException, MalformedURLException, NotBoundException
    {
    	String registryURL = "rmi://" + hostname + ":" + hostport + "/agenda";

        /* start a security manager - this is needed if stub downloading is in use for this application. */
        System.setSecurityManager(new RMISecurityManager());

        /* find the remote object and cast it to an interface object */
        agenda = (AgendaInterface)Naming.lookup(registryURL);
        System.out.println("Server connected.");
    }
    
    public void printLoginMsg(int n)
    {
    	switch (n)
    	{
    	case -1:
    		System.err.println("ERROR: " + username + " was not a registered user.");
    		break;
    	case 0:
    		System.err.println("ERROR: the password of " + username + " is not correct.");
    		break;
    	case 1:
    		System.out.println("Login succeeded.");
    		break;
    	default:
    		System.err.println("Unexpected result.");
    		break;    	
    	}
    }
	
    public void register() throws RemoteException
    {
		boolean flag = agenda.register(username, password);
		if (flag)
		{
			System.out.println(username + " registered.");
		}
		else
		{
			System.err.println("ERROR: " + username + " already registered.");
		}
    }
    
    public void add() throws RemoteException, NumberFormatException, ParseException
    {
    	int flag = agenda.login(username, password);
    	printLoginMsg(flag);
    	if (flag == 1)
    	{
    		int id = agenda.add(username, other, parse(start), parse(end), title);
    		if (id == -1)
    		{
				System.err.println("ERROR: Agenda adding failed. Conflict with others'.");
    		}
    		else if (id == -2)
    		{
                System.err.println("ERROR: Agenda adding failed. The other person is not a registered user.");
    		}
    		else
    		{
				System.out.println("Agenda added as MeetingID #" + id + ".");
    		}
    	}
    }
    
    public void query() throws RemoteException, ParseException
    {
    	int flag = agenda.login(username, password);
    	printLoginMsg(flag);	
    	if (flag == 1)
    	{
    		Vector<AgendaDataStruct> adsv = agenda.query(username, parse(start), parse(end));
			if (adsv.isEmpty())
			{
				System.err.println("No agenda found during " + start + " to " + end + ".");
			}
			else
			{
				System.out.println("Agenda found:");
                System.out.println("--------------------------------------------------------------------------------");
				System.out.println("   ID|    Sponsor|  Otherside|       StartTime|         EndTime|           Title");
	            System.out.println("--------------------------------------------------------------------------------");
                for (AgendaDataStruct adstmp : adsv)
				{
					System.out.println(String.format("%1$05d|%2$11s|%3$11s|%4$16s|%5$16s|%6$16s",
							adstmp.meetingid, adstmp.username, adstmp.otherside,
							format(adstmp.startTime), format(adstmp.endTime), adstmp.title));
				}
                System.out.println("--------------------------------------------------------------------------------");
			}
    	}
    }
    
    public void delete() throws RemoteException
    {
    	int flag = agenda.login(username, password);
    	printLoginMsg(flag);	
		if (flag == 1)
		{
			boolean succ = agenda.delete(username, meetingid);
			if (succ)
			{
				System.out.println("Agenda #" + meetingid + " deleted.");
			}
			else
			{
				System.err.println("ERROR: Agenda #" + meetingid + " not found.");
			}
		}
    }
    
    public void clear() throws RemoteException
    {
    	int flag = agenda.login(username, password);
    	printLoginMsg(flag);	
		if (flag == 1)
		{
			System.out.println(agenda.clear(username) + " agenda cleared.");
		}
    }
    
    /* Original Format is: yyyy-MM-dd,HH:mm 
     * New Format is a long type, contains yyMMddHHmm */
    public long parse(String time) throws NumberFormatException, ParseException
    {
		return (Long.parseLong(new SimpleDateFormat("yyMMddHHmm").format(
				new SimpleDateFormat("yyyy-MM-dd,HH:mm").parse(time))));
    }
    
    public String format(long time) throws ParseException
    {
		return (new SimpleDateFormat("yyyy-MM-dd,HH:mm").format(
				new SimpleDateFormat("yyyyMMddHHmm").parse(
						String.format("20%1$010d", time))));
    }

	
    public static void printUsage()
    {
    	System.err.println("Usage: java AgendaClient help -- print this help.");
    	System.err.println("       java AgendaClient <host> <port> register <username> <password> -- register a user.");
    	System.err.println("       java AgendaClient <host> <port> add <username> <password> <otherSide> <startTime> <endTime> <title> -- add an agenda");
    	System.err.println("       java AgendaClient <host> <port> query <username> <password> <startRange> <endRange> -- query an agenda");
    	System.err.println("       java AgendaClient <host> <port> delete <username> <password> <meetingId> -- delete an agenda");
    	System.err.println("       java AgendaClient <host> <port> clear <username> <password> -- clear all agenda");
    }
    
    public static void main(String args[])
    {
    	/* if call for help or invalid input */
		if ((args.length == 1 && args[0].equals("help")) ||
				(args.length < 5 || args.length > 9) ||
				(!args[2].equals("register") && !args[2].equals("add") && !args[2].equals("query") &&
						!args[2].equals("delete") && !args[2].equals("clear")))
    	{
    		printUsage();
            System.exit(0);
    	}
		/* if valid input */
    	AgendaClient ac = new AgendaClient(args);
    	try
		{
			ac.rmiProcess();
	    	ac.select();
		}
		catch (RemoteException e)
		{
			e.printStackTrace();
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
		}
		catch (NotBoundException e)
		{
			e.printStackTrace();
		}
    }
}
