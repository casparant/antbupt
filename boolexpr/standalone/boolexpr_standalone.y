%{
#include <ctype.h>
#include <stdio.h>
#define YYSTYPE int
%}

%error-verbose
%right NOT
%token  TRUE
%token  FALSE
%token  OR
%token  AND
%token  NOT

%%
line : line bexpr '\n' {$1 == 1 ? printf("true\n", $1) : printf("false\n", $1); }
     | line '\n'
     |
     | error '\n'
     ;

bexpr : bexpr OR bterm { $$ = $1 | $3; }
      | bterm
      ;

bterm : bterm AND bfactor { $$ = $1 & $3; }
      | bfactor
      ;

bfactor : NOT bfactor { $$ = !$2; }
        | '(' bexpr ')' { $$ = $2; }
        | TRUE
        | FALSE
        ;
%%

char *yytext;
int main()
{
    yytext = (char *)malloc(sizeof(char)*6);
    yyparse();
    free(yytext);
    return 0;
}

yylex()
{
    int c;
    while ((c = getchar()) == ' ')
        ;
    if (islower(c))
    {
        ungetc(c, stdin);
        scanf("%[^() \t\n\r]", yytext);
        if (!strcmp(yytext, "true"))
        {
            yylval = 1;
            return TRUE;
        }
        else if (!strcmp(yytext, "false"))
        {
            yylval = 0;
            return FALSE;
        }
        else if (!strcmp(yytext, "or"))
        {
            return OR;
        }
        else if (!strcmp(yytext, "and"))
        {
            return AND;
        }
        else if (!strcmp(yytext, "not"))
        {
            return NOT;
        }
    }
    if (c != ' ')
        return c;
}

int yyerror(char *msg, char* yytext)
{
    fprintf(stdout, "%s %s\n", yytext, msg);
}

