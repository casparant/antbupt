%{
#include <ctype.h>
#include <stdio.h>
#define YYSTYPE int
%}

%error-verbose
%right  NOT
%left   OR AND
%token  TRUE
%token  FALSE
%token  OR
%token  AND
%token  NOT

%%
line : line bexpr '\n' {$1 == 1 ? printf("true\n", $1) : printf("false\n", $1); }
     | line '\n'
     |
     ;

bexpr : bexpr OR bterm { $$ = $1 | $3; }
      | bterm
      ;

bterm : bterm AND bfactor { $$ = $1 & $3; }
      | bfactor
      ;

bfactor : NOT bfactor { $$ = !$2; }
        | '(' bexpr ')' { $$ = $2; }
        | TRUE
        | FALSE
        ;

%%

int main()
{
    yyparse();
    return 0;
}

int yyerror(char *msg, char* yytext)
{
    fprintf(stdout, "%s %s\n", yytext, msg);
}

