%{
#include "y.tab.h"
#include <stdio.h>
#include <ctype.h>
%}

TRUE    true
FALSE   false
OR      or
AND     and
NOT     not

%%
[ ]     {}
{TRUE}  { scanf("%[^() \t\n\r]", yytext); yylval = 1; return TRUE; }
{FALSE} { scanf("%[^() \t\n\r]", yytext); yylval = 0; return FALSE; }
{OR}    { scanf("%[^() \t\n\r]", yytext); return OR; }
{AND}   { scanf("%[^() \t\n\r]", yytext); return AND; }
{NOT}   { scanf("%[^() \t\n\r]", yytext); return NOT; }
\n|\)|\(  { return yytext[0]; }
%%

int yywrap()
{
    return 1;
}
