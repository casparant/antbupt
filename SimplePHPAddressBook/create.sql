CREATE TABLE `addr_list` (
	`id`	int(10) unsigned NOT NULL auto_increment,
	`name`	varchar(10) NOT NULL,
	`sex`	tinyint(1)	NOT NULL,
	`tel`	varchar(13) NOT NULL,
	`mobi`	varchar(11) NOT NULL,
	`email`	varchar(50) NOT NULL,
	`qq`	varchar(10)	NOT NULL,
	`blog`	varchar(30)	NOT NULL,
	`schaddr`	varchar(100)	NOT NULL,
	`homeaddr`	varchar(100)	NOT	NULL,
	PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
