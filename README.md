# README #

这是我在大学里写的一些小东西，基本上都是课堂作业、实训项目等。有一些实在羞于启齿的比如MFC的项目，我就没有放上来了。

### What is this repository for? ###

+ AgendaSystem: 一个简单的C/S架构的预定系统。主要技术: RMI (我也忘记了是啥玩意儿了)
+ AntJDiary: 大一自学Java的时候和朋友一起做的一个Java日记本项目，主要装逼地使用了MVC
+ CORBAVote: 一个简单的C/S的投票系统。主要技术：IDL
+ ChatRoomGUI: 一个带GUI的C/S架构的聊天室程序。我都不知道我当年能这么牛逼。
+ Monks: 三个和尚挑水的程序(看名字你能看出来是OS课上的争抢锁那一章的课堂练习么)
+ OnlineShelf: 一个J2EE的在线借书系统。也是课堂作业，我很鄙视J2EE的。
+ POS: 一个C/S架构的Qt写的POS机
+ Report Templates: 报告模板。
+ SimplePHPAddressBook: 一个 PHP 写的地址簿
+ Software Engineering Templates: 软件工程专业的各种模板
+ boolexpr: 一些语法解析语义解析的小代码，编译原理课的内容。
+ jimu: 一个挺有意思的积木题，可以看看README
+ writeos: 在于渊的自己动手写操作系统的代码基础上改了一下。惭愧，其中核心修改部分其实不是我做的……

看完这些突然觉得自己大学的时候好全才啊。