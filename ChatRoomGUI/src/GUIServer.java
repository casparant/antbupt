/**
 * @author Caspar Zhang
 */
public class GUIServer
{
	public static void main(String args[]) 
	{
		Server server = new Server();
		GUIServerMainFrm frame = new GUIServerMainFrm();
		frame.setVisible(true);	
		while (true)
		{
			if (server != null && server.isAlive())
			{
				try
				{
					server.addUser();
				}
				catch (Exception e)
				{
					System.err.println(e.getMessage());
				}
			}
		}
	}

}
