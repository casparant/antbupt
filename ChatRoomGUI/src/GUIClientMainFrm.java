import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;

/**
 * @author Caspar Zhang
 */
@SuppressWarnings("serial")
public class GUIClientMainFrm extends JFrame 
{
	private static GUIClientMainFrm instance;
	private Client client;
	private JTextField textFieldEnter;
	private JTextArea textAreaMsg;
	
	public GUIClientMainFrm()
	{
        super();
        client = Client.getInstance();
        instance = this;
        
		addWindowListener(new WindowAdapter() 
		{
			public void windowClosing(final WindowEvent e) 
			{
				sendMsg("/leave");
				try
				{
					client.disconnect();
				}
				catch (Exception ex)
				{
					ex.printStackTrace();
				}
			}
		});
        
		setTitle("Chatroom Client");
		getContentPane().setLayout(new BorderLayout());
		setBounds(100, 100, 600, 400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(600, 400);
		
		final JLabel labelTitle = new JLabel();
		labelTitle.setHorizontalAlignment(SwingConstants.CENTER);
		labelTitle.setText("Chatroom Client 0.1");
		getContentPane().add(labelTitle, BorderLayout.NORTH);
		
		final JSplitPane splitPaneMsg = new JSplitPane();
		splitPaneMsg.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPaneMsg.setLastDividerLocation(1);
		splitPaneMsg.setDividerSize(1);
		splitPaneMsg.setDividerLocation(0.95);
		splitPaneMsg.addComponentListener(new ComponentAdapter()
		{
            public void componentResized(ComponentEvent e) 
            {
                splitPaneMsg.setDividerLocation(0.9);
            }
        });
		getContentPane().add(splitPaneMsg, BorderLayout.CENTER);

		final JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		splitPaneMsg.setLeftComponent(scrollPane);

		textAreaMsg = new JTextArea();
		textAreaMsg.setEditable(false);
		textAreaMsg.setWrapStyleWord(true);
		textAreaMsg.setLineWrap(true);
		scrollPane.setViewportView(textAreaMsg);

		final JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		splitPaneMsg.setRightComponent(panel);

		final JButton sendButton = new JButton();
		sendButton.setToolTipText("Send your message");
		sendButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(final ActionEvent e) 
			{
				if (textFieldEnter.getText().length() == 0)
				{
					putMsg("You can't send empty message!");
				}
				else
				{
					sendMsg("/chat " + textFieldEnter.getText());
				}
				textFieldEnter.setText(null);
			}
		});
		sendButton.setPreferredSize(new Dimension(75, 25));
		sendButton.setText("Send");
		panel.add(sendButton, BorderLayout.EAST);

		textFieldEnter = new JTextField();
		textFieldEnter.addKeyListener(new KeyListener()
		{
			public void keyPressed(final KeyEvent e)
			{
				if (e.getKeyChar() == '\n')
				{
					if (textFieldEnter.getText().length() == 0)
					{
						putMsg("You can't send empty message!");
					}
					else
					{
						sendMsg("/chat " + textFieldEnter.getText());
					}
					textFieldEnter.setText(null);
				}
			}

			public void keyReleased(KeyEvent e) { }
			public void keyTyped(KeyEvent e) {	}
		});
		panel.add(textFieldEnter);
		textFieldEnter.setPreferredSize(new Dimension(0, 25));
	}
	
	public void sendMsg(String msg)
	{
		try
		{
			client.getWriter().writeString(msg);
		}
		catch (IOException e)
		{
			if (e.getMessage().matches("Broken pipe"))
			{
				try
				{
					client.disconnect();
				}
				catch (Exception ex)
				{
					ex.printStackTrace();
				}
			}
		}
	}
	
	public void setRedirectMsg(boolean flag)
	{
		if (flag == true)
		{
			JTextAreaPrintStream guiPrintStream = new JTextAreaPrintStream(System.out, textAreaMsg);
			System.setErr(guiPrintStream);
			System.setOut(guiPrintStream);
		}
		else
		{
			System.setErr(System.err);
			System.setOut(System.out);
		}
	}
	
	public static GUIClientMainFrm getInstance()
	{
		return instance;
	}
	
	public void putMsg(final String msg)
	{
		textAreaMsg.append(msg + "\n");
		textAreaMsg.setCaretPosition(textAreaMsg.getText().length());
	}
}
