import java.io.IOException;

/**
 * @author Amelie Lee
 * Modified by Caspar Zhang
 */
public class Controller 
{
	private User user;
	private	 Server server;
	private UTF8Writer writer;
	
	public Controller(User u)
	{
		this.user = u;
	}

	public synchronized void userMsgControl(String msg)
	{        
        server  = Server.getInstance();
        System.out.println("[Ctrl] Receive Message: " + msg);
		if(msg.startsWith("/setName"))
		{
			String name = msg.substring(9); // get substring
			if(name.matches("Visitor")) // if not set name
			{
				name = "Visitor"+server.getTotal();
				System.out.println("[Ctrl] Set Name: " + name);
			}
			else
			{
				System.out.println("[Ctrl] Get Name: " + name);
			}
			this.user.setNickname(name);

			polling("/show " + name + " comes to the chatroom !\n");
		}
		else if(msg.startsWith("/chat"))
		{
			String chat = msg.substring(6);
			polling("/show " + this.user.getNickname() + " says: " + chat);
		}
		else if (msg.startsWith("/leave"))
		{
			polling("/show " + this.user.getNickname() + " leaves the room!");
		}
	}
	
	public void polling(String msg)
	{
		int toDel = server.getUsers().size(); 
		for (int i = 0; i < server.getUsers().size(); ++i)
		{
			try
			{
				writer = new UTF8Writer(server.getUsers().get(i).getSocket().getOutputStream());
				writer.writeString(msg);
			}
			catch (IOException e)
			{
				if (e.getMessage().matches("Broken pipe"))
				{
					try
					{
						server.getUsers().get(i).getSocket().close();
						toDel = i;
					}
					catch (IOException ex)
					{
						ex.printStackTrace();
					}
				}
			}
	    }
		if (toDel != server.getUsers().size())
		{
			server.delUser(server.getUsers().get(toDel));
		}
	}
}
