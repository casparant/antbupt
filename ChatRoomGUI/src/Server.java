import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;

/**
 * @author Caspar Zhang
 */
public class Server
{
	private static Server instance;
	private ServerSocket serverSocket;
	private Socket socket;
	private LinkedList<User> users;
	private int PORT;
	private int total;	

	public Server() 
	{
		instance = this;
		this.users = new LinkedList<User>();
		this.serverSocket = null;
		this.socket = null;
		this.PORT = 2009;
		this.total = 0;
	}
	
	public void startServer(int port) throws Exception
	{
		this.PORT = port;
		this.socket = null;
		this.serverSocket = new ServerSocket(PORT, 10);
	}
	
	public void stopServer() throws Exception
	{
		this.serverSocket.close();
	}
	
	public boolean isStopped()
	{
		return (this.serverSocket == null || this.serverSocket.isClosed());
	}
	
	public void addUser() throws Exception
	{
		this.socket = this.serverSocket.accept();
		this.socket.setSoTimeout(300000);
		System.out.println("[Server] Accept: " + this.socket);

		if (this.socket != null)
		{
			User user = new User(this.socket);
			this.users.add(user);
			this.total++;
			user.start();
		}
	}
	
	public synchronized void delUser(User user)
	{
		users.remove(user);
	}
	
    public static Server getInstance() 
	{
		return instance;
	}
	
    public LinkedList<User> getUsers()
	{
		return this.users;
	}
	
    public Socket getSocket()
	{
		return this.socket;
	}
    
    public ServerSocket getServerSocket()
    {
    	return this.serverSocket;
    }
    
    public int getTotal()
    {
        return this.total;
    }
    
    public boolean isAlive()
    {
    	return (this.serverSocket != null && !this.serverSocket.isClosed());
    }

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		if (args.length != 1)
		{
			System.err.println("Usage: java Server <PORT>");
			System.exit(0);
		}

		int port = Integer.parseInt(args[0]);
		Server server = new Server();
		try
		{
			System.out.println("[Server] Launching Server...");
			server.startServer(port);
			System.out.println("[Server] Server Launched at PORT " + port + ".");
			while (true)
			{
				if (server != null && server.getServerSocket() != null && !server.getServerSocket().isClosed())
				{
					server.addUser();
				}
			}
		}
		catch (Exception e)
		{
			System.err.println("[Error] " + e.getMessage());
		}
		
	}
}
