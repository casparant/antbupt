import java.io.OutputStream;
import java.io.PrintStream;
import javax.swing.JTextArea;

/**
 * @author Caspar Zhang
 * This class is used to redirect standard console stream to the JTextArea instance
 */
public class JTextAreaPrintStream extends PrintStream
{
	private JTextArea textArea;
	
	public JTextAreaPrintStream(OutputStream out, JTextArea textArea)
	{
		super(out);
		this.textArea = textArea;
	}

	public void write(byte[] buf, int off, int len)
	{
		final String message = new String(buf, off, len);
		this.textArea.append(message);
		this.textArea.setCaretPosition(this.textArea.getText().length());
	}
}
