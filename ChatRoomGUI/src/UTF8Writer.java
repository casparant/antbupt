import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Binghuang Hong
 * Modified by Caspar Zhang
 * This Program is used to convert an output stream to utf-8 codeset.
 */
public class UTF8Writer 
{
	private OutputStream out;
	
	public UTF8Writer(OutputStream o)
	{
		this.out = o;
	}
	
	public void writeString(String s) throws IOException
	{
		out.write(s.getBytes("UTF-8"));
		out.write('\0');
	}
}
