import java.io.IOException;

/**
 * @author Caspar Zhang
 */
public class GUIClient 
{
	public static void main(String[] args)
	{
		final Client client = new Client();
        GUIClientMainFrm frame = new GUIClientMainFrm();
		GUIClientLoginDlg dialog = new GUIClientLoginDlg();
		dialog.setVisible(true);
		
		while (true)
		{
			if (client.isAlive())
			{
				String msg = null;
				try
				{
					msg = client.getReader().readString();
				}
				catch (IOException e)
				{
					System.out.println(e.getMessage());
				}
				finally
				{
					if (msg != null)
					{
						msg = client.msgHandler(msg);
						if (msg != null)
						{
							frame.putMsg(msg);
						}
					}
				}
			}
		}
	}

}
