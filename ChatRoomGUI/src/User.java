import java.io.IOException;
import java.net.*;

/**
 * @author Amelie Lee
 * Modified by Caspar Zhang
 */
public class User extends Thread
{
	private String nickname;
	private Socket socket;

	public User(Socket s)
	{
		this.socket = s;
	}
	
	public void run()
	{
        Controller con = new Controller(this);

		System.out.println("[Server] Client Thread is Running. Client Number: " + Server.getInstance().getUsers().size());
		while(!this.socket.isClosed())
		{
			try
			{
				String msg = new UTF8Reader(this.socket.getInputStream()).readString();
				if(msg != null)
				{
					con.userMsgControl(msg);
				}
			}
			catch (IOException e)
			{
				if (e.getMessage().matches("Connection reset"))
				{
					try
					{
						this.socket.close();
						Server.getInstance().delUser(this);
					}
					catch (IOException ex)
					{
						ex.printStackTrace();
					}
				}
			}
		}
		System.out.println("[Server] Client Thread Over. Client Number: " + Server.getInstance().getUsers().size());
	}
	
	public String getNickname()
	{
		return this.nickname;
	}
	
	public void setNickname(String s)
	{
		this.nickname = s;
	}

    public Socket getSocket()
    {
        return this.socket;
    }
}
