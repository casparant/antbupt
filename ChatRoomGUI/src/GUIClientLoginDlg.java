import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 * @author Caspar Zhang
 */
@SuppressWarnings("serial")
public class GUIClientLoginDlg extends JDialog 
{
	private Client client;
	private GUIClientMainFrm frame;
	private JTextField textFieldNickname;
	private JTextField textFieldPort;
	private JTextField textFieldIp;

	public GUIClientLoginDlg() 
	{
		super();
		client = Client.getInstance();
		frame = GUIClientMainFrm.getInstance();
		
		addWindowListener(new WindowAdapter() 
		{
			public void windowClosing(WindowEvent e) 
			{
				System.exit(0);
			}
		});
		
		setTitle("Chatroom Login");
		setResizable(false);
		setSize(new Dimension(400, 300));
		getContentPane().setLayout(null);
		setBounds(100, 100, 400, 300);

		final JLabel labelTitle = new JLabel();
		labelTitle.setHorizontalTextPosition(SwingConstants.CENTER);
		labelTitle.setHorizontalAlignment(SwingConstants.CENTER);
		labelTitle.setPreferredSize(new Dimension(250, 50));
		labelTitle.setBounds(75, 5, 250, 50);
		labelTitle.setText("Chatroom Login");
		getContentPane().add(labelTitle);

		final JLabel labelIp = new JLabel();
		labelIp.setHorizontalTextPosition(SwingConstants.CENTER);
		labelIp.setHorizontalAlignment(SwingConstants.TRAILING);
		labelIp.setBounds(50, 80, 75, 25);
		labelIp.setText("Server IP");
		getContentPane().add(labelIp);

		textFieldIp = new JTextField();
		textFieldIp.setText("127.0.0.1");
		textFieldIp.setBounds(130, 80, 200, 25);
		getContentPane().add(textFieldIp);

		textFieldPort = new JTextField();
		textFieldPort.setText("2009");
		textFieldPort.setBounds(130, 120, 200, 25);
		getContentPane().add(textFieldPort);

		final JLabel labelPort = new JLabel();
		labelPort.setHorizontalTextPosition(SwingConstants.CENTER);
		labelPort.setHorizontalAlignment(SwingConstants.TRAILING);
		labelPort.setText("Server PORT");
		labelPort.setBounds(50, 120, 75, 25);
		getContentPane().add(labelPort);

		final JLabel labelNickname = new JLabel();
		labelNickname.setHorizontalTextPosition(SwingConstants.CENTER);
		labelNickname.setHorizontalAlignment(SwingConstants.TRAILING);
		labelNickname.setText("Nickname");
		labelNickname.setBounds(50, 160, 75, 25);
		getContentPane().add(labelNickname);

		textFieldNickname = new JTextField();
		textFieldNickname.setAutoscrolls(false);
		textFieldNickname.setText("Visitor");
		textFieldNickname.setBounds(130, 160, 200, 25);
		getContentPane().add(textFieldNickname);

		final JLabel labelStatus = new JLabel();
		labelStatus.setBounds(50, 210, 200, 25);
		getContentPane().add(labelStatus);
		
		final JButton loginButton = new JButton();
		loginButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(final ActionEvent e) 
			{
				String ip = textFieldIp.getText();
				String port = textFieldPort.getText();
				String nickname = textFieldNickname.getText();
				
				try 
				{
					client.connect(ip, port, nickname);
					frame.setVisible(true);
					frame.setRedirectMsg(true);
					dispose();
				}
				catch (IOException ex)
				{
					labelStatus.setText(ex.getMessage());
				}
			}
		});
		loginButton.setText("Login");
		loginButton.setBounds(255, 210, 75, 25);
		getContentPane().add(loginButton);
	}
}
