import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

/**
 * @author Caspar Zhang
 *
 */
public class Client 
{
	private static Client instance;
	private Socket socket;
	
	public Client()
	{
		instance = this;
		socket = null;
	}
	
	public void connect(String ip, String port, String nickname) throws IOException
	{
		InetAddress a = InetAddress.getByName(ip);
		int p = Integer.parseInt(port);
		this.socket = new Socket(a, p);
		getWriter().writeString("/setName " + nickname);
	}
	
	public void disconnect() throws Exception
	{
		this.socket.close();
	}

	public String msgHandler(String msg)
	{
        //System.out.println("[DEBUG] Message Handler Received: " + msg);
		if(msg.startsWith("/show"))
		{
			String text = msg.substring(6);
			return text;
		}
		return null;
	}
	
	public static Client getInstance()
	{
		return instance;
	}
	
	public UTF8Writer getWriter() throws IOException
	{
		return new UTF8Writer(this.socket.getOutputStream());
	}
	
	public UTF8Reader getReader() throws IOException
	{
		return new UTF8Reader(this.socket.getInputStream());
	}
	
	public boolean isAlive()
	{
		return (this.socket != null && !this.socket.isClosed() && this.socket.isConnected());
	}
	
//	/**
//	 * CLI version
//	 * TODO not implemented.
//	 * @param args
//	 */
//	public static void main(String[] args)
//	{
//		
//	}
}
