import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;

/**
 * @author Caspar Zhang
 */

@SuppressWarnings("serial")
public class GUIServerMainFrm extends JFrame 
{
	private Server server;
	private JTextField textFieldPort;
	private JTextArea textAreaMsg;

	public GUIServerMainFrm() 
	{
		super();		
		server = Server.getInstance();
		
		setTitle("Chatroom Server");
		getContentPane().setLayout(new BorderLayout());
		setBounds(100, 100, 800, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		final JLabel labelTitle = new JLabel();
		labelTitle.setHorizontalAlignment(SwingConstants.CENTER);
		labelTitle.setText("Chatroom Server 0.1");
		getContentPane().add(labelTitle, BorderLayout.NORTH);

		final JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		getContentPane().add(scrollPane, BorderLayout.CENTER);

		textAreaMsg = new JTextArea();
		scrollPane.setViewportView(textAreaMsg);
		textAreaMsg.setLineWrap(true);
		textAreaMsg.setEditable(false);

		final JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.SOUTH);

		final JLabel labelPort = new JLabel();
		labelPort.setText("PORT");
		panel.add(labelPort);

		textFieldPort = new JTextField();
		textFieldPort.setPreferredSize(new Dimension(100, 25));
		textFieldPort.setText("2009");
		panel.add(textFieldPort);

		final JButton buttonLaunch = new JButton();
		buttonLaunch.setPreferredSize(new Dimension(120, 25));
		buttonLaunch.setText("Launch Server");
		panel.add(buttonLaunch);
		buttonLaunch.addActionListener(new ActionListener() 
		{
			public void actionPerformed(final ActionEvent e) 
			{
				if (server.isStopped())
				{
					putMsg("[Server] Launching Server...");
					int port;
					try
					{
						port = Integer.parseInt(textFieldPort.getText());
						server.startServer(port);
						putMsg("[Server] Server Launched at PORT " + port + ".");
					}
					catch (NumberFormatException ex)
					{
						putMsg("[Error] " + ex.getMessage());
					}
					catch (Exception ex)
					{
						putMsg("[Error] " + ex.getMessage());
					}
				}
				else
				{
					putMsg("[Error] Server Already Running. One Instance Allowed Only!");
				}
			}
		});
		setRedirectMsg(true);
	}
	
	public void setRedirectMsg(boolean flag)
	{
		if (flag == true)
		{
			JTextAreaPrintStream guiPrintStream = new JTextAreaPrintStream(System.out, textAreaMsg);
			System.setErr(guiPrintStream);
			System.setOut(guiPrintStream);
		}
		else
		{
			System.setErr(System.err);
			System.setOut(System.out);
		}
	}
	
	public void putMsg(final String msg)
	{
		textAreaMsg.append(msg + "\n");
		textAreaMsg.setCaretPosition(textAreaMsg.getText().length());
	}
}
