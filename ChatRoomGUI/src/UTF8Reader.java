import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Binghuang Hong
 * Modified by Caspar Zhang
 * This Program is used to convert an output stream to utf-8 codeset.
 */
public class UTF8Reader 
{
	private InputStream in;
	
	public UTF8Reader(InputStream s)
	{
		this.in = s;
	}
	
	public String readString() throws IOException
	{
		ByteArrayOutputStream b = new ByteArrayOutputStream();

		int n;
		String line = null;
		while((n = in.read()) != -1)
		{
			if ((n == 0) || (n == 10)) 
			{
				line = new String(b.toByteArray(), "UTF-8");
				b.reset();
				return line;
			}
			else
			{
				b.write(n);
			}
		}	
		return null;
	}
}
