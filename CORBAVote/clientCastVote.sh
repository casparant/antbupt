#!/bin/sh

echo "------------ Cast Vote ------------"

localip=`ifconfig eth0 | grep 'inet addr:' | cut -b 21-35 | cut -d ' ' -f 1`

echo -n "Enter Remote Host IP[$localip]: "
read ip
if [ -z $ip ]; then
    ip=$localip
fi

echo -n "Enter Host Port[1050]: "
read portnum
if [ -z $portnum ]; then
    portnum="1050"
fi

while [ -z $name ]
do
    echo -n "The Name You Want to Vote: "
    read name
done

java vote.VoteClient -ORBInitialHost $localip -ORBInitialPort $portnum -castVote $name
