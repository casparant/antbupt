package vote;

public class VoteDataStruct
{
	public String name;
	public long number;
	public VoteDataStruct(String name, long number)
	{
		this.name = name;
		this.number = number;
	}
}
