/**
 * @File: VoteServer.java
 * @Author: Caspar Zhang <casparant@gmail.com>
 * 
 */

package vote;

import org.omg.CORBA.ORB;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.PortableServer.POA;

import VoteApp.*;

public class VoteServer
{
	private ORB orb;
	private VoteImpl voteImpl = new VoteImpl();
	
	public VoteServer(String[] args) throws Exception
	{
		/* initialize the ORB */
		orb = ORB.init(args, null);
		init(args);
	}
	
	public void init(String[] args) throws Exception
	{
		/* get reference to rootpoa & activate the POAManager */
		POA rootPoa = (POA)orb.resolve_initial_references("RootPOA");
		rootPoa.the_POAManager().activate();

		/* register it with the ORB */
		voteImpl.setORB(orb);
		
		/* get object reference from the servant */
		org.omg.CORBA.Object ref = rootPoa.servant_to_reference(voteImpl);
		/* and cast the reference to a CORBA reference */
		Vote href = VoteHelper.narrow(ref);
		
		/* get the root naming context */
		/* NameService invokes the transient name service */
		org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
		
		/* Use NamingContextExt, which is part of the
		 * Interoperable Naming Service (INS) specification. */
		NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

		/* bind the Object Reference in Naming */
		NameComponent path[] = ncRef.to_name("Vote");
		ncRef.rebind(path, href);

		System.out.println("VoteServer ready and waiting ...");

		run();
	}
	
	public void run()
	{
		/* wait for invocations from clients */
		orb.run();
	}
	
	public static void main(String[] args)
	{
		try
		{
			new VoteServer(args);
		}
		catch (Exception e)
		{
			e.printStackTrace(System.err);
		}
		System.out.println("VoteServer Exiting ...");
	}
}
