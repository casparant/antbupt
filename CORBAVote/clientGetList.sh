#!/bin/sh

echo "------------ getList ------------"

localip=`ifconfig eth0 | grep 'inet addr:' | cut -b 21-35 | cut -d ' ' -f 1`

echo -n "Enter Remote Host IP[$localip]: "
read ip
if [ -z $ip ]; then
    ip=$localip
fi

echo -n "Enter Host Port[1050]: "
read portnum
if [ -z $portnum ]; then
    portnum="1050"
fi

java vote.VoteClient -ORBInitialHost $localip -ORBInitialPort $portnum -getList
