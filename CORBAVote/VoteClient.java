/**
 * @File: VoteClient.java
 * @author Caspar Zhang <casparant@gmail.com>
 * 
 */
package vote;

import org.omg.CORBA.ORB;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import VoteApp.*;

public class VoteClient
{
	private static Vote voteImpl;
	private ORB orb;
	
	public VoteClient(String[] args) throws Exception
	{
		init(args);
	}
	
	public void init(String[] args) throws Exception
	{	
		/* initialize the ORB */
		orb = ORB.init(args, null);

		/* get the root naming context */
		org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
		
		/* Use NamingContextExt instead of NamingContext,
		 * part of the Interoperable naming Service. */
		NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

		/* resolve the Object Reference in Naming */
		voteImpl = VoteHelper.narrow(ncRef.resolve_str("Vote"));
	}
	
	public void operation(String[] newArgs)
	{
		if (newArgs[0].equals("-getList"))
		{
			getList();
		}
		else if (newArgs[0].equals("-castVote"))
		{
			castVote(newArgs[1]);
		}
		else if (newArgs[0].equals("-shutdown"))
		{
			shutdown();
		}
		else 
		{
			System.err.println("Error Arguments");
		}
	}
	
	public void getList()
	{		
		String voteStr = voteImpl.getList();
		if (voteStr.isEmpty())
		{
			System.out.println("No Candidate, List Empty.");
		}
		else
		{
			System.out.println("------------ Candidate List ------------");
			System.out.println("Name                          Number");
			System.out.println(voteStr);
		}
	}
	
	public void castVote(String name)
	{
		voteImpl.castVote(name);
	}
	
	public void shutdown()
	{
		voteImpl.shutdown();
	}

	public static void main(String[] args)
	{
		String[] newArgs = new String[2];
		
		if (args.length <= 4 || !args[4].equals("-castVote") && !args[4].equals("-getList") && !args[4].equals("-shutdown"))
		{
			System.err.println("Usage: java VoteClient -ORBInitialHost <hostip> -ORBInitialPort <hostport> -castVote <name>\n" +
							   "       java VoteClient -ORBInitialHost <hostip> -ORBInitialPort <hostport> -getList\n" +
							   "       java VoteClient -ORBInitialHost <hostip> -ORBInitialPort <hostport> -shutdown\n");
			System.exit(128);
		}
		newArgs[0] = args[4];
		args[4] = null;
		if (args.length == 6)
		{
			newArgs[1] = args[5];
			args[5] = null;
		}
		try
		{
			VoteClient client = new VoteClient(args);
			client.operation(newArgs);
		}
		catch (Exception e)
		{
			e.printStackTrace(System.err);
		}
	}
}
