#!/bin/sh

# start orbd -ORBInitialPort 1234&

echo "------------ run ORBD ------------"

localip=`ifconfig eth0 | grep 'inet addr:' | cut -b 21-35 | cut -d ' ' -f 1`

echo -n "Enter Host IP[$localip]: "
read ip
if [ -z $ip ]; then
    ip=$localip
fi

echo -n "Enter Port[1050]: "
read portnum
if [ -z $portnum ]; then
    portnum="1050"
fi

orbd -ORBInitialPort $portnum -ORBInitialHost $localip &
