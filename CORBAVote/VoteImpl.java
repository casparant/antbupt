// The servant -- object implementation -- for the Hello
// example.  Note that this is a subclass of HelloPOA, whose
// source file is generated from the compilation of
// Hello.idl using j2idl.

package vote;

import java.util.LinkedList;

import org.omg.CORBA.ORB;
import VoteApp.*;

class VoteImpl extends VotePOA
{
	private ORB orb;
	private LinkedList<VoteDataStruct> list = new LinkedList<VoteDataStruct>();
	

	/**
	 * implement setORB
	 * @param orb_val
	 */
	public void setORB(ORB orb_val)
	{
		orb = orb_val;
	}

	/**
	 * implement getVote
	 * @return map
	 */
	public String getList()
	{
		StringBuffer retStrBuf = new StringBuffer();
		for (VoteDataStruct i : list)
		{
			retStrBuf.append(String.format("%1$-30s%2$d\n", i.name, i.number));
		}
		return (new String(retStrBuf));
	}
	
	public void castVote(String name)
	{
		for (int i = 0; i < list.size(); ++i)
		{
			VoteDataStruct voteTmp = list.get(i);
			if (voteTmp.name.equals(name))
			{
				long val = voteTmp.number;
				System.out.println(name + ": Orig-" + val + ", Now-" + (val+1));
				voteTmp.number++;
				return;
			}
		}
		// if not found
		
		System.out.println("new person to be elected: " + name);
		list.add(new VoteDataStruct(name, 1));
	}
	
	/**
	 *  implement shutdown() method
	 */
	public void shutdown()
	{
		orb.shutdown(false);
	}
}
