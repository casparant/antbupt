/*
 * Copyright(C) by Jiawei Chen, Rui Tao
 */

#include "postype.h"
#include "filelock.h"

LogRecord *log_list = NULL;
LogRecord *log_cur = NULL;
AccountRecord *acc_list = NULL;
AccountRecord *acc_cur = NULL;

int main(void)
{
    int             select, exit_in = 0;
    FILE            *logfp;
    FILE            *accfp;
    AccountRecord   *acc_tmp;
    LogRecord       *log_tmp;
    Client          *client_tmp;
    int             bytes, total_bytes = 0;
    double          income = 0, outcome = 0, total_income = 0, total_outcome = 0;
    int             started = false;
    long            clientNo, i;
    FILE            *clientfp, *clientsfp;

    while(!exit_in)
    {
        select = 0;
        printf("MENU:\n");
        printf("(1)Change POS Client Numbers.\n(2)Add Accounts.\n(3)Del Accounts.\n(4)Resume Accounts.\n(5)View Accounts.\n(0)Exit.\n");
        scanf("%d",&select);
        printf("\n");
        switch(select)
        {
        case 1:
            client_tmp = (Client *)malloc(CLIENT_STRUCT_SIZE);
            printf("Input POS Client Number(N<1,000,000): ");
            scanf("%ld", &clientNo);
            if ((clientsfp = fopen(CLIENTS_PATH, "w+")) == NULL)
            {
                fprintf(stderr, "Cannot create file \"clients\".\n");
                break;
            }
            for (i = 0; i < clientNo; ++i)
            {
                sprintf(client_tmp->clientNo, "%ld", i+1);
                strcpy(client_tmp->path, "./client.log.d/client");
                strcat(client_tmp->path, client_tmp->clientNo);
                strcat(client_tmp->path, ".log\0");
                strcpy(client_tmp->pid, "0");
                if ((clientfp = fopen(client_tmp->path, "w+")) == NULL)
                {
                    fprintf(stderr, "Cannot create file \"%s\".\n", client_tmp->path);
                    continue;
                }
                fclose(clientfp);
                writeClients(clientsfp, client_tmp);
                bzero(client_tmp, CLIENT_STRUCT_SIZE);
            }
            free(client_tmp);
            fclose(clientsfp);
            break;
        case 2:
            acc_tmp = (AccountRecord *)malloc(ACCRECORD_SIZE);
            acc_tmp->suspended = '0';
            printf("Input Account Number(L=%d): ", CARDID_LENGTH);
            scanf("%s", acc_tmp->cardId);
            // TODO verify the account already exists or not.
            printf("Input Account Password(L=%d): ", PASSWD_LENGTH);
            scanf("%s", acc_tmp->passwd);
            printf("Input Initial Balance(MAX=99,999,999.99): ");
            scanf("%s", acc_tmp->balance);
            if((accfp = fopen(ACCOUNTS_PATH, "a+")) == NULL)
            {
                perror("Cannot open file \"accounts\" for appending.");
              	break;
            }
            writeAccount(accfp, acc_tmp);
            fclose(accfp);
            break;
        case 3:
            //printf("Input Account Number to Del:");
            // TODO
        case 4:
            //printf("Input Account Number to Resume:");
            // TODO
        case 5:
        case 0:
            printf("Logout.\n");
            exit_in = 1;
            getchar();
        }
    }
    return 0;
}
