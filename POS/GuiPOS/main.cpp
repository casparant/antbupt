/*
 * Copyright(C) 2009 by Caspar Zhang, casparant@gmail.com
 */
#include <QtGui/QApplication>
#include "posmainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    POSMainWindow w;
    w.show();
    return a.exec();
}
