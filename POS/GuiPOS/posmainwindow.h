/*
 * Copyright(C) 2009 by Caspar Zhang, casparant@gmail.com
 */
#ifndef POSMAINWINDOW_H
#define POSMAINWINDOW_H

#include <QTimer>
#include <QtGui/QMainWindow>
#include <ui_posmainwindow.h>
#include <postype.h>
#include <filelock.h>

namespace Ui
{
    class POSMainWindowClass;
}

class POSMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    POSMainWindow(QWidget *parent = 0);
    ~POSMainWindow();

private slots:
    void on_pushButton1_clicked();
    void on_pushButton2_clicked();
    void on_pushButton3_clicked();
    void on_pushButton4_clicked();
    void on_pushButton5_clicked();
    void on_pushButton6_clicked();
    void on_pushButton7_clicked();
    void on_pushButton8_clicked();
    void on_pushButton9_clicked();
    void on_pushButton0_clicked();
    void on_pushButton00_clicked();
    void on_pushButtonDot_clicked();
    void on_pushButtonCancel_clicked();
    void on_pushButtonClear_clicked();
    void on_pushButtonEnter_clicked();
    void on_pushButtonInsert_clicked();

    void on_pushButtonMainWithdraw_clicked();
    void on_pushButtonMainInquire_clicked();
    void on_pushButtonMainDeposit_clicked();
    void on_pushButtonMainChangePwd_clicked();
    void on_pushButtonMainExit_clicked();

    void on_pushButtonWithdraw100_clicked();
    void on_pushButtonWithdraw200_clicked();
    void on_pushButtonWithdraw300_clicked();
    void on_pushButtonWithdraw400_clicked();
    void on_pushButtonWithdraw500_clicked();
    void on_pushButtonWithdraw1000_clicked();
    void on_pushButtonWithdrawExit_clicked();

    void on_pushButtonStatusContinue_clicked();
    void on_pushButtonStatusExit_clicked();

    void on_pushButtonDepositCancel_clicked();
    void on_pushButtonDepositOK_clicked();

    void timeoutslot();
private:
    bool eventFilter(QObject *target, QEvent *event);
    void withDraw(int amount);
    void deposit();
    void exitPOS();
    void clearLoginPage();
    void clearWithdrawPage();
    void clearDepositPage();
    void resetTimer(int interval);

    Ui::POSMainWindowClass *ui;

    QString lineEditValue;

    /* C-style code, for connecting the c-based server */
    FILE *acfp, *logfp, *clientsfp;
    long acpos, logpos, clientpos;
    AccountRecord * ar;
    LogRecord *log_buffer;
    Client *cr;
    QString seq;
    QString client_path;
    QTimer *timer;
};

#endif // POSMAINWINDOW_H
