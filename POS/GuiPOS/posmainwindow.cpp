/*
 * Copyright (C) 2009 by Caspar Zhang, casparant@gmail.com
 */
#include <posmainwindow.h>
#include <postype.c>
#include <filelock.c>
#include <signal.h>

POSMainWindow::POSMainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::POSMainWindowClass)
{
    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(timeoutslot()));
    ui->setupUi(this);
    ui->lineEditBottom->installEventFilter(this);
    ui->stackedWidgetDisp->setCurrentIndex(0);
    QRegExp regExp("[0-9]+");
    ui->lineEditBottom->setValidator(new QRegExpValidator(regExp, this));

    // open clients file
    if ((clientsfp = fopen(CLIENTS_PATH, "r+")) == NULL)
    {
        fprintf(stderr, "ERROR: ");
        perror(CLIENTS_PATH);
        return;
    }
    // find first available seq
    cr = (Client *)malloc(CLIENT_STRUCT_SIZE);
    if ((clientpos = findAvailClientSeq(clientsfp, cr)) < 0)
    {
        fprintf(stderr, "ERROR: client number reaches limit.\n");
        return;
    }

    // update clients
    sprintf(cr->pid, "%-7u", getpid());
    fseek(clientsfp, clientpos, SEEK_SET);
    writeClients(clientsfp, cr);
    fprintf(stdout, "client_path=%s started.\n", cr->path);
    fclose(clientsfp);

    ar = (AccountRecord *)malloc(ACCRECORD_SIZE);

}

POSMainWindow::~POSMainWindow()
{
    //if (PIS_WRITELOCK(acfp, acpos, SEEK_SET, ACCRECORD_SIZE) > 0)
    //{
        PUN_LOCK(acfp, acpos, SEEK_SET, ACCRECORD_SIZE);
        fprintf(stdout, "DEBUG: account record unlocked.\n");
        fclose(acfp);
    //}
    //if (IS_WRITELOCK(logfp) > 0)
    //{
        UN_LOCK(logfp);
        fprintf(stdout, "DEBUG: client log unlocked.\n");
        fclose(logfp);
    //}
    strcpy(cr->pid, "0");

    // open clients file
    if ((clientsfp = fopen(CLIENTS_PATH, "r+")) == NULL)
    {
        fprintf(stderr, "ERROR: ");
        perror(CLIENTS_PATH);
        return;
    }
    fseek(clientsfp, clientpos, SEEK_SET);
    writeClients(clientsfp, cr); // restore clients
    free(cr);
    fclose(clientsfp);
    timer->stop();
    delete ui;
}

//////////////////////////////////////////////////////////////////////////////
// Button 0-9, 00, .

void POSMainWindow::on_pushButton1_clicked()
{
    resetTimer(15);
    if (ui->stackedWidgetDisp->currentIndex() == 1 && lineEditValue.length() < 6)
    {
        lineEditValue.append("1");
        ui->lineEditPwd->setText(lineEditValue);
        ui->labelLoginMsg->setText("");
    }
    else if (ui->stackedWidgetDisp->currentIndex() == 3)
    {
        lineEditValue.append("1");
        ui->labelWithdrawTips->setText("Choose or Input amount.\nAmount should be multiples of 100");
        ui->lineEditWithdrawAmount->setText(lineEditValue);
    }
}
void POSMainWindow::on_pushButton2_clicked()
{
    resetTimer(15);
    if (ui->stackedWidgetDisp->currentIndex() == 1 && lineEditValue.length() < 6)
    {
        lineEditValue.append("2");
        ui->lineEditPwd->setText(lineEditValue);
        ui->labelLoginMsg->setText("");
    }
    else if (ui->stackedWidgetDisp->currentIndex() == 3)
    {
        lineEditValue.append("2");
        ui->labelWithdrawTips->setText("Choose or Input amount.\nAmount should be multiples of 100");
        ui->lineEditWithdrawAmount->setText(lineEditValue);
    }
}
void POSMainWindow::on_pushButton3_clicked()
{
    resetTimer(15);
    if (ui->stackedWidgetDisp->currentIndex() == 1 && lineEditValue.length() < 6)
    {
        lineEditValue.append("3");
        ui->lineEditPwd->setText(lineEditValue);
        ui->labelLoginMsg->setText("");
    }
    else if (ui->stackedWidgetDisp->currentIndex() == 3)
    {
        lineEditValue.append("3");
        ui->labelWithdrawTips->setText("Choose or Input amount.\nAmount should be multiples of 100");
        ui->lineEditWithdrawAmount->setText(lineEditValue);
    }
}
void POSMainWindow::on_pushButton4_clicked()
{
    resetTimer(15);
    if (ui->stackedWidgetDisp->currentIndex() == 1 && lineEditValue.length() < 6)
    {
        lineEditValue.append("4");
        ui->lineEditPwd->setText(lineEditValue);
        ui->labelLoginMsg->setText("");
    }
    else if (ui->stackedWidgetDisp->currentIndex() == 3)
    {
        lineEditValue.append("4");
        ui->labelWithdrawTips->setText("Choose or Input amount.\nAmount should be multiples of 100");
        ui->lineEditWithdrawAmount->setText(lineEditValue);
    }
}
void POSMainWindow::on_pushButton5_clicked()
{
    resetTimer(15);
    if (ui->stackedWidgetDisp->currentIndex() == 1 && lineEditValue.length() < 6)
    {
        lineEditValue.append("5");
        ui->lineEditPwd->setText(lineEditValue);
        ui->labelLoginMsg->setText("");
    }
    else if (ui->stackedWidgetDisp->currentIndex() == 3)
    {
        lineEditValue.append("5");
        ui->labelWithdrawTips->setText("Choose or Input amount.\nAmount should be multiples of 100");
        ui->lineEditWithdrawAmount->setText(lineEditValue);
    }
}
void POSMainWindow::on_pushButton6_clicked()
{
    resetTimer(15);
    if (ui->stackedWidgetDisp->currentIndex() == 1 && lineEditValue.length() < 6)
    {
        lineEditValue.append("6");
        ui->lineEditPwd->setText(lineEditValue);
        ui->labelLoginMsg->setText("");
    }
    else if (ui->stackedWidgetDisp->currentIndex() == 3)
    {
        lineEditValue.append("6");
        ui->labelWithdrawTips->setText("Choose or Input amount.\nAmount should be multiples of 100");
        ui->lineEditWithdrawAmount->setText(lineEditValue);
    }
}
void POSMainWindow::on_pushButton7_clicked()
{
    resetTimer(15);
    if (ui->stackedWidgetDisp->currentIndex() == 1 && lineEditValue.length() < 6)
    {
        lineEditValue.append("7");
        ui->lineEditPwd->setText(lineEditValue);
        ui->labelLoginMsg->setText("");
    }
    else if (ui->stackedWidgetDisp->currentIndex() == 3)
    {
        lineEditValue.append("7");
        ui->labelWithdrawTips->setText("Choose or Input amount.\nAmount should be multiples of 100");
        ui->lineEditWithdrawAmount->setText(lineEditValue);
    }
}
void POSMainWindow::on_pushButton8_clicked()
{
    resetTimer(15);
    if (ui->stackedWidgetDisp->currentIndex() == 1 && lineEditValue.length() < 6)
    {
        lineEditValue.append("8");
        ui->lineEditPwd->setText(lineEditValue);
        ui->labelLoginMsg->setText("");
    }
    else if (ui->stackedWidgetDisp->currentIndex() == 3)
    {
        lineEditValue.append("8");
        ui->labelWithdrawTips->setText("Choose or Input amount.\nAmount should be multiples of 100");
        ui->lineEditWithdrawAmount->setText(lineEditValue);
    }
}
void POSMainWindow::on_pushButton9_clicked()
{
    resetTimer(15);
    if (ui->stackedWidgetDisp->currentIndex() == 1 && lineEditValue.length() < 6)
    {
        lineEditValue.append("9");
        ui->lineEditPwd->setText(lineEditValue);
        ui->labelLoginMsg->setText("");
    }
    else if (ui->stackedWidgetDisp->currentIndex() == 3)
    {
        lineEditValue.append("9");
        ui->labelWithdrawTips->setText("Choose or Input amount.\nAmount should be multiples of 100");
        ui->lineEditWithdrawAmount->setText(lineEditValue);
    }
}
void POSMainWindow::on_pushButton0_clicked()
{
    resetTimer(15);
    if (ui->stackedWidgetDisp->currentIndex() == 1 && lineEditValue.length() < 6)
    {
        lineEditValue.append("0");
        ui->lineEditPwd->setText(lineEditValue);
        ui->labelLoginMsg->setText("");
    }
    else if (ui->stackedWidgetDisp->currentIndex() == 3 && lineEditValue != "" && lineEditValue.size() < 6)
    {
        lineEditValue.append("0");
        ui->labelWithdrawTips->setText("Choose or Input amount.\nAmount should be multiples of 100");
        ui->lineEditWithdrawAmount->setText(lineEditValue);
    }
}
void POSMainWindow::on_pushButton00_clicked()
{
    resetTimer(15);
    if (ui->stackedWidgetDisp->currentIndex() == 3 && lineEditValue != "" &&  lineEditValue.size() < 5)
    {
        lineEditValue.append("00");
        ui->labelWithdrawTips->setText("Choose or Input amount.\nAmount should be multiples of 100");
        ui->lineEditWithdrawAmount->setText(lineEditValue);
    }
}
void POSMainWindow::on_pushButtonDot_clicked()
{
    resetTimer(15);
    if (false)
    {
        lineEditValue.append(".");
        //isDotted = 0;
    }
}

//////////////////////////////////////////////////////////////////////////////
// Button Withdraw, Deposit, Inquiry, Exit

void POSMainWindow::on_pushButtonMainWithdraw_clicked()
{
    ui->stackedWidgetDisp->setCurrentIndex(3);
    resetTimer(15);

}
void POSMainWindow::on_pushButtonMainDeposit_clicked()
{
    ui->labelBottom->setText("Amount");
    ui->lineEditBottom->setReadOnly(false);
    ui->lineEditBottom->setText("Mutiples of 100:");
    ui->stackedWidgetDisp->setCurrentIndex(5);
    resetTimer(15);
}
void POSMainWindow::on_pushButtonMainInquire_clicked()
{
    QString msg = "Balance : ";
    msg.append(ar->balance);
    ui->labelStatus->setText("Inquire Balance");
    ui->labelStatusMsg->setText(msg);
    ui->stackedWidgetDisp->setCurrentIndex(4);
    resetTimer(15);
}
void POSMainWindow::on_pushButtonMainChangePwd_clicked()
{
    resetTimer(15);
}
void POSMainWindow::on_pushButtonMainExit_clicked()
{
    if (ui->stackedWidgetDisp->currentIndex() != 0 && ui->stackedWidgetDisp->currentIndex() != 6)
    {
        exitPOS();
    }
}

//////////////////////////////////////////////////////////////////////////////
// Withdraw: 100, 200, 300, 400, 500, 1000, Exit

void POSMainWindow::on_pushButtonWithdraw100_clicked()
{
    withDraw(100);
}
void POSMainWindow::on_pushButtonWithdraw200_clicked()
{
    withDraw(200);
}
void POSMainWindow::on_pushButtonWithdraw300_clicked()
{
    withDraw(300);
}
void POSMainWindow::on_pushButtonWithdraw400_clicked()
{
    withDraw(400);
}
void POSMainWindow::on_pushButtonWithdraw500_clicked()
{
    withDraw(500);
}
void POSMainWindow::on_pushButtonWithdraw1000_clicked()
{
    withDraw(1000);
}
void POSMainWindow::on_pushButtonWithdrawExit_clicked()
{
    exitPOS();
}

//////////////////////////////////////////////////////////////////////////////
// Status: Continue, Exit

void POSMainWindow::on_pushButtonStatusContinue_clicked()
{
    ui->stackedWidgetDisp->setCurrentIndex(2);
    resetTimer(15);
}
void POSMainWindow::on_pushButtonStatusExit_clicked()
{
    exitPOS();
}

//////////////////////////////////////////////////////////////////////////////
// Deposit: Cancel, OK

void POSMainWindow::on_pushButtonDepositCancel_clicked()
{
    clearDepositPage();
    ui->labelStatus->setText("Cancel");
    ui->labelStatusMsg->setText("Please get the cash back from the slot.");
    ui->stackedWidgetDisp->setCurrentIndex(4);
    resetTimer(15);
}
void POSMainWindow::on_pushButtonDepositOK_clicked()
{
    deposit();
}

//////////////////////////////////////////////////////////////////////////////
// Button Cancel, Clear, Enter, Insert

void POSMainWindow::on_pushButtonCancel_clicked()
{
    if (ui->stackedWidgetDisp->currentIndex() != 0 && ui->stackedWidgetDisp->currentIndex() != 6)
    {
        exitPOS();
    }
}
void POSMainWindow::on_pushButtonClear_clicked()
{
    resetTimer(15);
    if (ui->stackedWidgetDisp->currentIndex() == 1)
    {
        clearLoginPage();
    }
    else if (ui->stackedWidgetDisp->currentIndex() == 3)
    {
        clearWithdrawPage();
    }
}
void POSMainWindow::on_pushButtonEnter_clicked()
{
    if (ui->stackedWidgetDisp->currentIndex() == 1)
    {
        if (!strcmp(ui->lineEditPwd->text().toStdString().data(), ar->passwd))
        {
            clearLoginPage();
            // open client log
            if ((logfp = fopen(cr->path, "a+"))== NULL)
            {
                fprintf(stderr, "ERROR: ");
                perror(cr->path);
                return;
            }

            // is there any records left in the client*.log?
            log_buffer = (LogRecord *)malloc(LOGRECORD_SIZE);
            if ((logpos = findLog(logfp, ar->cardId, log_buffer)) >= 0)
            // if yes, update the balance instead of which in the accounts file.
            {
                strcpy(ar->balance, log_buffer->balance);
            }
            free(log_buffer);

            // lock account record
            fprintf(stdout, "DEBUG: try to lock account record.\n");
            if (PWRITE_LOCK(acfp, acpos, SEEK_SET, ACCRECORD_SIZE) < 0)
            {
                perror("ERROR: lock account record failed: fcntl");
                return;
            }
            fprintf(stdout, "DEBUG: account record locked.\n");

            // lock client log file
            fprintf(stdout, "DEBUG: try to lock client log.\n");
            if (WRITEW_LOCK(logfp) < 0)
            {
                perror("ERROR: lock client log failed: fcntl");
                return;
            }
            fprintf(stdout, "DEBUG: client log locked.\n");

            ui->stackedWidgetDisp->setCurrentIndex(2);
            resetTimer(15);
        }
        else
        {
            resetTimer(15);
            clearLoginPage();
            ui->labelLoginMsg->setText("Wrong Password!");
        }
    }
    else if (ui->stackedWidgetDisp->currentIndex() == 3)
    {
        bool ok;
        int amount = lineEditValue.toInt(&ok);
        if (amount % 100 == 0)
        {
            withDraw(amount);
        }
        else
        {
            clearWithdrawPage();
            ui->labelWithdrawTips->setText("Wrong Amount!\nMultiples of 100 required.");
            resetTimer(15);
        }
    }
    else if (ui->stackedWidgetDisp->currentIndex() == 5)
    {
        deposit();
    }
}
void POSMainWindow::on_pushButtonInsert_clicked()
{
    if (ui->stackedWidgetDisp->currentIndex() == 0)
    // New Login
    {
        // Search Account ID
        if ((acfp = fopen(ACCOUNTS_PATH, "a+")) == NULL)
        {
            perror("ERROR: accounts");
            return;
        }
        char cardId[CARDID_LENGTH*2];
        strcpy(cardId, ui->lineEditBottom->text().toStdString().data());
        memset((AccountRecord *)ar, 0, ACCRECORD_SIZE);
        acpos = findAccount(acfp, cardId, ar);

        if (acpos >= 0)
        // Account found
        {
            // is there anyone accessing this account?
            pid_t lockstate;
            if ((lockstate = PIS_WRITELOCK(acfp, acpos, SEEK_SET, ACCRECORD_SIZE)) < 0)
            {
                fprintf(stderr, "ERROR: getting account record locking state failed.\n");
                return;
            }
            else if (lockstate > 0)
            {
                fprintf(stderr, "ERROR: pid=%u is accessing this account.\n", lockstate);
                ui->labelInsertMsg->setText("Someone is Accessing this Account.");
                return;
            }

            ui->stackedWidgetDisp->setCurrentIndex(1);        
            timer->start(15000);
            clearLoginPage();
        }
        else
        // Account not found
        {
            ui->lineEditBottom->setText("Wrong Account No.!");
            fclose(acfp);
        }
    }
    else if (ui->stackedWidgetDisp->currentIndex() == 6)
    {
        ui->pushButtonInsert->setText("Insert");
        ui->labelExitTitle->setText("Thank you for Using POS");
        ui->lineEditBottom->setReadOnly(false);
        ui->lineEditBottom->setText("Input Account Number");
        ui->stackedWidgetDisp->setCurrentIndex(0);
        timer->stop();
    }
}

bool POSMainWindow::eventFilter(QObject *target, QEvent *event)
{
    if (target == ui->lineEditBottom && event->type() == QEvent::FocusIn)
    {
        ui->lineEditBottom->setText("");
        if (ui->stackedWidgetDisp->currentIndex() == 0)
            ui->labelInsertMsg->setText("Please Insert Your Card...");
    }
    return QMainWindow::eventFilter(target, event);
}

void POSMainWindow::withDraw(int amount)
{
    clearWithdrawPage();
    double balance = atof(ar->balance);
    if (balance >= amount)
    {
        // save log
        log_buffer = (LogRecord *)malloc(LOGRECORD_SIZE);
        strcpy(log_buffer->cardId, ar->cardId);     // update cardId
        log_buffer->type = WITHDRAW;                // update type
        sprintf(log_buffer->amount, "%11.2lf", (double)amount);     // update amount
        sprintf(log_buffer->balance, "%11.2lf", balance - amount);  // update balance
        strcpy(log_buffer->newpwd, "");             // interface to change passwd
        strcpy(ar->balance, log_buffer->balance);   // update ar->balance
        writeLog(logfp, log_buffer);
        fprintf(stderr, "WITHDRAW, amount=%11.2lf log=", (double)amount);
        printLog(log_buffer);
        free(log_buffer);

        ui->labelStatus->setText("Cash Out");
        ui->labelStatusMsg->setText("Please take out.");
        ui->stackedWidgetDisp->setCurrentIndex(4);
        resetTimer(15);
    }
    else
    {
        ui->labelStatus->setText("Overdraw!");
        ui->labelStatusMsg->setText("No enough money on your account.");
        ui->stackedWidgetDisp->setCurrentIndex(4);
        resetTimer(15);
    }
}
void POSMainWindow::deposit()
{
    double balance = atof(ar->balance);
    bool ok;
    long amount = ui->lineEditBottom->text().toLong(&ok);
    if (ok && amount % 100 == 0)
    {
        log_buffer = (LogRecord *)malloc(LOGRECORD_SIZE);
        strcpy(log_buffer->cardId, ar->cardId);     // update cardId
        log_buffer->type = DEPOSIT;                 // update type
        sprintf(log_buffer->amount, "%11.2lf", (double)amount);     // update amount
        sprintf(log_buffer->balance, "%11.2lf", balance + amount);  // update balance
        strcpy(log_buffer->newpwd, "");             // interface to change passwd
        strcpy(ar->balance, log_buffer->balance);   // update ar->balance
        writeLog(logfp, log_buffer);
        fprintf(stderr, "DEPOSIT , amount=%11.2lf log=", (double)amount);
        printLog(log_buffer);
        free(log_buffer);

        // Handling the displaying area
        ui->labelStatus->setText("Cash in");
        QString msg = ui->lineEditBottom->text();
        msg.append(" RMB saved.");
        ui->labelStatusMsg->setText(msg);

        // Handling the bottom.
        ui->labelBottom->setText("");
        ui->lineEditBottom->setReadOnly(true);
        ui->lineEditBottom->setText("");

        ui->stackedWidgetDisp->setCurrentIndex(4);
        resetTimer(15);
    }
    else
    {
        ui->lineEditBottom->setText("Wrong Amount!");
        resetTimer(15);
    }
}
void POSMainWindow::exitPOS()
{
    //if (PIS_WRITELOCK(acfp, acpos, SEEK_SET, ACCRECORD_SIZE) > 0)
    //{
        PUN_LOCK(acfp, acpos, SEEK_SET, ACCRECORD_SIZE);
        fprintf(stdout, "DEBUG: account record unlocked.\n");
        fclose(acfp);
    //}
    //if (IS_WRITELOCK(logfp) > 0)
    //{
        fclose(logfp);
        UN_LOCK(logfp);
        fprintf(stdout, "DEBUG: client log unlocked.\n");
    //}

    ui->pushButtonInsert->setText("OK");
    ui->stackedWidgetDisp->setCurrentIndex(6);
    resetTimer(15);
}

void POSMainWindow::clearLoginPage()
{
    lineEditValue = "";
    ui->labelBottom->setText("");
    ui->lineEditBottom->setText("");
    ui->lineEditBottom->setReadOnly(true);
    ui->lineEditPwd->setText("");
    ui->labelLoginMsg->setText("");
    ui->pushButtonInsert->setText("");
}
void POSMainWindow::clearWithdrawPage()
{
    lineEditValue = "";
    ui->lineEditWithdrawAmount->setText("");
    ui->labelWithdrawTips->setText("Choose or Input amount.\nAmount should be multiples of 100");
}
void POSMainWindow::clearDepositPage()
{
    ui->labelBottom->setText("");
    ui->lineEditBottom->setText("");
    ui->lineEditBottom->setReadOnly(true);
}

void POSMainWindow::timeoutslot()
{
    ui->labelExitTitle->setText("Timeout!");
    ui->stackedWidgetDisp->setCurrentIndex(6);
    exitPOS();
}

void POSMainWindow::resetTimer(int interval)
{
    timer->stop();
    timer->start(interval*1000);
}
