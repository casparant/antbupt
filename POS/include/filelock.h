/*
 * Copyright (C) by Jiawei Chen, Rui Tao
 */
#ifndef _FILELOCK_H
#define _FILELOCK_H

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

int lock_region(int fd, int cmd, int type, off_t offset, int whence, off_t len);
int lock_test(int fd, int cmd, int type, off_t offset, int whence, off_t len);

#define READ_LOCK(FP) \
    lock_region(fileno(FP), F_SETLK, F_RDLCK, 0, SEEK_SET, 0)
#define READW_LOCK(FP) \
    lock_region(fileno(FP), F_SETLKW, F_RDLCK, 0, SEEK_SET, 0)
#define WRITE_LOCK(FP) \
    lock_region(fileno(FP), F_SETLK, F_WRLCK, 0, SEEK_SET, 0)
#define WRITEW_LOCK(FP) \
    lock_region(fileno(FP), F_SETLKW, F_WRLCK, 0, SEEK_SET, 0)
#define UN_LOCK(FP) \
    lock_region(fileno(FP), F_SETLK, F_UNLCK, 0, SEEK_SET, 0)
#define IS_WRITELOCK(FP) \
    lock_test(fileno(FP), F_GETLK, F_WRLCK, 0, SEEK_SET, 0)

#define PREAD_LOCK(FP, OFFSET, WHENCE, LEN) \
	lock_region(fileno(FP), F_SETLK, F_RDLCK, OFFSET, WHENCE, LEN)
#define PREADW_LOCK(FP, OFFSET, WHENCE, LEN) \
    lock_region(fileno(FP), F_SETLKW, F_RDLCK, OFFSET, WHENCE, LEN)
#define PWRITE_LOCK(FP, OFFSET, WHENCE, LEN) \
	lock_region(fileno(FP), F_SETLK, F_WRLCK, OFFSET, WHENCE, LEN)
#define PWRITEW_LOCK(FP, OFFSET, WHENCE, LEN) \
    lock_region(fileno(FP), F_SETLKW, F_WRLCK, OFFSET, WHENCE, LEN)
#define PUN_LOCK(FP, OFFSET, WHENCE, LEN) \
	lock_region(fileno(FP), F_SETLK, F_UNLCK, OFFSET, WHENCE, LEN)
#define PIS_WRITELOCK(FP, OFFSET, WHENCE, LEN) \
    lock_test(fileno(FP), F_GETLK, F_WRLCK, OFFSET, WHENCE, LEN)

#endif /* _FILELOCK_H */
