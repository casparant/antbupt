/*
 * Copyright(C) by Jiawei Chen
 */
#ifndef _POSTYPE_H_
#define _POSTYPE_H_

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>

/* The length of card id appeared in the most struct above */
#define CARDID_LENGTH           16
/* The length of the path of some client log files */
#define CLIENT_PATH_LENGTH      32
/* The length of the password */
#define PASSWD_LENGTH           6
/* Location of file 'accounts', records accounts info and balance */
#define ACCOUNTS_PATH           "./accounts"
/* Location of file 'server.log', records business info from all clients */
#define SERVER_LOG_PATH         "./server.log"
/* Location of file 'clients', records client log file locations */
#define CLIENTS_PATH            "./clients"
/* Location of directory 'client.log.d', records client log files */
#define CLIENTS_DIR             "./client.log.d"

/* Some Format of log structs */
#define LOG_FORMAT_READ         "%s %c%s %s %s\n"
#define LOG_FORMAT_WRITE        "%-17s%c%-12s%-12s%-7s\n"

#define ACC_FORMAT_READ         "%c%s %s %s\n"
#define ACC_FORMAT_WRITE        "%c%-17s%-7s%-12s\n"

#define CLIENT_FORMAT_READ      "%s %s %s\n"
#define CLIENT_FORMAT_WRITE     "%-8s%-32s%-8s\n"

#ifndef false
#define false 0
#endif
#ifndef true
#define true 1
#endif

typedef enum
{
    WITHDRAW    = '0',
    DEPOSIT     = '1',
    TRANSFER    = '2',
    CHANGEPWD   = '3'
} menu_type;

/* This struct will be written into and read from the file "client*.log"
 * and "server.log" */
struct log_record
{
    /* A transaction is done with a card id */
    char    cardId[CARDID_LENGTH+1];

    /* menu_type */
    char    type;

    /* transaction amount */
    char    amount[12];

    /* balance */
    char    balance[12];

    /* pwd */
    char    newpwd[PASSWD_LENGTH+1];
};
typedef struct log_record LogRecord;

/* This struct will be written into and read from the file "accounts" */
struct account_record
{
    /* the account is suspended or not*/
    char    suspended;

    /* card id and password */
    char    cardId[CARDID_LENGTH+1];
    char    passwd[PASSWD_LENGTH+1];
    /* balance */
    char    balance[12];
};
typedef struct account_record AccountRecord;

/* This struct will be written into and read from the file "clients" */
struct client
{
    /* specify the client number */
    char    clientNo[8];

    /* the path of "client*.log" located. */
    char    path[CLIENT_PATH_LENGTH];

    /* who is using this client */
    char    pid[8];

    struct client *next;
};
typedef struct client Client;

/* Size of struct log_record */
#define LOGRECORD_SIZE  sizeof(struct log_record) + 1
/* Size of struct account_record */
#define ACCRECORD_SIZE  sizeof(struct account_record) + 1
/* Size of struct client */
#define CLIENT_SIZE     sizeof(struct client)
/* Size of struct client, excluding *next */
#define CLIENT_STRUCT_SIZE  sizeof(struct client)-sizeof(struct client*) + 1

/*****************************************************************************/

int     readAccount (FILE*, AccountRecord*);
int     writeAccount(FILE*, AccountRecord*);
long    findAccount (FILE*, char*, AccountRecord*);
int     readLog (FILE*, LogRecord*);
int     writeLog(FILE*, LogRecord*);
void    printLog(LogRecord*);

int     findAvailClientSeq(FILE*, Client*);
int     writeClients(FILE*, Client*);
int     readClients(FILE*, Client*);

/*****************************************************************************/

#endif /* _POSTYPE_H_ */
