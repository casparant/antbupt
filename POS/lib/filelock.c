/*
 * Copyright(C) by Jiawei Chen, Rui Tao
 */
#include <filelock.h>

/*
 * cmd : F_GETLK, F_SETLK, F_SETLKW
 * type : Readlock : F_RDLCK, Writelock : F_WRLCK, Unlock : F_UNLCK
 * offset : offset in bytes
 */
int lock_region(int fd, int cmd, int type, off_t offset, int whence, off_t len)
{
    struct flock lock;

    lock.l_type   = type;
    lock.l_whence = whence;
    lock.l_start  = offset;
    lock.l_len    = len;

    return fcntl(fd, cmd, &lock);
}

int lock_test(int fd, int cmd, int type, off_t offset, int whence, off_t len)
{
    struct flock lock;

    lock.l_type   = type;
    lock.l_whence = whence;
    lock.l_start  = offset;
    lock.l_len    = len;

    /*
     * if fcntl error, return error.
     * if not locked, return 0.
     * if locked, return pid
     */
    int ret_val = fcntl(fd, cmd, &lock);

    if (ret_val < 0)
    {
        perror("ERROR: fcntl");
        return ret_val;
    }
    if (lock.l_type == F_UNLCK)
        return 0;

    return lock.l_pid;
}
