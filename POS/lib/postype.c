/*
 * Copyright(C) by Jiawei Chen, Rui Tao
 */
#include <postype.h>
#include <filelock.h>

int readAccount(FILE* fp, AccountRecord* account)
{
    int     fdes = fileno(fp);
    char    buffer[ACCRECORD_SIZE+1];
    int     bytes;

    bytes = read(fdes, buffer, ACCRECORD_SIZE);
    sscanf(buffer, ACC_FORMAT_READ, &(account->suspended), account->cardId, account->passwd, account->balance);

    return bytes;
}

int writeAccount(FILE* fp, AccountRecord* account)
{
    int     fdes = fileno(fp);
    char    buffer[ACCRECORD_SIZE+1];
    int     bytes;

    sprintf(buffer, ACC_FORMAT_WRITE, account->suspended, account->cardId, account->passwd, account->balance);
    bytes = write(fdes, buffer, ACCRECORD_SIZE);

    return bytes; 
}

/* -1: not found, other: the location of account. */
long findAccount(FILE* fp, char* cardId, AccountRecord* ar)
{
    int bytes, totalBytes = 0;

    fseek(fp, 0, SEEK_SET);
    while((bytes = readAccount(fp, ar)) > 0)
    {
        totalBytes += bytes;
        if(!strcmp(ar->cardId, cardId))
        {
            return totalBytes-bytes;
        }
        bytes = 0;
        bzero(ar, ACCRECORD_SIZE);
    }
    return -1;
}

void printAccount(AccountRecord* account)
{
    fprintf(stdout, ACC_FORMAT_WRITE, account->suspended, account->cardId, account->passwd, account->balance);
}

/* Client Log Files */

int writeLog(FILE* fp, LogRecord* log)
{
    int     fdes = fileno(fp);
    char    buffer[LOGRECORD_SIZE+1];
    int     bytes;

    sprintf(buffer, LOG_FORMAT_WRITE, log->cardId, log->type, log->amount, log->balance, log->newpwd);

    bytes = write(fdes, buffer, LOGRECORD_SIZE);
    return bytes;
}

int readLog(FILE* fp, LogRecord* log)
{
    int     fdes = fileno(fp);
    char    buffer[LOGRECORD_SIZE+1];
    int     bytes;

    bytes = read(fdes, buffer, LOGRECORD_SIZE);
    sscanf(buffer, LOG_FORMAT_READ, log->cardId, &(log->type), log->amount, log->balance, log->newpwd);

    return bytes;
}

/* -1: not found, other: the location of account. */
long findLog(FILE* fp, char* cardId, LogRecord* log)
{
    int bytes, offset = 0, totalBytes = 0, found = false;

    fseek(fp, 0, SEEK_SET);
    /* while !FEOF */
    while((bytes = readLog(fp, log)) > 0)
    {
        offset += bytes;
        if(!strcmp(log->cardId, cardId))
        {
            totalBytes = offset-bytes;
            found = 1;
            continue;
        }
        bytes = 0;
        bzero(log, LOGRECORD_SIZE);
    }

    return (found ? totalBytes : -1);
}

void printLog(LogRecord* log)
{
    fprintf(stdout, LOG_FORMAT_WRITE, log->cardId, log->type, log->amount, log->balance, log->newpwd);
}

int readClients(FILE *fp, Client *cr)
{
    int     fdes = fileno(fp);
    char    buffer[CLIENT_STRUCT_SIZE+1];
    int     bytes;

    bytes = read(fdes, buffer, CLIENT_STRUCT_SIZE);
    sscanf(buffer, CLIENT_FORMAT_READ, cr->clientNo, cr->path, cr->pid);

    return bytes;
}

int writeClients(FILE *fp, Client *cr)
{
    int     fdes = fileno(fp);
    char    buffer[CLIENT_STRUCT_SIZE+1];
    int     bytes;

    sprintf(buffer, CLIENT_FORMAT_WRITE, cr->clientNo, cr->path, cr->pid);
    bytes = write(fdes, buffer, CLIENT_STRUCT_SIZE);

    return bytes;
}

int findAvailClientSeq(FILE* fp, Client* cr)
{
    int bytes, totalBytes = 0;

    fseek(fp, 0, SEEK_SET);
    while((bytes = readClients(fp, cr)) > 0)
    {
        totalBytes += bytes;
        if(!strcmp(cr->pid, "0"))
        {
            return totalBytes-bytes;
        }
        bytes = 0;
        bzero(cr, CLIENT_STRUCT_SIZE);
    }
    return -1;
}
