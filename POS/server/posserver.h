/*
 * Copyright(C) 2009 by Jiawei Chen, Rui Tao
 */

#ifndef _POSSERVER_H
#define _POSSERVER_H

#include <postype.h>
#include <filelock.h>

#define MESSAGE     "The server is fetching clients log files..\n"

/* Load all clients in file "clients" */
int loadClients();

/* This function will be executed every 10 seconds to fetching info
 * from "client*.log" one by one.
 */
void readLogs();

/* This function updates the account balance in the file "accounts" */
int updateAccount(FILE *, long, LogRecord*, AccountRecord*);

Client *client_table = NULL, *client_table_tail = NULL;

#endif
