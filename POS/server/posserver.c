/*
 * Copyright(C) 2009 by Jiawei Chen, Rui Tao
 */

#include "posserver.h"

int main()
{
    /* try to start daemon mode. */
    if(daemon(1, 1) == -1)
    {
        perror("daemon");
        return -1;
    }
    fprintf(stdout, "DEBUG: daemon ok.\n");
    
    /* load all clients */
    if(loadClients() == -1)
        return -1;

    /* loop for fetching client log every 10 seconds */
    while(true)
    {
        sleep(10); 
        readLogs();
    }
    return 0;
}

int loadClients()
{
    FILE    *clientfp;
    int     bytes = 0;
    Client  *tmp_client;

    /* file "clients" records some client*.log paths. */
    fprintf(stdout, "DEBUG: Opening file \"%s\".\n", CLIENTS_PATH);
    if((clientfp = fopen(CLIENTS_PATH, "r")) == NULL)
    {
        perror(CLIENTS_PATH);
        return -1;
    }

    fprintf(stdout, "------------ loading client table ------------\n");
    
    client_table = (Client *)malloc(CLIENT_SIZE);
    client_table->next = NULL;
    client_table_tail = client_table;
    tmp_client = (Client *)malloc(CLIENT_SIZE);
    while((bytes = readClients(clientfp, tmp_client)) > 0)
    {
        fprintf(stdout, "%8s:%-32s\n", tmp_client->clientNo, tmp_client->path);
        tmp_client->next = NULL;
        client_table_tail->next = tmp_client;
        client_table_tail = client_table_tail->next;
        tmp_client = (Client *)malloc(CLIENT_SIZE);
    }
    fprintf(stdout, "------------ finish loading table ------------\n");

    fprintf(stdout, "DEBUG: Closing file \"%s\".\n", CLIENTS_PATH);
    fclose(clientfp);
    return 0;
}

void readLogs()
{
    FILE            *serverfp, *accountsfp, *clientfp;
    Client          *current_client = client_table->next;
    LogRecord       *log_buffer;
    AccountRecord   *acc_buffer;
    int             accountBytes = 0, logBytes = 0, totalLogBytes = 0;
    long            acpos;

    /* open "accounts" in r+ mode */
    fprintf(stdout, "DEBUG: Opening file \"%s\".\n", ACCOUNTS_PATH);
    if((accountsfp = fopen(ACCOUNTS_PATH, "r+")) == NULL)
    {
        perror(ACCOUNTS_PATH);
        return;
    }

    /* open "server.log" in appending mode */
    fprintf(stdout, "DEBUG: Opening file \"%s\" to append.\n", SERVER_LOG_PATH);
    if((serverfp = fopen(SERVER_LOG_PATH, "a")) == NULL)
    {
        perror(SERVER_LOG_PATH);
        return;
    }

    /* rolling the clients */
    while(current_client != NULL)
    {
        fprintf(stdout, "Current client:%8s:%-32s\n", current_client->clientNo, current_client->path);
        /* open client log file */
        fprintf(stdout, "DEBUG: Opening file \"%s\".\n", current_client->path);
        if((clientfp = fopen(current_client->path, "r+")) == NULL)
        {
            perror(current_client->path);
            current_client = current_client->next;
            continue;
        }

        /* Set blocking lock, if client is working, wait until it finished. */
        WRITEW_LOCK(clientfp);
        fprintf(stdout, "DEBUG: %s locked.\n", current_client->path);

        log_buffer = (LogRecord *)malloc(LOGRECORD_SIZE);
        acc_buffer = (AccountRecord *)malloc(ACCRECORD_SIZE);
        fprintf(stdout, "Try to fetch records from client...\n");
        while((logBytes = readLog(clientfp, log_buffer)) > 0)
        {
            totalLogBytes += logBytes;
            /* print log */
            fprintf(stdout, "Record : ");
            printLog(log_buffer);
            /* find account related to this record */
            if((acpos = findAccount(accountsfp, log_buffer->cardId, acc_buffer)) < 0)
            {
                if (acpos == -2)
                {
                    fprintf(stderr, "Other people is accessing this account, wait.\n");
                    continue;
                }
                if (acpos == -1)
                {
                    fprintf(stderr, "Account not exist.\n");
                    continue;
                }
            }
            if(acc_buffer->suspended == 1)
            {
                fprintf(stderr, "Account is suspended.\n");
                continue;
            }
            fprintf(stdout, "Account: ");
            printAccount(acc_buffer);

            /* update account in file "accouts" */
            updateAccount(accountsfp, acpos, log_buffer, acc_buffer);

            /* append logs to file "server.log" */
            writeLog(serverfp, log_buffer);
            log_buffer = (LogRecord *)malloc(LOGRECORD_SIZE);
            acc_buffer = (AccountRecord *)malloc(ACCRECORD_SIZE);
        }
        free(log_buffer);
        free(acc_buffer);
        fprintf(stdout, "Fetching finished.\n");
        UN_LOCK(clientfp);
        fprintf(stdout, "DEBUG: %s unlocked.\n", current_client->path);

        /* clean and close "client*.log" and go to next client*.log */
        ftruncate(fileno(clientfp), 0);
        fclose(clientfp);
        fprintf(stdout, "DEBUG: file \"%s\" closed.\n", current_client->path);
        fprintf(stdout, "DEBUG: Accessing next client log file.\n");
        current_client = current_client->next;
    } // End of while

    fclose(serverfp);
    fprintf(stdout, "DEBUG: file \"%s\" closed.\n", SERVER_LOG_PATH);

    fclose(accountsfp);
    fprintf(stdout, "DEBUG: file \"%s\" closed.\n", ACCOUNTS_PATH);
    return;
}

int updateAccount(FILE *accountsfp, long acpos, LogRecord *log_buffer, AccountRecord *acc_buffer)
{
    /* Lock current account record */
    PWRITEW_LOCK(accountsfp, acpos, SEEK_SET, ACCRECORD_SIZE);
    fprintf(stdout, "DEBUG: accounts locked.\n");
    /*
     * Update current account
     * There will be no overdraws because checking balance has been done
     * on the client side.
     */
    strcpy(acc_buffer->balance, log_buffer->balance);
    fseek(accountsfp, acpos, SEEK_SET);
    writeAccount(accountsfp, acc_buffer);
    fprintf(stdout, "DEBUG: account updated.\n");

    /* Unlock current account record */
    PUN_LOCK(accountsfp, acpos, SEEK_SET, ACCRECORD_SIZE);
    fprintf(stdout, "DEBUG: accounts unlocked.\n");

    return 0;
}

