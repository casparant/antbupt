/*
 * jimu.c
 *
 * Copyright (C) 2009, Caspar Zhang: casparant@gmail.com
 *
 */

/*
 * This is a jimu problem. there are 12 building blocks with different shapes,
 * what we should do is to put them together to make up a 3*4*5 cude.
 * The 12 diffent building blocks is listed before every function sequencially.
 * The 3*4*5 is mark by a number from 0 to 59 below.
 * The direction 0-1-2-3-4... in the following figure is defined as x-direction
 * The direction 0-5-10... is defined as y-direction
 * The direction 0-15-30-45... is defined as z-direction(plz imagine this)
 * Every building block is separated into 5 units.
 * The palce in the cube of some units can be set first 
 * while the rest should be calculated later.
 * the calcuation is based on the following arrays.
 *
 */

/*****************************************************************************
 *                *                *                *                *
 * 10 11 12 13 14 * 25 26 27 28 29 * 40 41 42 43 44 * 55 56 57 58 59 *
 *  5  6  7  8  9 * 20 21 22 23 24 * 35 36 37 38 39 * 50 51 52 53 54 *
 *  0  1  2  3  4 * 15 16 17 18 19 * 30 31 32 33 34 * 45 46 47 48 49 *
 *                *                *                *                *
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>


/* 0-3 is one side and 4-7 is the other side */
int avail_24_0_15[4][8] =
{
    {-5, 5, -15, 15, -2, -8, -12, 18}, // 2 - x - e
    {-1, 1, -5, 5, 44, 46, 40, 50}, // 2 - z - e

    {-4, 6, -14, 16, -3, 7, -13, 17}, // 3 - x - e 
    {14, 16, 10, 20, 29, 31, 25, 35} // 3 -z - e
};

int avail_36_0_30[8][8] =
{
    {-5, 5, -15, 15, -3, 7, -13, 17}, // 4 - x - d
    {-6, 4, -16, 14, -2, 8, -12, 18}, // 4 - x - e
    {-1, 1, -5, 5, 29, 31, 25, 35}, // 4 - z - d
    {-16, -14, -20, -10, 44, 46, 40, 50}, // 4 - z - e

    {-5, 5, -15, 15, -3, 7, -13, 17}, // 5 - x - d
    {-10, 10, -30, 30, -8, 12, -28, 32}, // 5 - x - e
    {-1, 1, -5, 5, 29, 31, 25, 35}, // 5 - z - d
    {-2, 2, -10, 10, 28, 32, 20, 40} // 5 - z - e
};

int avail_36_20_30[36][8] =
{
    {-5, 5, -15, 15, -3, 7, -13, 17}, // 6 - x - d
    {-4, 6, -14, 16, -4, 6, -14, 16}, // 6 - x - e
    {-1, 1, -15, 15, 9, 11, -5, 25},  // 6 - y - d
    {4, 6, -10, 20, 4, 6, -10, 20},   // 6 - y - e
    {-1, 1, -5, 5, 29, 31, 25, 35}, // 6 - z - d
    {14, 16, 10, 20, 14, 16, 10, 20}, // 6 - z - e

    {-5, 5, -15, 15, -3, 7, -13, 17}, // 7 - x - d
    {6, -4, 16, -14, 6, -4, 16, -14}, // 7 - x - e
    {-1, 1, -15, 15, 9, 11, -5, 25}, // 7 - y - d
    {6, 4, -10, 20, 6, 4, 20, -10}, // 7 - y - e
    {-1, 1, -5, 5, 29, 31, 25, 35}, // 7 - z - d
    {16, 14, 20, 10, 16 ,14, 20, 10}, // 7 - z - e

    {-5, 5, -15, 15, 0, 0, 0, 0}, // 8 - x - d
    {-3, 7, -13, 17, 0, 0, 0, 0}, // e
    {-1, 1, -15, 15, 0, 0, 0, 0}, // 8 - y - d  
    {9, 11, -5, 25, 0, 0, 0, 0}, // e
    {-1, 1, -5, 5, 0, 0, 0, 0},  // 8 - z - d
    {29, 31, 25, 35, 0, 0, 0, 0}, // e

    {-5, 5, -15, 15, 0, 0, 0, 0}, // 9
    {7, -3, 17, -13, 0, 0, 0, 0},
    {-1, 1, -15, 15, 0, 0, 0, 0},
    {11, 9, 25, -5, 0, 0, 0, 0},
    {-1, 1, -5, 5, 0, 0, 0, 0},
    {31, 29, 35, 25, 0, 0, 0, 0},

    {-4, 6, -14, 16, 0, 0, 0, 0}, // 10
    {-9, 11, -29, 31, 0, 0, 0, 0},
    {4, 6, -15, 15, 0, 0, 0, 0},
    {3, 7, -30, 30, 0, 0, 0, 0},
    {14, 16, 10, 20, 0, 0, 0, 0},
    {13, 17, 5, 25, 0, 0, 0, 0},

    {-4, -14, 0, 0, 0, 0, 0, 0}, // 11
    {6, 16, 0, 0, 0, 0, 0, 0},
    {4, -10, 0, 0, 0, 0, 0, 0},
    {6, 20, 0, 0, 0, 0, 0, 0},
    {14, 10, 0, 0, 0, 0, 0, 0},
    {16, 20, 0, 0, 0, 0, 0, 0}
};

int avail_48_0_45[9][8] =
{
    {6, -4, 16, -14, -5, 5, 15, -15}, // 12 - x - c
    {7, -3, 17, -13, -6, 4, 14, -16}, // d
    {12, -8, 32, -28, -11, 9, 29, -31}, // e

    {20, 10, 16, 14, 1, -1, 5,-5}, // 12 - z - c
    {35, 25, 31, 29, -14, -16, -10, -20}, // d
    {40, 20, 32, 28, -13, -17, -5, -25}  // e
};

int jimu[60];
int result[13][5];

int try_12_0_0(int nr);
int try_24_0_15(int nr, int a, int b, int c, int d, int e);
int try_36_0_30(int nr, int a, int b, int c, int d, int e);
int try_36_20_30(int nr, int a, int b, int c, int d, int e);
int try_48_0_45(int nr, int a, int b, int c, int d, int e);

#define PRINT_ONE printf("#%-3d%3d%3d%3d%3d%3d\n", nr, a, b, c, d, e)

#define OCCUPY_ONE jimu[a] = jimu[b] = jimu[c] = jimu[d] = jimu[e] = 0

#define RESTORE_ONE jimu[a] = jimu[b] = jimu[c] = jimu[d] = jimu[e] = 1

#define UPDATE_RES result[nr][0] = a; result[nr][1] = b; result[nr][2] = c; result[nr][3] = d; result[nr][4] = e;

#define MSG_JUMP(a) printf("jump to #%-3d\n", a);

#define MSG_TRY(a) printf("try %s direction\n", a);

#define MSG_BACK(a) printf("jump back to #%-3d\n", a);

#define SAME_FACE_XY(a, b) (a / 15 == b / 15)

#define SAME_FACE_XZ(a, b) ((a % 15 < 5 && b % 15 < 5) || \
        (a % 15 >= 5 && a % 15 < 10 && b % 15 >= 5 && b % 15 < 10) || \
        (a % 15 >= 10 && b % 15 >= 10))

#define SAME_FACE_YZ(a, b) (a % 5 == b % 5)

#define SAME_LINE_X(a, b) (a / 5 == b / 5)

#define SAME_LINE_Y(a, b) (a % 5 == b % 5 && a / 15 == b / 15)

#define SAME_LINE_Z(a, b) (a % 15 == b % 15)

int main()
{
    int i;

    if (try_12_0_0(1) == 1)
    {
        printf("succeeded.\n");
        for (i = 1; i <= 12; ++i)
            printf("#%-3d%3d%3d%3d%3d%3d\n", i, result[i][0], result[i][1], result[i][2], result[i][3], result[i][4]);
    }
    else
        printf("failed.\n");
    return 0;
}

/************************************************
 * 1
 ************************************************
 * ooooo
 ***********************************************/
int try_12_0_0(int nr) // nr = 1
{
    int i, j;
    int a, b, c, d, e;

    i = 0;
    while (i < 12)
    {
        for (j = 0; j < 60; ++j)
            jimu[j] = 1; // 1-available; 0-occupied

        MSG_JUMP(nr);
        MSG_TRY("x"); // for #1, it can only try x
        a = i*5; b = a+1; c = b+1; d = c+1; e = d+1;
        OCCUPY_ONE;
        PRINT_ONE;
        if (try_24_0_15(2, a, b, c, d, e) == 1)
        {
            UPDATE_RES;
            return 1;
        }
        else
        {
            MSG_BACK(nr);
            RESTORE_ONE;
        }
    }

    return 0;
}

/************************************************
 * 2:   * 3:
 ************************************************
 * oooo * oooo
 * o    *  o
 ***********************************************/
int try_24_0_15(int nr, int a, int b, int c, int d, int e)
{
    int i, j, k;

    k = (nr - 2) * 2; // k = 0, 2

    if (nr == 4)
    {
        if (try_36_0_30(4, a, b, c, d, e) == 1)
            return 1;
        else
            return 0;
    }

    MSG_JUMP(nr);

    MSG_TRY("x");
    i = 0;
    while (i < 60)
    {
        a = i; b = a+1; c = b+1; d = c+1;
        if (jimu[a] && jimu[b] && jimu[c] && jimu[d])
        {
            for (j = 0; j < 8; ++j)
            {
                e = a + avail_24_0_15[k][j];
                if (e < 0 || e >= 60 || !jimu[e])
                    continue;
                if (!SAME_FACE_XY(a, e) && !SAME_FACE_XZ(a, e))
                    continue;
                if (nr == 2 && ((j < 4 && !SAME_LINE_Y(a, e) && !SAME_LINE_Z(a, e)) ||
                    (j >= 4 && !SAME_LINE_Y(d, e) && !SAME_LINE_Z(d, e))))
                    continue;
                if (nr == 3 && ((j < 4 && !SAME_LINE_Y(b, e) && !SAME_LINE_Z(b, e)) ||
                    (j >= 4 && !SAME_LINE_Y(c, e) && !SAME_LINE_Z(c, e))))
                    continue;
                OCCUPY_ONE;
                PRINT_ONE;
                if (try_24_0_15(nr+1, a, b, c, d, e) == 1)
                {
                    UPDATE_RES;
                    return 1;
                }
                else
                {
                    MSG_BACK(nr);
                    RESTORE_ONE;
                }
            }
        }
        // increase i
        if (i % 5 == 1)
            i+=4;
        else
            ++i;
    }

    MSG_TRY("z");
    i = 0;
    while (i < 15)
    {
        a = i; b = a+15; c = a+30; d = a+45;

        if (jimu[a] && jimu[b] && jimu[c] && jimu[d])
        {
            for (j = 0; j < 8; ++j)
            {
                e = a + avail_24_0_15[k+1][j];
                if (e < 0 || e >= 60 || !jimu[e])
                    continue;
                if (!SAME_FACE_XZ(a, e) && !SAME_FACE_YZ(a, e))
                    continue;
                if (nr == 2 && ((j < 4 && !SAME_LINE_X(a, e) && !SAME_LINE_Y(a, e)) ||
                    (j >= 4 && !SAME_LINE_X(d, e) && !SAME_LINE_Y(d, e))))
                    continue;
                if (nr == 3 && ((j < 5 && !SAME_LINE_X(b, e) && !SAME_LINE_Y(b, e)) ||
                    (j >= 4 && !SAME_LINE_X(c, e) && !SAME_LINE_Y(c, e))))
                    continue;
                OCCUPY_ONE;
                PRINT_ONE;
                if (try_24_0_15(nr+1, a, b, c, d, e) == 1)
                {
                    UPDATE_RES;
                    return 1;
                }
                else
                {
                    MSG_BACK(nr);
                    RESTORE_ONE;
                }
            }
        }

        // increase i;
        ++i;
    }
    return 0;
}

/************************************************
 * 4    * 5
 ************************************************
 *  ooo * ooo
 * oo   * o
 *      * o
 ***********************************************/
int try_36_0_30(int nr, int a, int b, int c, int d, int e)
{
    int i, j, k;

    k = (nr - 4) * 4; // k = 0, 4

    if (nr == 6)
    {
        if (try_36_20_30(6, a, b, c, d, e) == 1)
            return 1;
        else
            return 0;
    }

    MSG_JUMP(nr);

    MSG_TRY("x");
    i = 0;
    while (i < 60)
    {
        a = i; b = a+1; c = a+2;
        if (jimu[a] && jimu[b] && jimu[c])
        {
            for (j = 0; j < 8; ++j)
            {
                d = a + avail_36_0_30[k][j];
                if (d < 0 || d >= 60 || !jimu[d])
                    continue;
                if (!SAME_FACE_XY(b, d) && !SAME_FACE_XZ(b, d))
                    continue;
                if ((j < 4 && !SAME_LINE_Y(a, d) && !SAME_LINE_Z(a, d)) ||
                    (j >= 4 && !SAME_LINE_Y(c, d) && !SAME_LINE_Z(c, d)))
                    continue;
                e = a + avail_36_0_30[k+1][j];
                if (e < 0 || e >= 60 || !jimu[e])
                    continue;
                if (!SAME_FACE_XY(b, e) && !SAME_FACE_XZ(b, e))
                    continue;
                if (nr == 4 && !SAME_LINE_X(d, e))
                    continue;
                if (nr == 5 && !SAME_LINE_Y(d, e) && !SAME_LINE_Z(d, e))
                    continue;
                OCCUPY_ONE;
                PRINT_ONE;
                if (try_36_0_30(nr+1, a, b, c, d, e) == 1)
                {
                    UPDATE_RES;
                    return 1;
                }
                else
                {
                    MSG_BACK(nr);
                    RESTORE_ONE;
                }
            }
        }
        // increase i
        if (i % 5 == 2)
            i+=3;
        else
            ++i;
    }

    MSG_TRY("z");
    i = 0;
    while (i < 30)
    {
        a = i; b = a+15; c = a+30;
        if (jimu[a] && jimu[b] && jimu[c])
        {
            for (j = 0; j < 8; ++j)
            {
                d = a + avail_36_0_30[k+2][j];
                if (d < 0 || d >= 60 || !jimu[d])
                    continue;
                if (!SAME_FACE_XZ(b, d) && !SAME_FACE_YZ(b, d))
                    continue;
                if ((j < 4 && !SAME_LINE_X(a, d) && !SAME_LINE_Y(a, d)) ||
                    (j >= 4 && !SAME_LINE_X(c, d) && !SAME_LINE_Y(c, d)))
                    continue;
                e = a + avail_36_0_30[k+3][j];
                if (e < 0 || e >= 60 || !jimu[e])
                    continue;
                if (!SAME_FACE_XZ(b, e) && !SAME_FACE_YZ(b, e))
                    continue;
                if (nr == 4 && !SAME_LINE_Z(d, e))
                    continue;
                if (nr == 5 && !SAME_LINE_X(d, e) && !SAME_LINE_Y(d, e))
                    continue;
                OCCUPY_ONE;
                PRINT_ONE;
                if (try_36_0_30(nr+1, a, b, c, d, e) == 1)
                {
                    UPDATE_RES;
                    return 1;
                }
                else
                {
                    MSG_BACK(nr);
                    RESTORE_ONE;
                }
            }
        }
        // increase i
        ++i;
    }
    return 0;
}

/************************************************
 * 6   * 7   * 8   * 9   * 10  * 11
 ************************************************
 * ooo *  o  * ooo *   o * ooo *  o
 * oo  * ooo * o o * ooo *  o  * ooo
 *     * o   *     * o   *  o  *  o
 ***********************************************/
int try_36_20_30(int nr, int a, int b, int c, int d, int e) // nr = 6, 7, 8, 9, 10, 11
{
    int i, j, k, m;

    k = (nr - 6) * 6; // k = 0, 6, 12, 18, 24, 30

    if (nr == 12)
    {
        if (try_48_0_45(12, a, b, c, d, e) == 1)
            return 1;
        else
            return 0;
    }
    else if (nr == 11)
    {
        m = 2;
    }
    else if (nr > 7)
    {
        m = 4;
    }
    else
        m = 8;

    MSG_JUMP(nr);

    MSG_TRY("x");
    i = 0;
    while (i < 60)
    {
        a = i; b = a+1; c = a+2;
        if (jimu[a] && jimu[b] && jimu[c])
        {
            for (j = 0; j < m; ++j)
            {
                d = a + avail_36_20_30[k][j];
                if (d < 0 || d >= 60 || !jimu[d])
                    continue;
                if (!SAME_FACE_XY(a, d) && !SAME_FACE_XZ(a, d))
                    continue;
                if ((nr < 10 && j < 4 && !SAME_LINE_Y(a, d) && !SAME_LINE_Z(a, d)) ||
                        (nr < 10 && j >= 4 && !SAME_LINE_Y(c, d) && !SAME_LINE_Z(c, d)) ||
                        (nr >= 10 && !SAME_LINE_Y(b, d) && !SAME_LINE_Z(b, d)))
                    continue;
                /* TODO in fact, I should write more restrictive conditions here, y and z 
                 * to get less running time and correct results(yep, I can't make sure if
                 * I keep running when the first result comes out, the following results
                 * are still correct or not). But since I get one correct answer, I stop
                 * adding more if...else statments. :-D
                 * */
                e = a + avail_36_20_30[k+1][j];
                if (e < 0 || e >= 60 || !jimu[e])
                    continue;
                if (!SAME_FACE_XY(a, e) && !SAME_FACE_XZ(a, e))
                    continue;
                OCCUPY_ONE;
                PRINT_ONE;
                if (try_36_20_30(nr+1, a, b, c, d, e) == 1)
                {
                    UPDATE_RES;
                    return 1;
                }
                else
                {
                    MSG_BACK(nr);
                    RESTORE_ONE;
                }
            }
        }
        // increase i
        if(i % 5 == 2)
            i+=3;
        else
            ++i;
    }

    MSG_TRY("y");
    i = 0;
    while (i < 60)
    {
        a = i; b = a+5; c = a+10;
        if (jimu[a] && jimu[b] && jimu[c])
        {
            for (j = 0; j < m; ++j)
            {
                d = a + avail_36_20_30[k+2][j];
                if (d < 0 || d >= 60 || !jimu[d])
                    continue;
                if (!SAME_FACE_XY(a, d) && !SAME_FACE_YZ(a, d))
                    continue;
                if ((nr < 10 && j < 4 && !SAME_LINE_X(a, d) && !SAME_LINE_Z(a, d)) ||
                        (nr < 10 && j >= 4 && !SAME_LINE_X(c, d) && !SAME_LINE_Z(c, d)) ||
                        (nr >= 10 && !SAME_LINE_X(b, d) && !SAME_LINE_Z(b, d)))
                    continue;
                e = a + avail_36_20_30[k+3][j];
                if (e < 0 || e >= 60 || !jimu[e])
                    continue;
                if (!SAME_FACE_XY(a, e) && !SAME_FACE_YZ(a, e))
                    continue;
                OCCUPY_ONE;
                PRINT_ONE;
                if (try_36_20_30(nr+1, a, b, c, d, e) == 1)
                {
                    UPDATE_RES;
                    return 1;
                }
                else
                {
                    MSG_BACK(nr);
                    RESTORE_ONE;
                }
            }
        }
        // increase i
        if (i % 15 == 4)
            i+=11;
        else
            ++i;
    }

    MSG_TRY("z");
    i = 0;
    while (i < 30)
    {
        a = i; b = a+15; c = a+30;
        if (jimu[a] && jimu[b] && jimu[c])
        {
            for (j = 0; j < m; ++j)
            {
                d = a + avail_36_20_30[k+4][j];
                if (!SAME_FACE_XZ(a, d) && !SAME_FACE_YZ(a, d))
                    continue;
                if (d < 0 || d >= 60 || !jimu[d])
                    continue;
                if ((nr < 10 && j < 4 && !SAME_LINE_X(a, d) && !SAME_LINE_Y(a, d)) ||
                        (nr < 10 && j >= 4 && !SAME_LINE_X(c, d) && !SAME_LINE_Y(c, d)) ||
                        (nr >= 10 && !SAME_LINE_X(b, d) && !SAME_LINE_Y(b, d)))
                    continue;
                e = a + avail_36_20_30[k+5][j];
                if (e < 0 || e >= 60 || !jimu[e])
                    continue;
                if (!SAME_FACE_XZ(a, e) && !SAME_FACE_YZ(a, e))
                    continue;
                OCCUPY_ONE;
                PRINT_ONE;
                if (try_36_20_30(nr+1, a, b, c, d, e) == 1)
                {
                    UPDATE_RES;
                    return 1;
                }
                else
                {
                    MSG_BACK(nr);
                    RESTORE_ONE;
                }
            }
        }
        //increase i
        ++i;
    }

    return 0;
}

/************************************************
 * 12
 ************************************************
 *  oo
 * oo
 * o
 ************************************************/
int try_48_0_45(int nr, int a, int b, int c, int d, int e) //nr = 12
{
    int i, j, k;

    MSG_JUMP(nr);

    MSG_TRY("x");
    i = 0;
    while (i < 60)
    {
        a = i; b = a+1;
        if (jimu[a] && jimu[b])
        {
            for (j = 0; j < 8; ++j)
            {
                c = a + avail_48_0_45[0][j];
                if (c < 0 || c >= 60 || !jimu[c])
                    continue;
                if ((j < 4 && !SAME_LINE_Y(b, c) && !SAME_LINE_Z(b, c)) ||
                    (j >= 4 && !SAME_LINE_Y(a, c) && !SAME_LINE_Z(a, c)))
                    continue;
                d = a + avail_48_0_45[1][j];
                if (d < 0 || d >= 60 || !jimu[d])
                    continue;
                if (!SAME_LINE_X(c, d))
                    continue;
                e = a + avail_48_0_45[2][j];
                if (e < 0 || e >= 60 || !jimu[e])
                    continue;
                if (!SAME_LINE_Y(d, e) && !SAME_LINE_Z(d, e))
                    continue;
                OCCUPY_ONE;
                PRINT_ONE;
                UPDATE_RES;
                return 1;
            }
        }
        // increase i
        if (i % 5 == 3)
            i+=2;
        else
            ++i;
    }

    MSG_TRY("z");
    i = 0;
    while (i < 45)
    {
        a = i; b = a+15;
        if (jimu[a] && jimu[b])
        {
            for (j = 0; j < 8; ++j)
            {
                c = a + avail_48_0_45[3][j];
                if (c < 0 || c >= 60 || !jimu[c])
                    continue;
                if ((j < 4 && !SAME_LINE_X(b, c) && !SAME_LINE_Y(b, c)) ||
                    (j >= 4 && !SAME_LINE_X(a, c) && !SAME_LINE_Y(a, c)))
                    continue;
                d = a + avail_48_0_45[4][j];
                if (d < 0 || d >= 60 || !jimu[d])
                    continue;
                if (!SAME_LINE_Z(c, d))
                    continue;
                e = a + avail_48_0_45[5][j];
                if (e < 0 || e >= 60 || !jimu[e])
                    continue;
                if (!SAME_LINE_X(d, e) && !SAME_LINE_Y(d, e))
                    continue;
                OCCUPY_ONE;
                PRINT_ONE;
                UPDATE_RES;
                return 1;
            }
        }
        // increase i
        ++i;
    }
    return 0;
}
