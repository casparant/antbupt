package com.ant_studio.mvc;

import java.awt.event.*;
import javax.swing.*;

public class WmvcController implements    ActionListener,
                                             ItemListener
{
	protected JComponent myComponent;
	private WmvcExecutor wmvcExecutor;
	
	public WmvcController(JComponent comp, WmvcExecutor wExec)
	{
		myComponent = comp;
		wmvcExecutor = wExec;
	}
	
	public WmvcExecutor getWmvcExecutor()
	{   return wmvcExecutor ;  }
	

	public void actionPerformed(ActionEvent event) {
		// TODO 自动生成方法存根
		if(wmvcExecutor != null)
			wmvcExecutor.execute(event);
	}

	public void itemStateChanged(ItemEvent event) {
		// TODO 自动生成方法存根
		if(wmvcExecutor != null)
			wmvcExecutor.execute(event);
		
	}

}
