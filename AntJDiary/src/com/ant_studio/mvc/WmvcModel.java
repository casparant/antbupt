package com.ant_studio.mvc;

import java.util.*;

public class WmvcModel extends Observable
{
	public WmvcModel ()
	{
		super();
	}
	
	public void addView (WmvcView view)
	{
		addObserver((Observer) view);
	}
	
	public void deleteView(WmvcView view)
	{
		deleteObserver((Observer) view);
	}
	
	public void notifyViews()
	{
		setChanged();
		notifyObservers();
	}

}
