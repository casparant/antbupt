package com.ant_studio;

import java.awt.Component;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JTree;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.*;

import com.ant_studio.mvc.*;

public class DiaryTreeView extends WmvcView 
{
	@SuppressWarnings("unused")
	private static boolean updating = false;
	private DiaryModel myDiaryModel = JDiary.getModel();
	private static Diary diary;
	private static JTree tree;
	private static JTree tempTree;
	private static int location;

	private final JPopupMenu popupMenu = new JPopupMenu();
	@SuppressWarnings("unused")
	private final 	JMenuItem newMenuItem = new JMenuItem();
	private final 	JMenuItem deleteMenuItem = new JMenuItem();
	private final 	JMenuItem saveMenuItem = new JMenuItem();
	private final 	JMenuItem importMenuItem = new JMenuItem();
	private final 	JMenuItem printMenuItem = new JMenuItem();
	private final 	JMenuItem advSearchMenuItem = new JMenuItem();	
	
	private final Border enteredBorder = new LineBorder(SystemColor.activeCaption, 1, true);
	private final Border exitedBorder = BorderFactory.createEmptyBorder();
	private MyMouseListener myMouse;
	
	public DiaryTreeView() 
	{
		myDiaryModel.addView(this);	
		init();
	}
	
	public void init() 
	{	
		tree = new JTree(myDiaryModel.getDiaryTree());
		
		tree.addMouseListener(new MouseAdapter()
		{
			public void mousePressed(MouseEvent e)
			{
				tempTree = (JTree)e.getSource();
				location = tempTree.getRowForLocation(e.getX(), e.getY());

				TreePath path = tempTree.getPathForRow(location);
				DefaultMutableTreeNode node = (DefaultMutableTreeNode)
				              path.getLastPathComponent();
				diary = (Diary)node.getUserObject();
				if(e.getClickCount() == 1)
					myDiaryModel.setCurrentDiary(diary);
			}
		});
		
//		添加右键菜单
		addPopup(tree, popupMenu);
		popupMenu.setBorder(new CompoundBorder(new LineBorder(SystemColor.activeCaption, 1, true), new TitledBorder(null, "Ant_Studio", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, SystemColor.activeCaptionBorder)));

		
		//删除
		deleteMenuItem.setText("删除");
		deleteMenuItem.addMouseListener(myMouse);
		popupMenu.add(deleteMenuItem);
		
		deleteMenuItem.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0)
			{
				myDiaryModel.deleteCurrentDiary(myDiaryModel.getCurrentDiary());
				if(myDiaryModel.getTreeChanged())updateView();
			}

		});		//删除结束
		
//		保存
		saveMenuItem.setText("保存");
		saveMenuItem.addMouseListener(myMouse);
		popupMenu.add(saveMenuItem);
		
		saveMenuItem.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				myDiaryModel.saveDiarys();
			}
		});		//保存结束
		
//		批量导出
		importMenuItem.setText("批量导出");
		importMenuItem.addMouseListener(myMouse);
		popupMenu.add(importMenuItem);
		
		importMenuItem.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Format.tip();
			}
		});		//批量导出结束
		
//		打印
		printMenuItem.setText("打印");
		printMenuItem.addMouseListener(myMouse);
		popupMenu.add(printMenuItem);
		
		printMenuItem.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				Format.tip();
			}
		});//		打印结束
		
//		高级查找
		advSearchMenuItem.setText("高级查找");
		advSearchMenuItem.addMouseListener(myMouse);
		popupMenu.add(advSearchMenuItem);

		advSearchMenuItem.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0)
			{
				new AdvancedSearchDialog();
			}
		});		//高级查找结束	
	}
	
	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger())
					showMenu(e);
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger())
					showMenu(e);
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
	class MyMouseListener extends MouseAdapter
	{
		public void mouseEntered(MouseEvent e) 
		{
			((JMenuItem)e.getSource()).setBorder(enteredBorder);
		}
		public void mouseExited(MouseEvent e) 
		{
			((JMenuItem)e.getSource()).setBorder(exitedBorder);
		}
	}
	
	/**
	 * @return Diary
	 */
	public Diary getDiary()
	{
		return diary;
	}
	
	/**
	 * @return JTree
	 */
	public static JTree getTree()
	{
		return tree;
	}
	/**
	 * 在MVC中使用
	 */
	public void updateView()
	{
		updating = true;
		
		if(myDiaryModel.getNeedRepaint())
		{
			init();
		}
	}
	
	/**
	 * 在选择下一篇或者上一篇日记时
	 * 使用这个location
	 * 来快速获取
	 */
	public static int getLocation()
	{
		return location;
	}
	
	public static int getPreviousLocation()
	{
		location--;
		return location;
	}
	
	public static int getNextLocation()
	{
		location++;
		return location;
	}
	/**
	 * 获取上一个的树
	 */
	public static JTree getTempTree()
	{
		return tempTree;
	}
}
