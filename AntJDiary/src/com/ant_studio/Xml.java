package com.ant_studio; 

import java.awt.Point;
import java.io.*;
import java.util.LinkedList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.jdom.xpath.XPath;

public class Xml 
{
	public static boolean isNotPause; //控制是否允许暂停
	
	public static Document loadXml(String path) throws FileNotFoundException, JDOMException, IOException
	{
		SAXBuilder sb = new SAXBuilder(); // 新建立构造器
		Document doc = sb.build(new FileInputStream(path));
		doc.getRootElement().setAttribute("filename", path);
		return doc;
	}

	public static void saveXml(Document doc, String path)	throws FileNotFoundException, IOException 
	{
		Format f = Format.getPrettyFormat();
		f.setEncoding("GB2312");
		XMLOutputter outp = new XMLOutputter(f); // 构造新的输出流

		FileOutputStream out = new FileOutputStream(path);
		outp.output(doc, out); // 输出到2.XML文件中
	}

	/**
	 * 把点转换成字串
	 * 
	 * @param str
	 * @return
	 */
	public static String points2String(List<Point> pts) 
	{
		StringBuffer buf = new StringBuffer();
		for (Point p : pts) 
		{
			buf.append(Xml.int2char(p.x));
			buf.append(Xml.int2char(p.y));
		}
		return buf.toString();
	}
	
	/**
	 * 把点转换成字串
	 * 
	 * @param str
	 * @return
	 */
	public static String point2String(Point p) 
	{
		StringBuffer buf = new StringBuffer();
		buf.append(Xml.int2char(p.x));
		buf.append(Xml.int2char(p.y));
		return buf.toString();
	}

	public static List<Point> String2Points(String str) 
	{
		if (str != null && str.length() % 2 == 0) 
		{
			List<Point> ls = new LinkedList<Point>();
			for (int i = 0; i < str.length(); i += 2) 
			{
				int x = char2int(str.charAt(i));
				int y = char2int(str.charAt(i + 1));
				ls.add(new Point(x, y));
			}
			return ls;
		}
		return null;
	}

	public static Point String2Point(String str) 
	{
		if (str != null && str.length() == 2) 
		{
			int x = char2int(str.charAt(0));
			int y = char2int(str.charAt(1));
			return new Point(x, y);
		}
		return null;
	}

	public static List<Point> String2Points(String str, Point op) 
	{
		if (str != null && str.length() % 2 == 0) 
		{
			List<Point> ls = new LinkedList<Point>();
			for (int i = 0; i < str.length(); i += 2) 
			{
				int x = char2int(str.charAt(i));
				int y = char2int(str.charAt(i + 1));
				x += op.x;
				y += op.y;
				ls.add(new Point(x, y));
			}
			return ls;
		}
		return null;
	}
    
	public static Element  stringToXml(String xml)
	{
		if (xml != null) 
		{
			try 
			{
				StringReader sr = new StringReader(xml);
				SAXBuilder sb = new SAXBuilder(); // 新建立构造器
				Document doc = sb.build(sr);
				Element root = doc.getRootElement();
				return root; 
			}
			catch (JDOMException e) 
			{
				e.printStackTrace();
			}
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		} 
		return null;
	}
	
	/**
	 * 将坐标编码 a=0 A=-1
	 * 
	 * @param b
	 * @return
	 */
	public static int char2int(char b) 
	{
		if (b >= 'a') 
		{
			return b - 'a';
		}
		else 
			return (b - 'A') * -1;
	}

	/**
	 * 将坐标编码反 a=0 A=-1
	 * 
	 * @param n
	 * @return
	 */
	public static char int2char(int n) 
	{
		if (n > -1) 
		{
			return (char) (n + 'a');
		} else
			return (char) (n * -1 + 'A');
	}

	/**
	 * 用xpath选择一个节点
	 * 
	 * @param xpath
	 * @return
	 */
	public static Element xpathSelectSingle(Element root, String xpath) 
	{
		try 
		{
			Element em = (Element) XPath.selectSingleNode(root, xpath);
			// if(em == null){
			// throw new NullPointerException(xpath);
			// }
			return em;
		} 
		catch (JDOMException e) 
		{
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 用xpath选择一组节点
	 * 
	 * @param xpath
	 * @return
	 */
	public static List<Element> xpathSelectNodes(Element root, String xpath) 
	{
		try 
		{
			List ls = XPath.selectNodes(root, xpath);
			List<Element> ls1 = new LinkedList<Element>();
			for (Object ob : ls)
				ls1.add((Element) ob);
			return ls1;
		} 
		catch (JDOMException e) 
		{
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 把模式中的face中的点描述转换成为List<Point>
	 * 
	 * @param op
	 *            原点，如果为null的话就不进行坐标+原点的转化
	 * @param face
	 * @param label
	 * @return
	 */

	public static List<Point> getGoPoints(Element bs, Point op) 
	{
		if (bs != null) 
		{
			String pts = bs.getAttributeValue("pts");
			if (pts != null) 
			{
				if (op != null)
					return Xml.String2Points(pts, op);
				else
					return Xml.String2Points(pts);
			}
			return null;
		} else
			return null;
	}

	/**
	 * 打印XML元素
	 * 
	 * @param doc
	 */
	public static void printlnXml(Element doc) 
	{

		try 
		{
			XMLOutputter outp = new XMLOutputter(Format.getPrettyFormat()); // 构造新的输出流
			outp.output(doc, System.out); // 输出
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 打印XML元素
	 * 
	 * @param doc
	 */
	public static String xml2String(Element doc) 
	{

		try 
		{
			StringWriter out = new StringWriter();
			XMLOutputter outp = new XMLOutputter(Format.getPrettyFormat()); // 构造新的输出流
			outp.output(doc, out); // 输出
			return out.toString();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		return null;

	}
	
	/**
	 * 从一个文本生成xml
	 * 
	 * @param xmldata
	 * @return
	 * @throws FileNotFoundException
	 * @throws JDOMException
	 * @throws IOException
	 */
	public static Element parseElement(String xmldata) 	throws FileNotFoundException, JDOMException, IOException 
	{
		StringReader reader = new StringReader(xmldata);
		SAXBuilder sb = new SAXBuilder(); // 新建立构造器
		Document doc = null;
		
		try 
		{
			doc = sb.build(reader);
			Element em = doc.getRootElement();
			doc.removeContent(em);
			return em;
		} 
		catch (JDOMException e1) 
		{
			e1.printStackTrace();
		}
		catch (IOException e1) 
		{
			e1.printStackTrace();			
		}
		return null;
	}
	
	/**
	 * 暂停
	 *
	 */
	public static void pause()
	{
		if(isNotPause)
			return;
		try 
		{
			System.in.read();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
}
 