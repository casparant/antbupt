package com.ant_studio;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;
import com.swtdesigner.SwingResourceManager;

public class ChooseDateDialog extends JDialog
{
	/**
	 * 选择日期对话框
	 * @author 张金利
	 */
	private static final long serialVersionUID = 2L;
	private final int INITIAL_X = 2, INITIAL_Y = 4, END_X = 8;
		
	private JButton previousYearButton = new JButton();
	private JButton previousMonthButton = new JButton();
	private JLabel dateLabel = new JLabel();
	private JButton nextMonthButton = new JButton();
	private JButton nextYearButton = new JButton();

	private JLabel sundayLabel = new JLabel();
	private JLabel mondayLabel = new JLabel();
	private JLabel tuesdayLabel = new JLabel();
	private JLabel wednesdayLabel = new JLabel();
	private JLabel thursdayLabel = new JLabel();
	private JLabel fridayLabel = new JLabel();
	private JLabel saturdayLabel = new JLabel();
	private JToggleButton[] dateToggleButton = new JToggleButton[31]; 
	
	private JButton todayButton = new JButton();
	private JButton okButton = new JButton();
	private JButton cancelButton = new JButton();

	private static int year = Format.THIS_YEAR, month = Format.THIS_MONTH, day = Format.TODAY;
	private static boolean flag;
	
	public ChooseDateDialog()
	{
		//对话框初始化
		super();
		setTitle("选择日期...");	
		setResizable(false);
		setLocation(150, 150);
		setAlwaysOnTop(true);
		setModal(true);
		flag = false;
		
		//设置表格布局
		getContentPane().setLayout(new FormLayout(
			new ColumnSpec[] 
			{
				FormFactory.RELATED_GAP_COLSPEC,
				new ColumnSpec("50px"),
				new ColumnSpec("50px"),
				new ColumnSpec("50px"),
				new ColumnSpec("50px"),
				new ColumnSpec("50px"),
				new ColumnSpec("50px"),
				new ColumnSpec("50px"),
				FormFactory.RELATED_GAP_COLSPEC
			},
			new RowSpec[] 
			{
				FormFactory.RELATED_GAP_ROWSPEC,
				new RowSpec("25px"),
				new RowSpec("30px"),
				new RowSpec("30px"),
				new RowSpec("30px"),
				new RowSpec("30px"),
				new RowSpec("30px"),
				new RowSpec("30px"),
				new RowSpec("30px"),
				FormFactory.RELATED_GAP_ROWSPEC,
				new RowSpec("30px"),
				FormFactory.RELATED_GAP_ROWSPEC
			}));
		
		//“上一年”按钮
		previousYearButton.setToolTipText("上一年");
		previousYearButton.setMargin(new Insets(2, 5, 2, 5));
		previousYearButton.setText("<<");
		getContentPane().add(previousYearButton, new CellConstraints("2, 2, 1, 1, fill, fill"));
		
		previousYearButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				removeOldDateForm(year, month);
				year--;
				createNewDateForm(year, month, day);
				selectDate(year, month);
			}
		});

		//“上一个月”按钮
		previousMonthButton.setToolTipText("上一个月");
		previousMonthButton.setMargin(new Insets(2, 5, 2, 5));
		previousMonthButton.setText("<");
		getContentPane().add(previousMonthButton, new CellConstraints("3, 2, 1, 1, fill, fill"));
		
		previousMonthButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(final ActionEvent e) 
			{
				removeOldDateForm(year, month);
				month--;
				if (month == 0) month = 12;
				createNewDateForm(year, month, day);	
				selectDate(year, month);
			}
		});

		//中间的日期标签
		dateLabel.setHorizontalTextPosition(SwingConstants.CENTER);
		dateLabel.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(dateLabel, new CellConstraints("4, 2, 3, 1, fill, fill"));
		
		//“下一个月”按钮
		nextMonthButton.setToolTipText("下一个月");
		nextMonthButton.setMargin(new Insets(2, 5, 2, 5));
		nextMonthButton.setText(">");
		getContentPane().add(nextMonthButton, new CellConstraints("7, 2, 1, 1, fill, fill"));
		
		nextMonthButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(final ActionEvent e) 
			{
				removeOldDateForm(year, month);
				month++;
				if (month == 13) month = 1;
				createNewDateForm(year, month, day);	
				selectDate(year, month);
			}
		});

		//“下一年”按钮
		nextYearButton.setToolTipText("下一年");
		nextYearButton.setMargin(new Insets(2, 5, 2, 5));
		nextYearButton.setText(">>");
		getContentPane().add(nextYearButton, new CellConstraints("8, 2, 1, 1, fill, fill"));
		
		nextYearButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(final ActionEvent e) 
			{
				removeOldDateForm(year, month);
				year++;
				createNewDateForm(year, month, day);	
				selectDate(year, month);
			}
		});

		//“今天日期”按钮
		todayButton.setMargin(new Insets(2, 5, 2, 5));
		todayButton.setText("今天：" + Format.today());
		getContentPane().add(todayButton, new CellConstraints("2, 11, 3, 1, center, center"));
		
		todayButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				removeOldDateForm(year, month);
				year = Format.THIS_YEAR;
				month = Format.THIS_MONTH;
				day = Format.TODAY;
				createNewDateForm(year, month, day);
				for (int i = 0; i < Format.getDayOfMonth(Format.THIS_YEAR, Format.THIS_MONTH); i++)
				{
					dateToggleButton[i].setSelected(false);
				}
				dateToggleButton[Format.TODAY - 1].setSelected(true);
				dateLabel.setText(Format.date(year, month, day));
			}
		});

		//显示星期栏
		sundayLabel.setText("星期天");
		getContentPane().add(sundayLabel, new CellConstraints("2, 3, 1, 1, center, fill"));
		mondayLabel.setText("星期一");
		getContentPane().add(mondayLabel, new CellConstraints("3, 3, 1, 1, center, fill"));
		wednesdayLabel.setText("星期三");
		getContentPane().add(wednesdayLabel, new CellConstraints("5, 3, 1, 1, center, fill"));
		thursdayLabel.setText("星期四");
		getContentPane().add(thursdayLabel, new CellConstraints("6, 3, 1, 1, center, fill"));
		fridayLabel.setText("星期五");
		getContentPane().add(fridayLabel, new CellConstraints("7, 3, 1, 1, center, fill"));
		tuesdayLabel.setText("星期二");
		getContentPane().add(tuesdayLabel, new CellConstraints("4, 3, 1, 1, center, fill"));
		saturdayLabel.setText("星期六");
		getContentPane().add(saturdayLabel, new CellConstraints("8, 3, 1, 1, center, fill"));	

		//创建初始日期按钮表格
		createNewDateForm(year, month, day);	
		selectDate(year, month);
		
		//“选定”按钮
		okButton.setText("选定 (O)");
		okButton.setMnemonic(KeyEvent.VK_O);
		okButton.setIcon(SwingResourceManager.getIcon(ChooseDateDialog.class, "/images/okey.gif"));
		getContentPane().add(okButton, 	new CellConstraints("5, 11, 2, 1, center, center"));

		okButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(final ActionEvent e) 
			{
				flag = true;
				dispose();
			}
		});
		
		//“取消”按钮
		cancelButton.setText("取消 (C)");
		cancelButton.setMnemonic(KeyEvent.VK_C);
		cancelButton.setIcon(SwingResourceManager.getIcon(ChooseDateDialog.class, "/images/cancel.gif"));
		getContentPane().add(cancelButton, new CellConstraints("7, 11, 2, 1, center, center"));
		
		cancelButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(final ActionEvent e) 
			{
				flag = false;
				dispose();
			}
		});
		
		pack();
	}	//SelectDateDialog()构造方法结束
	
	/**
	 * 功能：在创建新的日期表格前除去旧的表格
	 * @param year
	 * @param month
	 */
	private void removeOldDateForm(int year, int month)
	{
		for (int i = 0; i < Format.getDayOfMonth(year, month); i++)
		{
			dateToggleButton[i].setSelected(false);
			dateToggleButton[i].setVisible(false);			
		}
	}
	
	/**
	 * 功能 依据提供的参数创建一个新的日期表格
	 * @param year
	 * @param month
	 * @param day
	 */
	private void createNewDateForm(final int year, final int month, int day)
	{
		int i, x = Format.getWeekIndex(year, month) + INITIAL_X -1, y = INITIAL_Y;
		
		for (i = 0; i < Format.getDayOfMonth(year, month); i++)
		{
			dateToggleButton[i] = new JToggleButton();
		}
		if (day > Format.getDayOfMonth(year, month))
			day = Format.getDayOfMonth(year, month);
		dateToggleButton[day - 1].setSelected(true);
		dateLabel.setText(Format.date(year, month, day));

		for(i = 0; i< Format.getDayOfMonth(year, month); i++)
		{
			dateToggleButton[i].setText(Integer.toString(i + 1));
			x ++;
			if (x > END_X) 
			{
				x = 2;
				y ++;
			}
			getContentPane().add(dateToggleButton[i], new CellConstraints(x, y));
		}	//while循环结束		
		ChooseDateDialog.day = day;
	}
	
	/**
	 * 监听事件获得选取的日期
	 * @param year
	 * @param month
	 */
	private void selectDate(final int year, final int month)
	{		
		for (int i = 0; i < Format.getDayOfMonth(year, month); i++)
		{
			dateToggleButton[i].addMouseListener(new MouseAdapter()
			{
				public void mousePressed(MouseEvent e) 
				{
					for (int i = 0; i < Format.getDayOfMonth(year, month); i++)
					{
						if ((JToggleButton)e.getSource() == dateToggleButton[i])
						{
							day = i + 1;
							ChooseDateDialog.year = year;
							ChooseDateDialog.month = month;
							dateLabel.setText(Format.date(year, month, day));
						    break;
						}
					}
					for (int i = 0; i < Format.getDayOfMonth(year, month); i++)
					{
						dateToggleButton[i].setSelected(false);
					}
				}
			});
		}
	}
	
	/**
	 * 以下三个静态函数分别返回三个字符串：
	 * 选取的年份
	 * 选取的月份
	 * 选取的日期
	 */
	public static String getYear()
	{
		if (year < 10) 
			return ("000" + Integer.toString(year));
		else 
			if (year < 100) 
				return ("00" + Integer.toString(year));
		else 
			if (year < 1000) 
				return ("0" + Integer.toString(year));
		else 
			return (Integer.toString(year));
	}
	
	public static String getMonth()
	{
		if (month < 10) 
			return ("0" + Integer.toString(month));
		else 
			return (Integer.toString(month));
	}
	
	public static String getDay()
	{
		if (day < 10)
			return ("0" + Integer.toString(day));
		else
			return (Integer.toString(day));
	}
	
	public static boolean getFlag()
	{
		return flag;
	}
}//	SelectDateDialog 类结束