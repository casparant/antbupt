package com.ant_studio;

import java.util.*;

public class MyVector extends Vector{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String str;
	
	public MyVector(String str)
	{
		this.str = str;
	}
	
	public String toString()
	{
		return str;
	}
	

}
