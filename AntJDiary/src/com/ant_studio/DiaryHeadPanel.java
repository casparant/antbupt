package com.ant_studio;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import com.ant_studio.mvc.WmvcView;
import com.swtdesigner.SwingResourceManager;

public class DiaryHeadPanel extends WmvcView
{
	private JPanel previousNextPanel = new JPanel();
	private 	JButton previousButton = new JButton();
	private 	JButton nextButton = new JButton();
	private static JLabel chooseDateLabel = new JLabel();
	private static JButton weatherButton = new JButton();
	@SuppressWarnings("unused")
	private JPanel myJPanel;
	private JTree tree;
	
	/**
	 * Create the dialog
	 */
	public DiaryHeadPanel(final JPanel headBarPanel) 
	{
		myJPanel = headBarPanel;
		JDiary.getModel().addView(this);
		
		headBarPanel.setLayout(new BorderLayout());	
		
		//面板西部为前篇日记和后篇日记按钮组成的新面板
		headBarPanel.add(previousNextPanel, BorderLayout.WEST);	
		
		//增加前篇日记按钮
		previousNextPanel.add(previousButton);				
		previousButton.setPreferredSize(new Dimension(25, 25));
		previousButton.setMargin(new Insets(2, 5, 2, 5));
		previousButton.setToolTipText("前一篇日记");
		previousButton.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/previous.gif"));
		previousButton.addActionListener( new ActionListener()
		{

			@SuppressWarnings("static-access")
			public void actionPerformed(ActionEvent event) {
				tree = JDiary.getTreeView().getTempTree();
				Diary diary;
				
		        try{
		        	int index = JDiary.getTreeView().getPreviousLocation();
		        	TreePath path = tree.getPathForRow(index);
		        	DefaultMutableTreeNode node = (DefaultMutableTreeNode)
				              path.getLastPathComponent();
		        	diary = (Diary)node.getUserObject();
		        	JDiary.getModel().setCurrentDiary(diary);
		        } catch (Exception e) {
		        	JOptionPane.showMessageDialog(null, "已到尽头");
		        	JDiary.getTreeView().getNextLocation();//把多减的1加上
		        }
		        
		   }
			
		});
		//增加后篇日记按钮
		previousNextPanel.add(nextButton);					
		nextButton.setPreferredSize(new Dimension(25, 25));
		nextButton.setMargin(new Insets(2, 5, 2, 5));
		nextButton.setToolTipText("后一篇日记");
		nextButton.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/next.gif"));
		nextButton.addActionListener( new ActionListener()
		{
			@SuppressWarnings("static-access")
			public void actionPerformed(ActionEvent event) {
			    tree = JDiary.getTreeView().getTempTree();
				Diary diary;
				
				try{
					int index = JDiary.getTreeView().getNextLocation();
		        	TreePath path = tree.getPathForRow(index);
		        	DefaultMutableTreeNode node = (DefaultMutableTreeNode)
				              path.getLastPathComponent();
		        	diary = (Diary)node.getUserObject();
		        	JDiary.getModel().setCurrentDiary(diary);
		        } catch (Exception e) {
		        	JOptionPane.showMessageDialog(null, "已到尽头");
		        	JDiary.getTreeView().getPreviousLocation();// 把多加的1减掉
		        }
			}
		});
		
		
		//面板中部为选择日期标签
		headBarPanel.add(chooseDateLabel, BorderLayout.CENTER);	
		chooseDateLabel.setHorizontalAlignment(SwingConstants.CENTER);
		chooseDateLabel.setText(Format.date(Format.THIS_YEAR, Format.THIS_MONTH, Format.TODAY));
		chooseDateLabel.setToolTipText("选择一个日期");
		
		chooseDateLabel.addMouseListener(new MouseAdapter() 
		{
			public void mouseClicked(MouseEvent clickEvent) 
			{
				new ChooseDateDialog().setVisible(true);
				Operation.createNewDiary(ChooseDateDialog.getFlag(), chooseDateLabel);
			}
			public void mousePressed(MouseEvent pressEvent) 
			{
				chooseDateLabel.setForeground(new Color(255, 0, 0));
			}
			public void mouseReleased(MouseEvent releaseEvent) 
			{
				chooseDateLabel.setForeground(new Color(0, 0, 0));			
			}
		});
		
		//面板东部为选择日期标签
		headBarPanel.add(weatherButton, BorderLayout.EAST);		
		weatherButton.setPreferredSize(new Dimension(40, 30));
		weatherButton.setMargin(new Insets(2, 2, 2, 2));
		weatherButton.setText("天气");
		weatherButton.setToolTipText("选择天气");
		
		weatherButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent actionEvent) 
			{
				new WeatherDialog(weatherButton).setVisible(true);
			}
		});
	}	//选择日期和天气面板构造方法结束

	/**
	 * 
	 */
	public static JLabel getChooseDateLabel()
	{
		return chooseDateLabel;
	}
	
	/**
	 * 返回天气
	 */
	public static String getWeather()
	{
		return weatherButton.getText();
	}
	
	/**
	 * 注册的视图
	 * 改变日期和天气
	 */
	
	@SuppressWarnings("static-access")
	public void updateView()
	{
		chooseDateLabel.setText( JDiary.getModel().getCurrentDate() );	//改变日期
		weatherButton.setText( JDiary.getModel().getCurrentDiary().getWeather() );//改变天气
		tree = JDiary.getTreeView().getTempTree();
	}
}
