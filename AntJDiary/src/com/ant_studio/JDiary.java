package com.ant_studio;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.File;
import java.io.FileInputStream;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.ScrollPaneLayout;

import com.ant_studio.mvc.WmvcView;
import com.swtdesigner.SwingResourceManager;

public class JDiary extends WmvcView
{	
	private final static JFrame mainFrame = new JFrame();
	private static DiaryModel myDiaryModel = new DiaryModel();
	
	private JMenuBar menuBar = new JMenuBar(); 
	private JToolBar toolBar = new JToolBar(); 
	private JSplitPane splitPane = new JSplitPane();
	private 	JPanel leftPanel = new JPanel();
	private 		static  JTabbedPane tabbedPane = new JTabbedPane();
	private 			static DiaryTreeView treeView = new DiaryTreeView();
	private 	JPanel rigthPanel = new JPanel();	
	private 		JPanel headPanel = new JPanel();
	private 		JScrollPane textScrollPane = new JScrollPane();
	private			static DiaryTextAreaView textAreaView = new DiaryTextAreaView();
	private JPanel statusPanel = new JPanel();
	/**
	 * 启动程序
	 * @param args
	 */	
	@SuppressWarnings("static-access")
	public static void main(String[] args) 
	{
		try
		{					
			File f = new File("data");
			FileInputStream temp = new FileInputStream(f);
		    byte[] password = new byte[temp.available()];
		    temp.read(password);
		    String blank = "";
		    byte[] temp1 = DataSecurity.MD5(blank.getBytes());
		    
			if(password.length != 0 && !DataSecurity.IsByteArrayTheSame(password, temp1))
				new LoginDialog().setVisible(true);
			else
				new JDiary().getJFrame().setVisible(true);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * 构造函数
	 */
	public JDiary() 
	{
		myDiaryModel.addView(this);
		initialize();
	}

	/**
	 * 初始化框架
	 */
	@SuppressWarnings({ "static-access" })
	void initialize()
	{
		//主框架基本属性		
		mainFrame.setSize(new Dimension(700, 500));
		mainFrame.setIconImage(SwingResourceManager.getImage(JDiary.class, "/images/diary3.gif"));
		mainFrame.setTitle("Java日记本");
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setLocationRelativeTo(null);
		
		//设定每个容器的位置
		mainFrame.setJMenuBar(menuBar);	
		mainFrame.getContentPane().add(toolBar, BorderLayout.NORTH);
		mainFrame.getContentPane().add(splitPane, BorderLayout.CENTER);
		mainFrame.getContentPane().add(statusPanel, BorderLayout.SOUTH);
		
		//给每个容器装载组件
		new DiaryMenuBar(menuBar);						//菜单栏
		new DiaryToolBar(toolBar);						//工具栏
		new DiaryStatusPanel(statusPanel);				//状态栏
		{												//主版面
			//拆分窗格属性
			splitPane.setOneTouchExpandable(true);
			
			//左面板
			splitPane.setLeftComponent(leftPanel);
			leftPanel.setPreferredSize(new Dimension(150, 0));
			leftPanel.setLayout(new BorderLayout());	//拆分窗格左边建立新的面板并设定布局
			    leftPanel.add(tabbedPane, BorderLayout.CENTER);
				tabbedPane.removeAll();
				tabbedPane.addTab("目录", null, treeView.getTree() , "目录");
			
			//右面板
			splitPane.setRightComponent(rigthPanel);	
			rigthPanel.setLayout(new BorderLayout());	//拆分窗格右边建立新的面板并设定布局
				rigthPanel.add(headPanel, BorderLayout.NORTH);	
					new DiaryHeadPanel(headPanel);		//建立选择日期和天气面板(headBar)并设定布局
				rigthPanel.add(textScrollPane, BorderLayout.CENTER);
					textScrollPane.setLayout(new ScrollPaneLayout());
					textScrollPane.setViewportView(textAreaView.getTextArea());
		}												//主版面结束
	}	//initialize() 方法结束

	/**
	 * @return JTabbedPane
	 */
	public static JTabbedPane getTabbedPane()
	{
		return tabbedPane;
	}
	
	/**
	 * 在MVC中使用
	 */
	@SuppressWarnings("static-access")
	public void updateView()
	{
		
		
		treeView.updateView();
			
		if(myDiaryModel.getNeedRepaint())
		{
			tabbedPane.removeAll();
			tabbedPane.addTab("目录", null, treeView.getTree() , "目录");
		}
	}

	public static DiaryModel getModel()
	{
		return myDiaryModel;
	}
	
	public static DiaryTreeView getTreeView()
	{
		return treeView;
	}
	
	public static DiaryTextAreaView getTextView()
	{
		return textAreaView;
	}
	
	public static JFrame getJFrame()
	{
		return mainFrame;
	}
}	//JDiary() 类结束
