package com.ant_studio;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class HighlightColorChooserDialog extends JDialog implements ChangeListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 突出颜色选择器
	 * @author 陶睿
	 */	
	private static Color newHighlightColor, originalHighlightColor ;
	protected JColorChooser highlightColorChooser;
	private final JPanel panel;
	private final TitledBorder titledBorder;
	
	private final JPanel buttonPanel = new JPanel();
	private final JButton confirmButton = new JButton();
	private final JButton cancelButton = new JButton();
	private final JButton resetButton = new JButton();

	
	public HighlightColorChooserDialog(Color currentColor) {
		super();
		originalHighlightColor = currentColor;	//重设、取消的时候用，相当于备份
		
		setTitle("突出颜色选择");
		setBounds(100, 100, 467, 436);

		panel = new JPanel();
		panel.setLayout(new BorderLayout());
		titledBorder = new TitledBorder(null, "Ant_ColorChooser", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, SystemColor.activeCaption);
		titledBorder.setTitleFont(new Font("Candara", Font.PLAIN, 16));
		panel.setBorder(new CompoundBorder(titledBorder, null));
		getContentPane().add(panel);
		
		//颜色选择器
		highlightColorChooser = new JColorChooser(currentColor);
		highlightColorChooser.getPreviewPanel().setToolTipText("当你的鼠标在这里停留了超过五秒钟，说明你的智商已经达到了我们的水平，恭喜，恭喜！");
		//highlightColorChooser.setFont(new Font("@Malgun Gothic", Font.PLAIN, 12));
		highlightColorChooser.getSelectionModel().addChangeListener(this);
		highlightColorChooser.setBorder(BorderFactory.createTitledBorder(
                                             "Choose Highlight Color"));	
		panel.add(highlightColorChooser);
		//颜色选择器结束
		
		//按键板块
		panel.add(buttonPanel, BorderLayout.SOUTH);

		//确认按钮
		confirmButton.setBorder(new EmptyBorder(0,0,0,0));
		confirmButton.setPreferredSize(new Dimension(50, 25));
		confirmButton.setText("确定");
		buttonPanel.add(confirmButton);
		//事件监听
		confirmButton.addMouseListener(new MouseAdapter() 
		{
			public void mouseEntered(MouseEvent arg0)
			{
				confirmButton.setBorder(new LineBorder(SystemColor.activeCaption));
			}
			public void mouseExited(MouseEvent e)
			{
				confirmButton.setBorder(new EmptyBorder(0, 0, 0, 0));
			}
		});		

		confirmButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				dispose();
			}
		});		//确认按钮结束		

		//取消按钮
		cancelButton.setBorder(new EmptyBorder(0, 0, 0, 0));
		cancelButton.setPreferredSize(new Dimension(50, 25));
		cancelButton.setText("取消");
		buttonPanel.add(cancelButton);
		
		cancelButton.addMouseListener(new MouseAdapter() 
		{
			public void mouseEntered(MouseEvent arg0)
			{
				cancelButton.setBorder(new LineBorder(SystemColor.activeCaption));
			}
			public void mouseExited(MouseEvent e)
			{
				cancelButton.setBorder(new EmptyBorder(0, 0, 0, 0));
			}
		});
		cancelButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0)
			{
				newHighlightColor = originalHighlightColor;
				dispose();
			}
		});		//取消按钮结束		

		//重设按钮
		resetButton.setBorder(new EmptyBorder(0, 0, 0, 0));
		resetButton.setPreferredSize(new Dimension(50, 25));
		resetButton.setText("重设");
		buttonPanel.add(resetButton);
		
		resetButton.addMouseListener(new MouseAdapter() 
		{
			public void mouseEntered(MouseEvent arg0)
			{
				resetButton.setBorder(new LineBorder(SystemColor.activeCaption));
			}
			public void mouseExited(MouseEvent e)
			{
				resetButton.setBorder(new EmptyBorder(0, 0, 0, 0));
			}
		});
		resetButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				newHighlightColor = originalHighlightColor;
			}
		});		//重设按钮结束		
		//按键板块结束
	}

	public void stateChanged(ChangeEvent e)
	{
		// TODO 自动生成方法存根
		newHighlightColor = highlightColorChooser.getColor();
	}
		 
	/**
	 * @return highlightColor
	 */
	public static Color getNewHighlightColor()
	{
		return newHighlightColor;
	}

}
