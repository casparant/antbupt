package com.ant_studio;

import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JComboBox;

import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;

import com.swtdesigner.SwingResourceManager;

public class ToolBarDialog extends JDialog  implements ActionListener
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 浮动工具栏对话框
	 * @author 陶睿
	 */
	
	private JTextArea diaryText = DiaryTextAreaView.getTextArea(); 
	private final JPanel panel = new JPanel();
	//private boolean isMakedCenter = false;
	//private boolean isOutStand = false;	
	private MyButtonMouseListener myButtonMouseListener;
	private MyToggleButtonMouseListener myToggleButtonMouseListener;	
	//颜色选择器所需变量
	private static Color selectedHighlightColor = Color.YELLOW;	//SystemColor.activeCaption;
	private final int doubleClick = 2;
	private static Color textColor = Color.BLACK;
	private static Color backgroundColor = Color.WHITE;
	//字体及大小选择所需变量
	private JComboBox fontSizeComboBox;
	private JComboBox fontComboBox;
	private JComboBox tempComboBox;
	private Font currentFont = new Font("Serif", Font.PLAIN, 14), newFont = new Font("Serif", Font.PLAIN, 14) ;	 
	private String[] fontSize = {"8","9","10","11","12","14","16","18","20","22","24","26","28","36","48","72"};
	private String[] fontNames = java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();	
	private Font tempFont;
	private String selectedFontName;
	private String selectedSize;
	//居中所需变量
	private int column, start, end, startLine, endLine, head, tail, blanks;
	private String blankString;
	//buttons
	private final JToggleButton boldToggleButton = new JToggleButton();
	private final JToggleButton italicsToggleButton = new JToggleButton();
	private final JToggleButton highlightToggleButton = new JToggleButton();
	private final JButton pasteButton = new JButton();
	private final JButton makeCenterButton = new JButton();
	private final JButton colorButton = new JButton();
	private final JButton copyButton = new JButton();
	private final JButton signButton = new JButton();
	private final JButton undoButton = new JButton();
	/**
	 * Create the dialog
	 * 构造函数
	 */
	public ToolBarDialog() {
		super();
		setUndecorated(true);
		//setAlwaysOnTop(true);
		setSize(229, 62);
		getContentPane().setBackground(new Color(230, 230, 250));
		getContentPane().setLayout(null);
		
		panel.setBorder(new CompoundBorder(new BevelBorder(SoftBevelBorder.RAISED), new LineBorder(SystemColor.activeCaption, 1, false)));
		panel.setLayout(null);
		panel.setBounds(0, 0, 229, 61);
		getContentPane().add(panel);

		//字体选择开始
		fontComboBox = new JComboBox(fontNames);
		fontComboBox.setBorder(new LineBorder(SystemColor.activeCaption, 1, false));
		fontComboBox.setBounds(5, 5, 144, 23);
		panel.add(fontComboBox);
		fontComboBox.setToolTipText("字体选择");
		fontComboBox.setMaximumRowCount(10);
		//事件监听
		fontComboBox.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				tempComboBox = (JComboBox)e.getSource();
				selectedFontName = (String)tempComboBox.getSelectedItem();
				tempFont = diaryText.getFont();
				newFont = new Font(selectedFontName, tempFont.getStyle(), tempFont.getSize());
				diaryText.setFont(newFont);
				currentFont = newFont;
			}
		});		//字体选择结束

		//字体大小
		fontSizeComboBox = new JComboBox(fontSize);
		fontSizeComboBox.setBorder(new LineBorder(SystemColor.activeCaption, 1, false));
		fontSizeComboBox.setBounds(157, 5, 67, 23);
		panel.add(fontSizeComboBox);
		fontSizeComboBox.setToolTipText("字体大小");
		fontSizeComboBox.setMaximumRowCount(10);
		//事件监听
		fontSizeComboBox.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				tempComboBox = (JComboBox)e.getSource();
				selectedSize = (String)tempComboBox.getSelectedItem();
				tempFont = diaryText.getFont();
				newFont = new Font(tempFont.getName(), tempFont.getStyle(), Integer.parseInt(selectedSize));
				diaryText.setFont(newFont);
				currentFont = newFont;
			}
		});		//字体大小结束
		
		//		粗体	
		boldToggleButton.setBorder(new EmptyBorder(0, 0, 0, 0));
		boldToggleButton.setBounds(5, 34, 19, 23);
		panel.add(boldToggleButton);
		boldToggleButton.setIcon(SwingResourceManager.getIcon(ToolBarDialog.class, "/images/bold.gif"));
		boldToggleButton.setToolTipText("粗体");		
		//		事件监听
		boldToggleButton.addMouseListener(myToggleButtonMouseListener);
		
		boldToggleButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent ae) 
			{
				if(boldToggleButton.isSelected())
				{
					if(currentFont.isItalic())
					{
						newFont = new Font(currentFont.getName(), Font.BOLD + Font.ITALIC, currentFont.getSize()); 
						diaryText.setFont(newFont); 		
						currentFont = newFont;
					}
					else
					{
						newFont = new Font(currentFont.getName(), Font.BOLD , currentFont.getSize()); 
						diaryText.setFont(newFont); 
						currentFont = newFont;
					}
				}
				else if( ! boldToggleButton.isSelected())
				{
					if(currentFont.isItalic())
					{
						newFont = new Font(currentFont.getName(), Font.ITALIC + Font.PLAIN, currentFont.getSize()); 
						diaryText.setFont(newFont); 
						currentFont = newFont;
					}
					else
					{
						newFont = new Font(currentFont.getName(), Font.PLAIN, currentFont.getSize()); 
						diaryText.setFont(newFont); 
						currentFont = newFont;
					}
				}
			}
		});		//粗体结束

		//斜体开始		
		italicsToggleButton.setBorder(new EmptyBorder(0, 0, 0, 0));
		italicsToggleButton.setBounds(30, 34, 19, 23);
		panel.add(italicsToggleButton);
		italicsToggleButton.setIcon(SwingResourceManager.getIcon(ToolBarDialog.class, "/images/italic.gif"));
		italicsToggleButton.setToolTipText("斜体");		
		//		事件监听
		italicsToggleButton.addMouseListener(myToggleButtonMouseListener);
		
		italicsToggleButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0)
			{
				if(italicsToggleButton.isSelected())
				{
					if(currentFont.isBold())
					{
						newFont = new Font(currentFont.getName(), Font.BOLD + Font.ITALIC, currentFont.getSize()); 
						diaryText.setFont(newFont); 	//	对选择部分设置italic
						currentFont = newFont;
						}
					else
					{
						newFont = new Font(currentFont.getName(), Font.ITALIC, currentFont.getSize()); 
						diaryText.setFont(newFont); 
						currentFont = newFont;
					}
				}
				else if( ! italicsToggleButton.isSelected())
				{
					if(currentFont.isBold())
					{
						newFont = new Font(currentFont.getName(), Font.BOLD + Font.PLAIN, currentFont.getSize()); 
						diaryText.setFont(newFont); 	//	对选择部分设置italic
						currentFont = newFont;
					}
					else
					{
						newFont = new Font(currentFont.getName(), Font.PLAIN, currentFont.getSize()); 
						diaryText.setFont(newFont); 
						currentFont = newFont;
					}
				}
			}
		});		//斜体结束
		
		//		背景突出		
		highlightToggleButton.setBounds(80, 34, 19, 23);
		highlightToggleButton.setBorder(new EmptyBorder(0, 0, 0, 0));
		highlightToggleButton.setIcon(SwingResourceManager.getIcon(ToolBarDialog.class, "/images/background.gif"));
		highlightToggleButton.setToolTipText("背景突出");
		panel.add(highlightToggleButton);		
		//		事件监听
		highlightToggleButton.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent me)
			{
				if(me.getButton() == MouseEvent.BUTTON3)		//if double clicked
				{
					selectedHighlightColor = JColorChooser.showDialog(ToolBarDialog.this,"Ant_ColorChooser",selectedHighlightColor);
					try {
						diaryText.getHighlighter().addHighlight(DiaryTextAreaView.getSelectionStart(), DiaryTextAreaView.getSelectionEnd(), 
																								new DefaultHighlighter.DefaultHighlightPainter(selectedHighlightColor));						
					} catch (BadLocationException e) {
						 //TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if(me.getClickCount() == doubleClick)		//if double clicked
				{
					diaryText.getHighlighter().removeAllHighlights();
				}
			}
		});
		
		highlightToggleButton.addMouseListener(myToggleButtonMouseListener);
		
		highlightToggleButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				if(highlightToggleButton.isSelected())
				{
					try {
						diaryText.getHighlighter().addHighlight(DiaryTextAreaView.getSelectionStart(), DiaryTextAreaView.getSelectionEnd(), 
																						new DefaultHighlighter.DefaultHighlightPainter(selectedHighlightColor));						
					} catch (BadLocationException e1) 
					{
						// TODO 自动生成 catch 块
						e1.printStackTrace();
					}				
				}				
			}
		});		//背景突出结束
		
		//粘贴		
		pasteButton.setIcon(SwingResourceManager.getIcon(ToolBarDialog.class, "/images/paste1_1.gif"));
		pasteButton.setBorder(new EmptyBorder(0, 0, 0, 0));
		pasteButton.setBounds(180, 34, 19, 23);
		panel.add(pasteButton);
		pasteButton.setToolTipText("粘贴");
		
		//事件监听
		pasteButton.addMouseListener(myButtonMouseListener);
		
		pasteButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				diaryText.paste();
			}
		});		//粘贴结束
		
		//居中		
		makeCenterButton.setBorder(new EmptyBorder(0, 0, 0, 0));
		makeCenterButton.setBounds(55, 34, 19, 23);
		panel.add(makeCenterButton);
		makeCenterButton.setIcon(SwingResourceManager.getIcon(ToolBarDialog.class, "/images/center.gif"));
		makeCenterButton.setToolTipText("居中");		
		//		事件监听
		makeCenterButton.addMouseListener(myButtonMouseListener);
		
		makeCenterButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				try {											
						makeCenterButton.setEnabled(true);
						column = diaryText.getColumns();
						start = diaryText.getSelectionStart();
						end = diaryText.getSelectionEnd();
						startLine = diaryText.getLineOfOffset(start);
						endLine = diaryText.getLineOfOffset(end);
						for(int i = startLine; i <= endLine; i++)
						{
							head = diaryText.getLineStartOffset(i);
							tail = diaryText.getLineEndOffset(i);
							blanks = (column - (tail - head));
							for(int j = 1; j < blanks; j++)
							{
								blankString = "\0";
								blankString = blankString.concat("\0");
							}
							diaryText.insert(blankString, head);
					}					
				} catch (BadLocationException e) 
				{
					// TODO 自动生成 catch 块
					e.printStackTrace();
				}
			}
		});		//居中结束		
		
		//颜色选择	
		colorButton.setBorder(new EmptyBorder(0, 0, 0, 0));
		colorButton.setBounds(105, 35, 19, 23);
		panel.add(colorButton);
		colorButton.setIcon(SwingResourceManager.getIcon(ToolBarDialog.class, "/images/color.gif"));
		colorButton.setToolTipText("颜色选择");
		colorButton.setFont(new Font("@Arial Unicode MS", Font.PLAIN, 12));		
		//		事件监听
		colorButton.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent me)
			{
				if(me.getButton() == MouseEvent.BUTTON1)		//if double clicked
				{
					textColor = JColorChooser.showDialog(ToolBarDialog.this,"Ant_ColorChooser",selectedHighlightColor);	
					diaryText.setForeground(textColor);					
				}
				if(me.getButton() == MouseEvent.BUTTON3)		//if double clicked
				{
					backgroundColor = JColorChooser.showDialog(ToolBarDialog.this,"Ant_ColorChooser",selectedHighlightColor);	
					diaryText.setBackground(backgroundColor);					
				}
			}
		});		//颜色选择结束

		//复制	
		copyButton.setIcon(SwingResourceManager.getIcon(ToolBarDialog.class, "/images/copy1_1.gif"));
		copyButton.setBorder(new EmptyBorder(0, 0, 0, 0));
		copyButton.setBounds(157, 34, 19, 23);
		panel.add(copyButton);
		copyButton.setIcon(SwingResourceManager.getIcon(ToolBarDialog.class, "/images/copy1_1.gif"));
		copyButton.setToolTipText("复制");		
		//		事件监听
		copyButton.addMouseListener(myButtonMouseListener);
		
		copyButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0)
			{
				diaryText.copy();
			}
		});		//复制结束
	
		//插入符号		
		signButton.setBorder(new EmptyBorder(0, 0, 0, 0));
		signButton.setBounds(130, 34, 19, 23);
		panel.add(signButton);
		signButton.setIcon(SwingResourceManager.getIcon(ToolBarDialog.class, "/images/sign.gif"));
		signButton.setToolTipText("特殊符号");		
		//		事件监听
		signButton.addMouseListener(myButtonMouseListener);
		
		signButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0)
			{
				Format.tip();
			}
		});		//插入符号结束
		
		//撤销开始		
		//undoButton.setEnabled(false);
		undoButton.setBorder(new EmptyBorder(0, 0, 0, 0));
		undoButton.setBounds(205, 34, 19, 23);
		panel.add(undoButton);
		undoButton.setIcon(SwingResourceManager.getIcon(ToolBarDialog.class, "/images/cancel1.gif"));
		undoButton.setToolTipText("撤销");		
		//		事件监听
		undoButton.addMouseListener(myButtonMouseListener);
		undoButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0)
			{
				Format.tip();
			}
		});		//撤销结束
		
	}	//构造方法结束
	
	 class MyButtonMouseListener extends MouseAdapter
	{
		public void mouseEntered(MouseEvent e) 
		{
			((JButton)e.getSource()).setBorder(new LineBorder(SystemColor.activeCaption));
		}
		public void mouseExited(MouseEvent e)
		{
			((JButton)e.getSource()).setBorder(new EmptyBorder(0, 0, 0, 0));
		}
	}
	 class MyToggleButtonMouseListener extends MouseAdapter
	{
		public void mouseEntered(MouseEvent e) 
		{
			((JToggleButton)e.getSource()).setBorder(new LineBorder(SystemColor.activeCaption));
		}
		public void mouseExited(MouseEvent e)
		{
			((JToggleButton)e.getSource()).setBorder(new EmptyBorder(0, 0, 0, 0));
		}
	}
	 
	public void actionPerformed(ActionEvent arg0) 
	{
		// TODO Auto-generated method stub
		if(!diaryText.getLineWrap()) //测试条件有待考虑
		{
			makeCenterButton.setEnabled(false);
		}
		else 
		{
			makeCenterButton.setEnabled(true);
		}
	}	
}
