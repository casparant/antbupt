package com.ant_studio;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.swtdesigner.SwingResourceManager;

public class AboutDialog extends JDialog 
{
	/**
	 * “关于”对话框
	 * @author 张金利
	 */
	private static final long serialVersionUID = 1L;	

	private JTabbedPane tabbedPane = new JTabbedPane();
	private 	JPanel copyrightPanel = new JPanel();
	private 		JLabel logoLabel = new JLabel();
	private 		JTextArea thankfulTextArea = new JTextArea();
	private 	JPanel thankfulPanel = new JPanel();
	private 		JLabel iconLabel = new JLabel();
	private 		JTextArea copyrightTextArea = new JTextArea();
	private JPanel okButtonPanel = new JPanel();
	private 	JButton okButton = new JButton();

	/**
	 * Create the dialog
	 */
	public AboutDialog() 
	{
		//初始信息
		super();
		setAlwaysOnTop(true);
		setModal(true);
		setBounds(100, 100, 500, 300);
		setTitle("关于本程序...");

		//标签面板
		getContentPane().add(tabbedPane, BorderLayout.CENTER);
		tabbedPane.setBackground(Color.LIGHT_GRAY);

		//版权标签
		tabbedPane.addTab("版权声明", null, copyrightPanel, null);
		copyrightPanel.setBackground(Color.LIGHT_GRAY);
		copyrightPanel.setLayout(null);

		//Logo图标
		copyrightPanel.add(logoLabel);
		logoLabel.setBounds(21, 53, 49, 84);
		logoLabel.setIcon(SwingResourceManager.getIcon(AboutDialog.class, "/images/diary3.gif"));

		//版权文本域
		copyrightPanel.add(copyrightTextArea);
		copyrightTextArea.setBounds(76, 33, 376, 141);
		copyrightTextArea.setBackground(Color.LIGHT_GRAY);
		copyrightTextArea.setEditable(false);
		copyrightTextArea.setLineWrap(true);
		copyrightTextArea.setText("Java日记本 V1.0.0\r\n开发团队：蚂蚁工作室(Ant-Studio) 版权所有(C) 2007-2008\r\n开发者：谢帆、李慧颖、张金利、陶睿、黄文灏\r\n\r\n敬告：本程序受中华人民共和国版权法和国际版权公约的保护。非法使用本程序可能会带来严重的法律后果。请查阅使用手册中的用户许可协议以了解您的权利和义务。");

		//“感谢”标签
		tabbedPane.addTab("感  谢", null, thankfulPanel, null);
		thankfulPanel.setBackground(Color.LIGHT_GRAY);
		thankfulPanel.setLayout(null);

		//图标
		thankfulPanel.add(iconLabel);
		iconLabel.setBounds(13, 64, 58, 69);
		iconLabel.setIcon(SwingResourceManager.getIcon(AboutDialog.class, "/images/thanks.gif"));

		//“感谢”文本域
		thankfulPanel.add(thankfulTextArea);
		thankfulTextArea.setBounds(77, 10, 364, 171);
		thankfulTextArea.setBackground(Color.LIGHT_GRAY);
		thankfulTextArea.setText(
				"Sun Microsystems, Inc. (http://java.sun.com/)" +
				"\n感谢他们开发出Java这样一个神奇的开发工具。" + 
				"\n\n胡映 -- 原《Java日记本》作者" +
				"\n感谢他制作了如此出色的日记本，我们的开发是以此为模板的。" +
				"\n\n感谢所有使用本程序的用户。"+ 
				"\n感谢所有关心本程序发展，给予建议和支持的朋友。");
		
		//确认按钮面板
		getContentPane().add(okButtonPanel, BorderLayout.SOUTH);
		okButtonPanel.setBackground(Color.LIGHT_GRAY);

		//确认按钮
		okButton.setMargin(new Insets(2, 5, 2, 5));
		okButton.setMnemonic(KeyEvent.VK_O);
		okButton.setIcon(SwingResourceManager.getIcon(AboutDialog.class, "/images/okey.gif"));
		okButton.setText("确认");
		okButtonPanel.add(okButton);

		okButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(final ActionEvent arg0) 
			{
				dispose();
			}
		});
	}	//AboutDialog() 构造方法结束
}	//AboutDialog 类结束
