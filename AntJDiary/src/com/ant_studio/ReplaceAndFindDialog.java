package com.ant_studio;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

import javax.swing.JDialog;


public class ReplaceAndFindDialog extends JDialog {

	/**
	 * 替换中的查找替换对话框
	 * author：黄文灏
	 * 
	 */
	private static final long serialVersionUID = 1L;

	

	/**
	 * Create the dialog
	 */
	public ReplaceAndFindDialog(final String findWords,final String replaceWords) {
		super();
		
		setTitle("替换...");
		setResizable(false);
		getContentPane().setLayout(null);
		setSize(521, 68);

		final JButton findNextButton = new JButton();
		findNextButton.setText("查找下一个(F)");
		findNextButton.setBounds(0, 0, 129, 36);
		getContentPane().add(findNextButton);
		
		findNextButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String text = DiaryTextAreaView.getTextArea().getText();
				int caretPosition = DiaryTextAreaView.getTextArea().getCaretPosition();
				String text2 = text.substring(caretPosition,text.length()) + text.substring(0,caretPosition);
		    	int start = text2.indexOf(findWords);
		    	
				if(start>=(text.length()-caretPosition))start = start - (text.length()-caretPosition);
		    	else start = start + caretPosition;
		    	
				int end = start + findWords.length();
		    	
		    	if(end>text.length())
				{
					start = text.indexOf(findWords);
					end = start + findWords.length();
				}
				DiaryTextAreaView.getTextArea().requestFocusInWindow();
				DiaryTextAreaView.getTextArea().select(start, end);
			}
		});

		final JButton replaceButton = new JButton();
		replaceButton.setText("替换(R)");
		replaceButton.setBounds(128, 0, 129, 36);
		getContentPane().add(replaceButton);

		replaceButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				DiaryTextAreaView.getTextArea().replaceSelection(replaceWords);
				
				String text = DiaryTextAreaView.getTextArea().getText();
				int caretPosition = DiaryTextAreaView.getTextArea().getCaretPosition();
				String text2 = text.substring(caretPosition,text.length()) + text.substring(0,caretPosition);
		    	int start = text2.indexOf(findWords);
		    	
		    	if(start==-1)dispose();
				
		    	if(start>=(text.length()-caretPosition))start = start - (text.length()-caretPosition);
		    	else start = start + caretPosition;
		    	
		    	int end = start + findWords.length();
		    	
		    	if(end>text.length())
				{
					start = text.indexOf(findWords);
					end = start + findWords.length();
				}
		    	
				DiaryTextAreaView.getTextArea().requestFocusInWindow();
				DiaryTextAreaView.getTextArea().select(start, end);
			}
		});
		
		final JButton replaceAllButton = new JButton();
		replaceAllButton.setText("全部替换(A)");
		replaceAllButton.setBounds(256, 0, 129, 36);
		getContentPane().add(replaceAllButton);
        
		replaceAllButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String text = DiaryTextAreaView.getTextArea().getText();
				text = text.replaceAll(findWords,replaceWords);
				DiaryTextAreaView.getTextArea().setText(text);
				dispose();
			}
		});

		final JButton cancelButton = new JButton();
		cancelButton.setText("取消(C)");
		cancelButton.setBounds(385, 0, 129, 36);
		getContentPane().add(cancelButton);
		
		cancelButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				dispose();
			}
		});
		//
	}

}
