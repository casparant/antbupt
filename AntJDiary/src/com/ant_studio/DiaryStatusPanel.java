package com.ant_studio;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.*;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.BadLocationException;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

public class DiaryStatusPanel
{
	/**
	 * 缺省串行版本标识
	 */
	private static final long serialVersionUID = 1L;

	private JLabel todayLabel = new JLabel();
	private JLabel currentRCLabel = new JLabel();
	private JLabel totalByteLabel = new JLabel();
	
	private JTextArea diaryText = DiaryTextAreaView.getTextArea();
	private int column, row, bytes;
	private int currentCaretPosition = 0;
	/**
	 * 构造状态栏面板
	 */
	public DiaryStatusPanel(final JPanel statusPanel)
	{
		statusPanel.setLayout(new FormLayout(
			new ColumnSpec[] 
			{
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC
			},
			new RowSpec[] 
			{
				FormFactory.DEFAULT_ROWSPEC
			}));

		//显示日期
		todayLabel.setText("今天是：" + Format.today());
		statusPanel.add(todayLabel, new CellConstraints(1, 1));		//显示日期结束

		//显示行列数
		statusPanel.add(currentRCLabel, new CellConstraints(3, 1));
		
		diaryText.addCaretListener(new CaretListener()
		{
			public void caretUpdate(CaretEvent arg0) {
			   currentCaretPosition = diaryText.getCaretPosition();				
				try {
					row = diaryText.getLineOfOffset(currentCaretPosition) ;
				} catch (BadLocationException e) {
					e.printStackTrace();
				}				
				try {
					column = currentCaretPosition - diaryText.getLineStartOffset(row) ;
				} catch (BadLocationException e) {
					e.printStackTrace();
				}
				/*方案二：
				 * currentCaretPosition = diaryText.getCaretPosition();
				 * totalColumn = diaryText.getColumns();
				 * column = currentCaretPosition % totalColumn;	这个算法耗时多
				 * row = currentCaretPosition / totalColumn;
				 * */
				currentRCLabel.setText("当前行数：	" + (row+1)+ " " +"列数：	" + column);
			}
		});			//显示行列数结束
	
		//显示字节数			
		statusPanel.add(totalByteLabel, new CellConstraints(5, 1));
		diaryText.addKeyListener(new KeyAdapter() 
		{
			public void keyTyped(KeyEvent e) 
			{
				bytes = diaryText.getText().length();
				totalByteLabel.setText("文章总字数：" + bytes);	
			}
		});		//显示字节数结束
	}	//状态栏构造方法结束
}	//DiaryStatusPanel类结束