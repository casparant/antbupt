package com.ant_studio;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

public class DataSecurity {
	
	private static byte[] defaultKey = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a',
        'b', 'c', 'd', 'e', 'f' };
        
	private static byte[] key = defaultKey;
	
	/**
	 * The setKey method applies MD5 to user name and password to produce
	 * the key to the Blowfish encoding algorithm.
	 */
	public static void setKey(String password) throws Exception
	{
		if(password == null)
		{
			key = defaultKey;
		}
		else
		{
			String temp = new String();
			temp = password;
			key = MD5(temp.getBytes());
		}
	}

	/**
	 * The MD5 method uses MD5 to digest information
	 */
	public static byte[] MD5(byte[] input) throws Exception
	{
		java.security.MessageDigest alg=java.security.MessageDigest.getInstance("MD5");
	    
	    alg.update(input);
	    byte[] digest = alg.digest();
	  
	    return digest;
	}
	public static byte[] encode(byte[] input) throws Exception
	{
		try
		{
			SecretKey deskey = new javax.crypto.spec.SecretKeySpec(key, "Blowfish");
			
			Cipher c1 = Cipher.getInstance("Blowfish");
			c1.init(Cipher.ENCRYPT_MODE, deskey);
			byte[] cipherByte = c1.doFinal(input);
			
			return cipherByte;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
			
		}
	}
	
	@SuppressWarnings("static-access")
	public static byte[] decode (byte[] input) throws Exception
	{
		try
		{
			SecretKey deskey = new javax.crypto.spec.SecretKeySpec(key, "Blowfish");
			
			Cipher c1 = Cipher.getInstance("Blowfish");
			c1.init(c1.DECRYPT_MODE, deskey);
			byte[] clearByte = c1.doFinal(input);
	
			return clearByte;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean IsByteArrayTheSame(byte[] arr1, byte[] arr2)
	{
		if(arr1.length != arr2.length)
			return false;
		else
		{
			for(int i = 0; i < arr1.length; i++)
			{
				if(arr1[i] != arr2[i])
					return false;
			}
			
			return true;
		}
	}
}
