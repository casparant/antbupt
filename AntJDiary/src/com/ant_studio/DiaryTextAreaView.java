package com.ant_studio;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import com.ant_studio.mvc.*;
import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

public class DiaryTextAreaView extends WmvcView
{
	private DiaryModel myDiaryModel;
	private static JTextArea diaryText;
	private static Diary diary;
	
	private static int selectionStart, selectionEnd;
	//private int totalColumn = 0;
	
	private 			final JPopupMenu popupMenu = new JPopupMenu();
	private 				final JMenuItem cutPopupMenuItem = new JMenuItem();
	private 				final JMenuItem copyPopupMenuItem = new JMenuItem();
	private 				final JMenuItem pastePopupMenuItem = new JMenuItem();
	private 				final JMenuItem deletePopupMenuItem = new JMenuItem();
	private				final JMenuItem allSelectPopupMenuItem = new JMenuItem();		

	private 		ToolBarDialog toolBarDialog;	
	private 		boolean isCaretChanged = false;
	private		boolean isMouseDragged = false;
	private 		boolean isToolBarDialogShowing = false;
	//构造函数
	public DiaryTextAreaView()
	{
		myDiaryModel = JDiary.getModel();
		myDiaryModel.addView(this);

		diaryText = new JTextArea();
		try
		{
			diaryText.setText(myDiaryModel.getCurrentDiary().getMainText());
		}
		catch (Exception e)
		{
			new DiaryModel().getFile();
		}
		diaryText.setEditable(true);
		diaryText.setLineWrap(true);

		//		textArea添加右键菜单
		popupMenu.setBorder(new BevelBorder(BevelBorder.RAISED));
		addPopup(diaryText, popupMenu);
		
		//textArea添加右键菜单项开始
		//剪切菜单项开始
		cutPopupMenuItem.setText("剪切(T)");
		//cutPopupMenuItem.setBounds(1, 1, 30,14);
		cutPopupMenuItem.setMnemonic(KeyEvent.VK_T);
		popupMenu.add(cutPopupMenuItem);
		
		cutPopupMenuItem.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				try
				{
					diaryText.cut();
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
		});		//剪切菜单项结束

		//复制菜单项开始
		copyPopupMenuItem.setText("复制(C)");
		copyPopupMenuItem.setMnemonic(KeyEvent.VK_C);
		popupMenu.add(copyPopupMenuItem);
		
		copyPopupMenuItem.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				try
				{
					diaryText.copy();
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
		});	//复制菜单项结束

		//粘贴菜单项开始
		pastePopupMenuItem.setText("粘贴(P)");
		pastePopupMenuItem.setMnemonic(KeyEvent.VK_P);
		popupMenu.add(pastePopupMenuItem);
		
		pastePopupMenuItem.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				try
				{
					diaryText.paste();
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
		});	//粘贴菜单项结束

		//删除菜单项开始
		deletePopupMenuItem.setText("清除(D)");
		deletePopupMenuItem.setMnemonic(KeyEvent.VK_D);
		popupMenu.add(deletePopupMenuItem);
		
		deletePopupMenuItem.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				try
				{
					diaryText.replaceSelection("");
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
		});	//删除菜单项结束

		//菜单分隔符
		popupMenu.addSeparator();
			
		//全选菜单项开始
		allSelectPopupMenuItem.setText("全选(A)");
		allSelectPopupMenuItem.setMnemonic(KeyEvent.VK_A);;
		popupMenu.add(allSelectPopupMenuItem);
		
		allSelectPopupMenuItem.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				try
				{
					diaryText.selectAll();
					updateView();
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
		});		//全选菜单项结束		
		//diaryText添加右键菜单项结束	
		
		//		diaryText添加事件监听
		//		diaryText添加CaretListener
		diaryText.addCaretListener(new CaretListener() 
		{
			public void caretUpdate(CaretEvent ce) 
			{				
				try
				{				
					if((ce.getDot() - ce.getMark()) != 0) isCaretChanged = true;	
					selectionStart = diaryText.getSelectionStart();
					selectionEnd = diaryText.getSelectionEnd();					
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
		});		
		//		diaryText添加MouseListener
		diaryText.addMouseListener(new MouseAdapter() 
		{											
			public void mouseReleased(MouseEvent me) 
			{
				try
				{		
					if( isCaretChanged && isMouseDragged  && ! isToolBarDialogShowing)
					{
						toolBarDialog = new ToolBarDialog();
						toolBarDialog.setLocation(diaryText.getLocationOnScreen().x + me.getX(),diaryText.getLocationOnScreen().y + me.getY() + 20);
						toolBarDialog.setAlwaysOnTop(true);
						/*toolBarDialog.addMouseListener(new MouseAdapter() 
						{
							public void mouseEntered(MouseEvent e)
							{
								toolBarDialog.setVisible(true);
							}
						});*/
						toolBarDialog.setVisible(true);
						isToolBarDialogShowing = true;
						isCaretChanged = false;
						isMouseDragged = false;
						//if(toolBarDialog.isFocusableWindow() == false)toolBarDialog.dispose();
					}
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
			public void mouseClicked(MouseEvent e) 
			{
				if(isToolBarDialogShowing && diaryText.isFocusOwner())
					{
					toolBarDialog.dispose();
					isToolBarDialogShowing = false;
					}
			}	
		});
		
		//	diaryText添加MouseMotionListener
		diaryText.addMouseMotionListener(new MouseMotionAdapter() 
		{
			public void mouseDragged(MouseEvent e) 
			{
				isMouseDragged = true;
			}
		});				//diaryText事件监听结束		
	}		//构造函数结束
	
	/**
	 * @return selectionStart
	 */
	public static int getSelectionStart()
	{
		return selectionStart;
	}
	
	/**
	 * @return selectionEnd
	 */
	public static int getSelectionEnd()
	{
		return selectionEnd;
	}
	
	/**
	 * @return JTextArea
	 */
	public static JTextArea getTextArea()
	{
		return diaryText;
	}
	
	/**
	 * 在MVC中使用
	 */
	public void updateView()
	{
		diary = myDiaryModel.getCurrentDiary();
		diaryText.setText(diary.getMainText());
	}
	
	
	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger())
					showMenu(e);
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger())
					showMenu(e);
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
	
	/**
	 * 在保存中使用
	 * @return mainText
	 */
	public String getText()
	{
		return diaryText.getText();
	}
}
