package com.ant_studio;

import javax.swing.*;
import java.awt.event.*;
import com.swtdesigner.SwingResourceManager;

public class DiaryMenuBar
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JMenu fileMenu = new JMenu();
	private	JMenuItem newMenuItem = new JMenuItem();
	private 	JMenuItem deleteMenuItem = new JMenuItem();
	private	JMenuItem saveMenuItem = new JMenuItem();
	private	JMenuItem importsMenuItem = new JMenuItem();
	private	JMenuItem exportsMenuItem = new JMenuItem();
	private	JMenuItem printMenuItem = new JMenuItem();
	private	JMenuItem exitMenuItem = new JMenuItem();	
	private JMenu editMenu = new JMenu();
	private	JMenuItem cutMenuItem = new JMenuItem();
	private	JMenuItem copyMenuItem = new JMenuItem();
	private	JMenuItem pasteMenuItem = new JMenuItem();
	private	JMenuItem clearMenuItem = new JMenuItem();
	private	JMenuItem findMenuItem = new JMenuItem();
	private	JMenuItem replaceMenuItem = new JMenuItem();
	private	JMenuItem selectAllMenuItem = new JMenuItem();		
	private JMenu settingMenu = new JMenu();
	private	static JCheckBoxMenuItem autoLineCheckBoxMenuItem = new JCheckBoxMenuItem();
	private	static JCheckBoxMenuItem readOnlyCheckBoxMenuItem = new JCheckBoxMenuItem();
	private	static JCheckBoxMenuItem bgMusicCheckBoxMenuItem = new JCheckBoxMenuItem();
	private	JMenuItem optionMenuItem = new JMenuItem();		
	private JMenu advancedMenuItem = new JMenu();
	private	JMenuItem advancedSearchMenuItem = new JMenuItem();
	private	JMenuItem appendDateTimeMenuItem = new JMenuItem();
	private	JMenuItem importFromFileMenuItem = new JMenuItem();
	private	JMenuItem backupMenuItem = new JMenuItem();		
	private JMenu helpMenu = new JMenu();
	private	JMenuItem helpSubjectMenuItem = new JMenuItem();
	private	JMenuItem aboutMenuItem = new JMenuItem();
	
	/**
	 * 构造菜单栏组件
	 */
	public DiaryMenuBar(final JMenuBar menuBar) 
	{
		//文件菜单栏
		fileMenu.setMnemonic(KeyEvent.VK_F);
		fileMenu.setText("文件(F)");
		menuBar.add(fileMenu);
		
		{
			//新建日记
			newMenuItem.setToolTipText("新建一则日记");
			newMenuItem.setMnemonic(KeyEvent.VK_N);
			newMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
			newMenuItem.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/diary.gif"));
			newMenuItem.setText("新建(N)...");
			fileMenu.add(newMenuItem);
			
			newMenuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					new ChooseDateDialog().setVisible(true);
					Operation.createNewDiary(ChooseDateDialog.getFlag(), DiaryHeadPanel.getChooseDateLabel());
				}
			});

			fileMenu.addSeparator();
	
			//删除日记
			deleteMenuItem.setToolTipText("删除所选日记");
			deleteMenuItem.setMnemonic(KeyEvent.VK_D);
			deleteMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, InputEvent.CTRL_MASK));
			deleteMenuItem.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/delete.gif"));
			deleteMenuItem.setText("删除(D)");
			fileMenu.add(deleteMenuItem);
			
			deleteMenuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					Operation.deleteDiary();
				}
			});
	
			//保存日记
			saveMenuItem.setToolTipText("保存所有改动");
			saveMenuItem.setMnemonic(KeyEvent.VK_S);
			saveMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
			saveMenuItem.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/save.gif"));
			saveMenuItem.setText("保存(S)");
			fileMenu.add(saveMenuItem);
			
			saveMenuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					if(OptionDialog.getCheckBox3().isSelected())DiaryTextAreaView.getTextArea().append("\n\n修改时间：" + Format.currentTime());
					JDiary.getModel().getCurrentDiary().setMainText( JDiary.getTextView().getText() );
					Operation.save();
					JOptionPane.showMessageDialog(saveMenuItem, "日记已经保存！", "信息", JOptionPane.INFORMATION_MESSAGE);
				}
			});

			fileMenu.addSeparator();
	
			//批量导入
			importsMenuItem.setToolTipText("从指定的目录导入日记");
			importsMenuItem.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/import.gif"));
			importsMenuItem.setText("批量导入...");
			fileMenu.add(importsMenuItem);
			
			importsMenuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					Format.tip();
				}
			});
	
			//批量导出
			exportsMenuItem.setToolTipText("把日记导出到指定目录");
			exportsMenuItem.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/export.gif"));
			exportsMenuItem.setText("批量导出...");
			fileMenu.add(exportsMenuItem);
			
			exportsMenuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					Format.tip();
				}
			});
			
			fileMenu.addSeparator();
	
			//打印日记
			printMenuItem.setToolTipText("打印所选日记");
			printMenuItem.setMnemonic(KeyEvent.VK_P);
			printMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_MASK));
			printMenuItem.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/print.gif"));
			printMenuItem.setText("日记打印(P)");
			fileMenu.add(printMenuItem);
			
			printMenuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					Format.tip();
				}
			});
			
			fileMenu.addSeparator();
	
			//退出日记本
			exitMenuItem.setToolTipText("退出应用程序");
			exitMenuItem.setMnemonic(KeyEvent.VK_X);
			exitMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.ALT_MASK));
			exitMenuItem.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/exit.gif"));
			exitMenuItem.setText("退出");
			fileMenu.add(exitMenuItem);	
			
			exitMenuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					System.exit(0);
				}
			});
		}	//文件菜单栏结束
	

		//编辑菜单栏
		editMenu.setMnemonic(KeyEvent.VK_E);
		editMenu.setText("编辑(E)");
		menuBar.add(editMenu);
		{				
			//剪切
			cutMenuItem.setToolTipText("剪切指定内容到剪贴板");
			cutMenuItem.setMnemonic(KeyEvent.VK_T);
			cutMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_MASK));
			cutMenuItem.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/cut.gif"));
			cutMenuItem.setText("剪切(T)");
			editMenu.add(cutMenuItem);
			
			cutMenuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					DiaryTextAreaView.getTextArea().cut();
				}
			});
			
			//复制
			copyMenuItem.setToolTipText("复制指定内容到剪贴板");
			copyMenuItem.setMnemonic(KeyEvent.VK_C);
			copyMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK));
			copyMenuItem.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/copy.gif"));
			copyMenuItem.setText("复制(C)");
			editMenu.add(copyMenuItem);
			
			copyMenuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					DiaryTextAreaView.getTextArea().copy();
				}
			});
			
			//粘贴				
			pasteMenuItem.setToolTipText("从剪贴板粘贴");
			pasteMenuItem.setMnemonic(KeyEvent.VK_P);
			pasteMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_MASK));
			pasteMenuItem.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/paste.gif"));
			pasteMenuItem.setText("粘贴(P)");
			editMenu.add(pasteMenuItem);
			
			pasteMenuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					DiaryTextAreaView.getTextArea().paste();
				}
			});
			
			//清除
			clearMenuItem.setToolTipText("清除所选内容");
			clearMenuItem.setMnemonic(KeyEvent.VK_D);
			clearMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
			clearMenuItem.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/clear.gif"));
			clearMenuItem.setText("清除(D)");
			editMenu.add(clearMenuItem);
			
			clearMenuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					DiaryTextAreaView.getTextArea().replaceSelection("");
				}
			});
			
			editMenu.addSeparator();

			//查找
			findMenuItem.setToolTipText("查找文字");
			findMenuItem.setMnemonic(KeyEvent.VK_F);
			findMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_MASK));
			findMenuItem.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/find.gif"));
			findMenuItem.setText("查找(F)...");
			editMenu.add(findMenuItem);
			
			findMenuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					new FindDialog().setVisible(true);
				}
			});
			
			//替换
			replaceMenuItem.setMnemonic(KeyEvent.VK_R);
			replaceMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK));
			replaceMenuItem.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/replace.gif"));
			replaceMenuItem.setText("替换(R)...");
			editMenu.add(replaceMenuItem);
			
			replaceMenuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					new ReplaceDialog().setVisible(true);
				}
			});
			
			editMenu.addSeparator();

			//全选
			selectAllMenuItem.setToolTipText("选择全部文本");
			selectAllMenuItem.setMnemonic(KeyEvent.VK_A);
			selectAllMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK));
			selectAllMenuItem.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/blank.gif"));
			selectAllMenuItem.setText("全选(A)");
			editMenu.add(selectAllMenuItem);
			
			selectAllMenuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					DiaryTextAreaView.getTextArea().selectAll();
				}
			});
		}	//编辑菜单栏结束

		//设置菜单栏
		settingMenu.setMnemonic(KeyEvent.VK_V);
	    settingMenu.setText("设置(V)");
	    menuBar.add(settingMenu);	
		{
			//自动换行
			autoLineCheckBoxMenuItem.setToolTipText("是否自动换行");
			autoLineCheckBoxMenuItem.setSelected(true);
			autoLineCheckBoxMenuItem.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/wordwrap.gif"));
			autoLineCheckBoxMenuItem.setText("自动换行");
			settingMenu.add(autoLineCheckBoxMenuItem);
			
			autoLineCheckBoxMenuItem.addItemListener(new ItemListener()
			{
				public void itemStateChanged(ItemEvent e)
				{
					Operation.autoLine(DiaryToolBar.getAutoLineToggleButton(), DiaryTextAreaView.getTextArea(), autoLineCheckBoxMenuItem);
				}
			});
			
			//只读
			readOnlyCheckBoxMenuItem.setToolTipText("是否设为只读模式");
			readOnlyCheckBoxMenuItem.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/readonly.gif"));
			readOnlyCheckBoxMenuItem.setText("只读");
			settingMenu.add(readOnlyCheckBoxMenuItem);
			
			readOnlyCheckBoxMenuItem.addItemListener(new ItemListener()
			{
				public void itemStateChanged(ItemEvent e)
				{
					Operation.readOnly(DiaryToolBar.getReadOnlyToggleButton(), DiaryTextAreaView.getTextArea(), readOnlyCheckBoxMenuItem);
				}
			});
			
			//背景音乐
			bgMusicCheckBoxMenuItem.setToolTipText("是否开启背景音乐");
			bgMusicCheckBoxMenuItem.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/midi.gif"));
			bgMusicCheckBoxMenuItem.setText("背景音乐");
			settingMenu.add(bgMusicCheckBoxMenuItem);
			
			bgMusicCheckBoxMenuItem.addItemListener(new ItemListener()
			{
				public void itemStateChanged(ItemEvent e)
				{
					Operation.bgMusic(DiaryToolBar.getBgMusicToggleButton(), bgMusicCheckBoxMenuItem);
				}					
			});

			settingMenu.addSeparator();

			//选项
			optionMenuItem.setToolTipText("设置应用程序选项");
			optionMenuItem.setMnemonic(KeyEvent.VK_O);
			optionMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
			optionMenuItem.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/option.gif"));
			optionMenuItem.setText("选项(O)...");
			settingMenu.add(optionMenuItem);
			
			optionMenuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					new OptionDialog().setVisible(true);
				}
			});
		}	//设置菜单栏结束

		//高级选项菜单栏
		advancedMenuItem.setMnemonic(KeyEvent.VK_O);
	    advancedMenuItem.setText("高级(O)");
	    menuBar.add(advancedMenuItem);
	    {
			//高级查找
			advancedSearchMenuItem.setToolTipText("对所有日记进行查找");
			advancedSearchMenuItem.setMnemonic(KeyEvent.VK_F);
			advancedSearchMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F3, InputEvent.CTRL_MASK));
			advancedSearchMenuItem.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/advfind.gif"));
			advancedSearchMenuItem.setText("高级查找(F)");
			advancedMenuItem.add(advancedSearchMenuItem);
			
			advancedSearchMenuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					new AdvancedSearchDialog().setVisible(true);
				}
			});

			advancedMenuItem.addSeparator();

			//插入当前时间和日期
			appendDateTimeMenuItem.setToolTipText("插入当前日期时间");
			appendDateTimeMenuItem.setMnemonic(KeyEvent.VK_D);
			appendDateTimeMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
			appendDateTimeMenuItem.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/insertdate.gif"));
			appendDateTimeMenuItem.setText("插入时间和日期(D)");
			advancedMenuItem.add(appendDateTimeMenuItem);
			
			appendDateTimeMenuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					DiaryTextAreaView.getTextArea().append("\n" + Format.currentTime());
				}
			});

			//从文件导入文本
			importFromFileMenuItem.setMnemonic(KeyEvent.VK_E);
			importFromFileMenuItem.setToolTipText("从一个文本文件选择内容粘贴到日记中");
			importFromFileMenuItem.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/blank.gif"));
			importFromFileMenuItem.setText("从文件导入文本(E)...");
			advancedMenuItem.add(importFromFileMenuItem);
			
			importFromFileMenuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					try {
						ImportTextFromFiles window = new ImportTextFromFiles();
						window.setVisible(true);
					} catch (Exception ee) {
						ee.printStackTrace();
					}
				}
			});

			advancedMenuItem.addSeparator();

			//数据备份
			backupMenuItem.setToolTipText("数据备份");
			backupMenuItem.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/blank.gif"));
			backupMenuItem.setText("数据备份(B)...");
			advancedMenuItem.add(backupMenuItem);
			
			backupMenuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					Format.tip();
				}
			});
		}	//高级菜单栏结束

		//帮助菜单栏
		helpMenu.setMnemonic(KeyEvent.VK_H);
	    helpMenu.setText("帮助(H)");
	    menuBar.add(helpMenu);
		{
			//使用手册
			helpSubjectMenuItem.setToolTipText("教会您如何安装和使用本软件");
			helpSubjectMenuItem.setMnemonic(KeyEvent.VK_H);
			helpSubjectMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
			helpSubjectMenuItem.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/help.gif"));
			helpSubjectMenuItem.setText("使用手册(H)...");
			helpMenu.add(helpSubjectMenuItem);
			
			helpSubjectMenuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					try
					{
						Runtime.getRuntime().exec("hh.exe"+ " " + "help.chm");
					}
					catch (Exception er)
					{
						er.printStackTrace();
					}
				}
			});
			
			helpMenu.addSeparator();

			//关于本软件
			aboutMenuItem.setMnemonic(KeyEvent.VK_A);
			aboutMenuItem.setToolTipText("本软件的版权信息");
			aboutMenuItem.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/about.gif"));
			aboutMenuItem.setText("关于本软件(A)...");
			helpMenu.add(aboutMenuItem);
			
			aboutMenuItem.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					new AboutDialog().setVisible(true);
				}
			});
		}	//帮助菜单栏结束
	}	//组件构造方法结束
	
	/**
	 * 以下三个静态方法分别返回三个复选菜单项组件：
	 * 自动换行 复选菜单项
	 * 只读模式 复选菜单项
	 * 背景音乐 复选菜单项
	 */
	public static JCheckBoxMenuItem getAutoLineCheckBoxMenuItem()
	{
		return autoLineCheckBoxMenuItem;
	}

	public static JCheckBoxMenuItem getReadOnlyCheckBoxMenuItem()
	{
		return readOnlyCheckBoxMenuItem;
	}
	
	public static JCheckBoxMenuItem getBgMusicCheckBoxMenuItem()
	{
		return bgMusicCheckBoxMenuItem;
	}
	
}	//DiaryMenuBar 类结束
