package com.ant_studio;

import javax.swing.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Format 
{
	public static final int THIS_YEAR = new GregorianCalendar().get(Calendar.YEAR),
								THIS_MONTH = new GregorianCalendar().get(Calendar.MONTH) + 1,
								TODAY = new GregorianCalendar().get(Calendar.DATE);

	public Format()
	{
		
	}
	
	public static String date(int year, int month, int day)
	{
		return (Integer.toString(year) + " - " + Integer.toString(month) + " - " +Integer.toString(day));
	}
	
	public static String today()
	{
		return new SimpleDateFormat("yyyy年MM月dd日").format(new Date());
	}
	
	public static String currentTime()
	{
		return new SimpleDateFormat("yyyy年MM月dd日 hh:mm:ss").format(new Date());
	}
	
	public static void tip()
	{
		JOptionPane.showMessageDialog(null, "此功能视情况将在今后版本中实现", "提示", JOptionPane.WARNING_MESSAGE);
	}
	
	public static int getDayOfMonth(int year, int month)
	{
			int theMonth[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
			if (new GregorianCalendar().isLeapYear(year) && month == 2)
				return theMonth[month-1] + 1;
			else
				return theMonth[month-1];
	}
	
	public static int getWeekIndex(int year, int month)
	{
		int theMonth[] = {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334},
		previousYear = year - 1,
		previousDay = (previousYear) * 365 + (previousYear) / 4 - (previousYear) / 100 + (previousYear) / 400 + theMonth[month-1];
		
		if (month - 1 >= 2 && new GregorianCalendar().isLeapYear(year))
			previousDay++;
		return (previousDay % 7 + 1) % 7;
	}
}
