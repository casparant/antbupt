package com.ant_studio;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.*;

import org.jdom.Document;
import org.jdom.Element;

import com.ant_studio.mvc.*;

public class DiaryModel extends WmvcModel 
{
	private static MyVector theTree;
	private String currentDate;	
	private static Diary currentDiary;	
	private File myFile;	
	private boolean editsMade;
	private boolean treeChanged;
	@SuppressWarnings("unused")
	private boolean isInsertTime;
	@SuppressWarnings("unused")
	private boolean isTimeChanged;
	private boolean needRepaint = false;
	
	public DiaryModel()
	{
		editsMade = false;
		treeChanged = false;
		isInsertTime = false;
		isTimeChanged = false;
		
		theTree = new MyVector("我的日记");

		myFile =new File("JDiary.dat");
		currentDate = null;
		loadDiarys(myFile);
	}
	
	/**
	 * 从文件读取日记
	 * @param file
	 */
	private void loadDiarys(File file) 
	{
		if(file != null)
		{
			try
			{
				DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
				buildTreeModel(in);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
	}

	/**
	 * 建立树的模型
	 * @param in
	 * @throws Exception 
	 */
	@SuppressWarnings("unchecked")
	private void buildTreeModel(DataInputStream in) throws Exception 
	{
		//Xml元素的定义开始
		Element root;
		Element tempDateElement;
		Element tempYearElement;
		Element tempMonthElement;
		List yearList;
		List monthList;
		List dateList;
		Iterator itYearList;
		Iterator itMonthList;
		Iterator itDateList;
		String xml;
		//xml元素的定义结束
		
		//树元素定义开始
		Diary tempDiary;
		String year;
		String month;
		String date;
		String mainText;
		String weather;
		//树元素定义结束
		
		String readOut = in.readUTF();//读出经过base64加密的string
		byte[] beforeDecode = new sun.misc.BASE64Decoder().decodeBuffer(readOut);//base64解码得到相应byte数组
		byte[] afterDecode = DataSecurity.decode(beforeDecode);//解密算法解密得到byte数组
		xml = new String(afterDecode);
		root = Xml.stringToXml(xml);
		yearList = root.getChildren();
		itYearList = yearList.iterator();
		
		while(itYearList.hasNext())//年循环开始
		{
			tempYearElement = (Element)itYearList.next();
			monthList = tempYearElement.getChildren();
			itMonthList = monthList.iterator();
			
			while(itMonthList.hasNext())//月循环开始
			{
				tempMonthElement = (Element)itMonthList.next();
				dateList = tempMonthElement.getChildren();
				itDateList = dateList.iterator();
				
				while(itDateList.hasNext())//日循环开始
				{
					tempDateElement = (Element)itDateList.next();
					
					year = tempDateElement.getAttributeValue("year");
					month = tempDateElement.getAttributeValue("month");
					date = tempDateElement.getAttributeValue("date");
					weather = tempDateElement.getAttributeValue("weather");
					mainText = tempDateElement.getText();
					
					tempDiary = new Diary(year, month, date, weather);
					tempDiary.setMainText(mainText);
					
					addDiary(tempDiary);
				}//日循环结束
			}//月循环结束
		}//年循环结束		
	}	//树模型建立方法结束

	/**
	 * 保存日记数据到文件
	 * @param file
	 * @return
	 */

	private boolean saveDiarysAs(File file) 
	{
		if (file == null)
			return false;
		
		if (file != null)
		{		
			try
			{
				DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
				xmlBuild(out);
				out.close();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return false;
			}
		}		
		
		return true;
	}	
	
	/**
	 * 新建一则日记
	 * @param diary
	 */
	@SuppressWarnings("unchecked")
	public void addDiary (Diary diary)//必须设置为final 让内部类用
	{
		if(diary == null)
		{
			return;
		}
		editsMade = true;
		treeChanged = true;
		
		MyVector currentVector = theTree;
		
		//开始年的搜索
		{
			boolean yearExisted = false;
			boolean isYearInteratorEmpty;//防止两次用hasenext()造成判断错误
			MyVector yVector = null;
			
			MyVector theYear = new MyVector(diary.getYear());
			
			
			ListIterator itYear = theTree.listIterator();
			int nextY = 0;// 插入下标

			while (isYearInteratorEmpty = itYear.hasNext()) //防止两次用hasenext()造成判断错误
			{
				nextY = itYear.nextIndex();
				yVector = (MyVector) itYear.next();
				String yAdd = (String) diary.getYear();
				String curYear = yVector.toString();

				if (yAdd.compareTo(curYear) < 0)
					break;
				if (yAdd.compareTo(curYear) == 0)
				{
					yearExisted = true; 
					break;
				}
			}	//end of while
			
			if(yearExisted)
			{
				currentVector = yVector;
			
			}
			else
			{			
				if (!isYearInteratorEmpty) //防止两次用hasenext()造成判断错误
				{
					theTree.add(theYear);
				} 
				else
				{
					theTree.add(nextY,theYear);
				}
				currentVector = theYear;
			
			}
		}   //结束年的搜索
		
		//开始月的搜索
		{
			boolean monthExisted = false;
			boolean isMonthInteratorEmpty;//防止两次用hasenext()造成判断错误
			MyVector mVector = null;
			
			MyVector theMonth = new MyVector(diary.getMonth());
			
			ListIterator itMonth = currentVector.listIterator();
			int nextM = 0;
			
			while(isMonthInteratorEmpty = itMonth.hasNext())//防止两次用hasenext()造成判断错误
			{
				nextM = itMonth.nextIndex();
				mVector = (MyVector)itMonth.next();
				String mAdd = diary.getMonth();
				String curMonth = mVector.toString();
				
				if(mAdd.compareTo(curMonth) < 0)
					break;
				if(mAdd.compareTo(curMonth) == 0)
				{
					monthExisted = true;
					break;
				}
			}
			
			if(monthExisted)
			{
				currentVector = mVector;
			}
			else 
			{
				if (!isMonthInteratorEmpty) //防止两次用hasenext()造成判断错误
				{
					currentVector.add(theMonth);
				} 
				else 
				{
					currentVector.add(nextM,theMonth);
				}
				currentVector = theMonth;				
			}			
		}	//搜索月结束
		
		
		//搜索日开始
		{
			boolean dateExisted = false;
			boolean isDateInteratorEmpty;//防止两次用hasenext()造成判断错误
			Diary d = null;
			
			ListIterator itDate = currentVector.listIterator();
			int nextD = 0;
			@SuppressWarnings("unused")
			boolean a;
			while(isDateInteratorEmpty = itDate.hasNext())//防止两次用hasenext()造成判断错误
			{
				nextD = itDate.nextIndex();
				d = (Diary) itDate.next();
				String dAdd = diary.getDate();
				String curDate = d.getDate();
				if( a = (dAdd.compareTo(curDate) < 0))
				{
					break;
				}
				if(dAdd.compareTo(curDate) == 0)
				{
					dateExisted = true;
					break;
				}				
			}
			
			if(dateExisted)
			{
				d = diary;
				return;
			} 
			else 
			{
				if (!isDateInteratorEmpty) //防止两次用hasenext()造成判断错误
				{
					currentVector.add(diary);
				}
				else 
				{
					currentVector.add(nextD,diary);
				}
			}
			
		}	//日期搜索结束
		currentDiary = diary;		
		needRepaint = true;
		notifyViews();
	}

	/**
	 * 删除当前日记
	 * @param diary
	 */
	@SuppressWarnings("unchecked")
	public void deleteCurrentDiary(Diary diary)
	{
		if(diary == null)
			return;
		editsMade = true;
		treeChanged = true;
		
		MyVector currentYear = null;
		MyVector currentMonth = null;
		
		//年搜索
		{		
			ListIterator itYear = theTree.listIterator();
			MyVector yVector = null;
			
			MyVector theYear = new MyVector(diary.getYear());
			
			while(itYear.hasNext())
			{
				yVector = (MyVector) itYear.next();
				if(theYear.toString().equals(yVector.toString()))
				{
					currentYear = yVector;
					break;
				}
			}
		}	//年搜索结束
		
		//月搜索开始
		{
			ListIterator itMonth = currentYear.listIterator();
			MyVector mVector = null;
			
			MyVector theMonth = new MyVector(diary.getMonth());
			
			while(itMonth.hasNext())
			{
				mVector = (MyVector) itMonth.next();
				if(theMonth.toString().equals(mVector.toString()))
				{
					currentMonth = mVector;
					break;
				}
			}
		}	//月搜索结束
		
		//删除日记
		{
			currentMonth.remove(diary);
			saveDiarysAs(myFile);
		}
		
		//检查是否有空的月份
		{
			if (currentMonth.size() == 0)
				currentYear.remove(currentMonth);
			if (currentYear.size() == 0)
				theTree.remove(currentYear);
		}
		
		currentDiary = diary;		
		needRepaint = true;
		notifyViews();
	}
	
	/**
	 * 建立XML数据库结构
	 * @param out
	 * @throws Exception 
	 */
	@SuppressWarnings("unchecked")
	private void xmlBuild(DataOutputStream out) throws Exception 
	{	
		if(out == null)
			return;
		
		//开始定义XML的元素
		Element root = new Element("我的日记");
		@SuppressWarnings("unused")
		Document doc = new Document(root);
		Vector yearNameList = new Vector();
		Vector monthNameList = new Vector();
		List yearList = null;
		List monthList = null;
		Iterator itYearList;
		Iterator itMonthList;
		String yearName = "";
		String monthName ="";
		String xmlString ;
		//结束对XML元素的定义
		
		Diary tempDiary;
		
		//树模型的定义开始
		MyVector yearVector = theTree;
		MyVector monthVector ;
		MyVector dateVector ;
		
		ListIterator itYear = yearVector.listIterator();
		ListIterator itMonth ;
		ListIterator itDate ;
		//树模型的定义结束
		
		while(itYear.hasNext())//年循环开始
		{
			monthVector = (MyVector) itYear.next();
			itMonth = monthVector.listIterator();
			while(itMonth.hasNext())//月循环开始
			{
				dateVector = (MyVector) itMonth.next();
				itDate = dateVector.listIterator();
				
				/*
				 * 这里是转化为Xml的主体部分
				 */
				while(itDate.hasNext())
				{
					tempDiary = (Diary) itDate.next();
					
					yearList = root.getChildren();
					itYearList = yearList.iterator();
					
					//测试第一次是否为空
					if(yearList.size() == 0)
					{
						Element eYear = new Element("year" + tempDiary.getYear());
						Element eMonth = new Element("month" + tempDiary.getMonth());
						Element eDate = new Element("date" + tempDiary.getDate());
						
						eDate.setText(tempDiary.getMainText());
						eDate.setAttribute("weather", tempDiary.getWeather());
						eDate.setAttribute("year", tempDiary.getYear());
						eDate.setAttribute("month", tempDiary.getMonth());
						eDate.setAttribute("date", tempDiary.getDate());
						
						eMonth.addContent(eDate);
						eYear.addContent(eMonth);
						root.addContent(eYear);
					}
					else   //当树结构不是空时
					{
						for(int i = 0; i < yearList.size();i++)//把树节点的所有年份取出来
						{
							Element tempYear = (Element)itYearList.next();
							yearName = tempYear.getName();
							if( ! yearNameList.contains(yearName))
								yearNameList.add(yearName);
						}
						
						if( yearNameList.contains("year" + tempDiary.getYear()))
						{
							monthList = root.getChild("year" + tempDiary.getYear()).getChildren();
							itMonthList = monthList.iterator();
							
							for(int i = 0; i<monthList.size();i++)
							{
								Element tempMonth = (Element) itMonthList.next();
								monthName = tempMonth.getName();
								if( !monthNameList.contains(monthName))
									monthNameList.add(monthName);
							}
							
							if(monthNameList.contains("month" + tempDiary.getMonth()))
							{
								Element eDate = new Element("date" + tempDiary.getDate());
								
								eDate.setText(tempDiary.getMainText());
								eDate.setAttribute("weather", tempDiary.getWeather());
								eDate.setAttribute("year", tempDiary.getYear());
								eDate.setAttribute("month", tempDiary.getMonth());
								eDate.setAttribute("date", tempDiary.getDate());
								
								root.getChild("year" + tempDiary.getYear()).getChild("month" + tempDiary.getMonth()).addContent(eDate);
							}
							else//如果月份不存在
							{
								Element eMonth = new Element("month" + tempDiary.getMonth());
								Element eDate = new Element("date" + tempDiary.getDate());
								
								eDate.setText(tempDiary.getMainText());
								eDate.setAttribute("weather", tempDiary.getWeather());
								eDate.setAttribute("year", tempDiary.getYear());
								eDate.setAttribute("month", tempDiary.getMonth());
								eDate.setAttribute("date", tempDiary.getDate());
								eMonth.addContent(eDate);
								
								root.getChild("year" + tempDiary.getYear()).addContent(eMonth);
							}
						}
						else//如果年份不存在
						{
							Element eYear = new Element("year" + tempDiary.getYear());
							Element eMonth = new Element("month" + tempDiary.getMonth());
							Element eDate = new Element("date" + tempDiary.getDate());
							
							//在此处对文本进行加密并写入xml结构
							String tempText = tempDiary.getMainText();//获得日记文本内容
							byte[] tempByte = tempText.getBytes();//将String类型转化成byte
							DataSecurity.encode(tempByte);//对转化后的byte进行加密
							
							String afterEncode = null;
							eDate.setText(afterEncode);
							//加密及写入结束
							eDate.setAttribute("weather", tempDiary.getWeather());
							eDate.setAttribute("year", tempDiary.getYear());
							eDate.setAttribute("month", tempDiary.getMonth());
							eDate.setAttribute("date", tempDiary.getDate());
							
							eMonth.addContent(eDate);
							eYear.addContent(eMonth);
							root.addContent(eYear);
						}
						
					}//树不为空的情况结束
				}//xml主体结束
			}//月循环结束
		}//年循环结束
		
		editsMade = false;
		
		xmlString = Xml.xml2String(root);
		byte[] afterEncode = DataSecurity.encode(xmlString.getBytes());//对XMLString进行加密
		String writeIn = new sun.misc.BASE64Encoder().encodeBuffer(afterEncode);//将byte用base64转化成String写入文件
		out.writeUTF(writeIn);//将String写进XML的实现
		out.flush();
		out.close();
	}

	public Diary getCurrentDiary()
	{
		return currentDiary;
	}
	
	public MyVector getDiaryTree()
	{
		return theTree;
	}
	public boolean getTreeChanged()
	{
		return treeChanged;
	}
	public boolean getEditsMade()
	{
		return editsMade;
	}
	public boolean getNeedRepaint()
	{
		return needRepaint;
	}
	public String getCurrentDate()
	{
		currentDate = currentDiary.getYear() + "-" + currentDiary.getMonth() + "-" + 
		              currentDiary.getDate() ;
		return currentDate;
	}
	public File getFile()
	{
		return myFile;
	}

	public void setCurrentDiary(Diary diary)
	{
		needRepaint = false;
		currentDiary.setMainText(JDiary.getTextView().getText());
		currentDiary = diary;
		notifyViews();
	}

	public boolean saveDiarys ()
	{
		return saveDiarysAs(myFile);
	}
}
