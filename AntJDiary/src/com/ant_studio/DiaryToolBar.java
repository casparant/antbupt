package com.ant_studio;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import com.swtdesigner.SwingResourceManager;

public class DiaryToolBar
{	
	/**
	 * 缺省串行版本标识
	 */
	private static final long serialVersionUID = 1L;
	
	private JButton newButton = new JButton();	
	private JButton deleteButton = new JButton();
	private JButton saveButton = new JButton();			
	private JButton cutButton = new JButton();	
	private JButton copyButton = new JButton();		
	private JButton pasteButton = new JButton();
	private JButton clearButton = new JButton();		
	private JButton findButton = new JButton();
	private JButton advancedSearchButton = new JButton();
	private JButton replaceButton = new JButton();
	private JButton appendDateTimeButton = new JButton();		
	private static JToggleButton readOnlyToggleButton = new JToggleButton();
	private static JToggleButton autoLineToggleButton = new JToggleButton();
	private static JToggleButton bgMusicToggleButton = new JToggleButton();
	private JButton optionButton = new JButton();		
	private JButton exitButton = new JButton();
	
	/**
	 * 构造函数--构造工具栏按钮
	 */
	public DiaryToolBar(final JToolBar toolBar) 
	{
		//新建文件
		toolBar.add(newButton);
		newButton.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/diary2.gif"));
		newButton.setPressedIcon(SwingResourceManager.getIcon(JDiary.class, "/images/diary3.gif"));
		newButton.setMargin(new Insets(0, 0, 0, 0));	
		newButton.setToolTipText("新建一则日记");		
		
		newButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				new ChooseDateDialog().setVisible(true);
				Operation.createNewDiary(ChooseDateDialog.getFlag(), DiaryHeadPanel.getChooseDateLabel());
			}
		});
		
		
		//删除按钮
		toolBar.add(deleteButton);
		deleteButton.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/delete2.gif"));
		deleteButton.setPressedIcon(SwingResourceManager.getIcon(JDiary.class, "/images/delete3.gif"));
		deleteButton.setMargin(new Insets(0, 0, 0, 0));
		deleteButton.setToolTipText("删除");
		
		deleteButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event)
			{
				Operation.deleteDiary();
			}
		});
	
		
		//保存按钮
		toolBar.add(saveButton);	
		saveButton.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/save2.gif"));
		saveButton.setPressedIcon(SwingResourceManager.getIcon(JDiary.class, "/images/save3.gif"));
		saveButton.setMargin(new Insets(0,0,0,0));
		saveButton.setToolTipText("保存");				
		
		saveButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				JDiary.getModel().getCurrentDiary().setMainText( JDiary.getTextView().getText() );
				Operation.save();
				JOptionPane.showMessageDialog(saveButton, "日记已经保存！", "信息", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		
		toolBar.addSeparator();

		
		//剪切按钮
		toolBar.add(cutButton);	
		cutButton.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/cut2.gif"));
		cutButton.setPressedIcon(SwingResourceManager.getIcon(JDiary.class, "/images/cut3.gif"));
		cutButton.setMargin(new Insets(0, 0, 0, 0));
		cutButton.setToolTipText("剪切");			
		
		cutButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				DiaryTextAreaView.getTextArea().cut();
			}
		});
			

		//复制按钮开始
		toolBar.add(copyButton);
		copyButton.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/copy2.gif"));
		copyButton.setPressedIcon(SwingResourceManager.getIcon(JDiary.class, "/images/copy3.gif"));
		copyButton.setMargin(new Insets(0, 0, 0, 0));
		copyButton.setToolTipText("复制");				
		
		copyButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				DiaryTextAreaView.getTextArea().copy();
			}
		});

		
		//粘贴按钮
		toolBar.add(pasteButton);
		pasteButton.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/paste2.gif"));
		pasteButton.setPressedIcon(SwingResourceManager.getIcon(JDiary.class, "/images/paste3.gif"));
		pasteButton.setMargin(new Insets(0, 0, 0, 0));
		pasteButton.setToolTipText("粘贴");				
		
		pasteButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				DiaryTextAreaView.getTextArea().paste();
			}
		});


		//清除按钮
		toolBar.add(clearButton);
		clearButton.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/clear2.gif"));
		clearButton.setPressedIcon(SwingResourceManager.getIcon(JDiary.class, "/images/clear3.gif"));
		clearButton.setMargin(new Insets(0, 0, 0, 0));
		clearButton.setToolTipText("清除所选内容");
		
		clearButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				DiaryTextAreaView.getTextArea().replaceSelection("");
			}
		});
		
		
		toolBar.addSeparator();


		//查找按钮
		toolBar.add(findButton);
		findButton.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/find2.gif"));
		findButton.setPressedIcon(SwingResourceManager.getIcon(JDiary.class, "/images/find3.gif"));
		findButton.setMargin(new Insets(0, 0, 0, 0));
		findButton.setToolTipText("当前日记中查找");
		
		findButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				new FindDialog().setVisible(true);
			}
		});	
		
		
		//查找所有按钮
		toolBar.add(advancedSearchButton);
		advancedSearchButton.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/advfind2.gif"));
		advancedSearchButton.setPressedIcon(SwingResourceManager.getIcon(JDiary.class, "/images/advfind3.gif"));
		advancedSearchButton.setMargin(new Insets(0, 0, 0, 0));
		advancedSearchButton.setToolTipText("在所有文件中查找");
		
		advancedSearchButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				new AdvancedSearchDialog().setVisible(true);
			}
		});	

		
		//替换按钮
		toolBar.add(replaceButton);
		replaceButton.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/replace2.gif"));
		replaceButton.setPressedIcon(SwingResourceManager.getIcon(JDiary.class, "/images/replace3.gif"));
		replaceButton.setMargin(new Insets(0, 0, 0, 0));
		replaceButton.setToolTipText("替换文字");		
		
		replaceButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				new ReplaceDialog().setVisible(true);
			}
		});
			
		
		//插入时间按钮
		toolBar.add(appendDateTimeButton);		
		appendDateTimeButton.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/insertdate2.gif"));
		appendDateTimeButton.setPressedIcon(SwingResourceManager.getIcon(JDiary.class, "/images/insertdate3.gif"));
		appendDateTimeButton.setMargin(new Insets(0, 0, 0, 0));
		appendDateTimeButton.setToolTipText("插入时间");		
		
		appendDateTimeButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				DiaryTextAreaView.getTextArea().append("\n" + Format.currentTime());
			}
		});	
		
		
		toolBar.addSeparator();


		//自动换行按钮
		toolBar.add(autoLineToggleButton);
		autoLineToggleButton.setSelected(true);
		autoLineToggleButton.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/wordwrap2.gif"));
		autoLineToggleButton.setSelectedIcon(SwingResourceManager.getIcon(JDiary.class, "/images/wordwrap3.gif"));
		autoLineToggleButton.setMargin(new Insets(0,0,0,0));
		autoLineToggleButton.setToolTipText("自动换行");
		
		autoLineToggleButton.addItemListener(new ItemListener()
		{
			public void itemStateChanged(ItemEvent e)
			{
				Operation.autoLine(DiaryMenuBar.getAutoLineCheckBoxMenuItem(), DiaryTextAreaView.getTextArea(), autoLineToggleButton);
			}
		});
		
		
		//只读模式按钮
		toolBar.add(readOnlyToggleButton);
		readOnlyToggleButton.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/readonly2.gif"));
		readOnlyToggleButton.setSelectedIcon(SwingResourceManager.getIcon(JDiary.class, "/images/readonly3.gif"));
		readOnlyToggleButton.setMargin(new Insets(0, 0, 0, 0));
		readOnlyToggleButton.setToolTipText("只读模式");				
		
		readOnlyToggleButton.addItemListener(new ItemListener()
		{
			public void itemStateChanged(ItemEvent e)
			{
				Operation.readOnly(DiaryMenuBar.getReadOnlyCheckBoxMenuItem(), DiaryTextAreaView.getTextArea(), readOnlyToggleButton);
			}
		});
		
		
		//背景音乐按钮
		toolBar.add(bgMusicToggleButton);
		bgMusicToggleButton.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/midi2.gif"));
		bgMusicToggleButton.setSelectedIcon(SwingResourceManager.getIcon(JDiary.class, "/images/midi3.gif"));
		bgMusicToggleButton.setMargin(new Insets(0, 0, 0, 0));
		bgMusicToggleButton.setToolTipText("背景音乐");				
		
		bgMusicToggleButton.addItemListener(new ItemListener()
		{
			public void itemStateChanged(ItemEvent e)
			{
				Operation.bgMusic(DiaryMenuBar.getBgMusicCheckBoxMenuItem(), bgMusicToggleButton);
			}
		});
		

		//选项按钮开始
		toolBar.add(optionButton);		
		optionButton.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/option2.gif"));
		optionButton.setSelectedIcon(SwingResourceManager.getIcon(JDiary.class, "/images/option3.gif"));
		optionButton.setMargin(new Insets(0, 0, 0, 0));
		optionButton.setToolTipText("选项");
		
		optionButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				new OptionDialog().setVisible(true);
			}
		});
		
		
		toolBar.addSeparator();
			
		
		//退出按钮开始
		toolBar.add(exitButton);
		exitButton.setIcon(SwingResourceManager.getIcon(JDiary.class, "/images/exit2.gif"));
		exitButton.setPressedIcon(SwingResourceManager.getIcon(JDiary.class, "/images/exit3.gif"));
		exitButton.setMargin(new Insets(0, 0, 0, 0));
		exitButton.setToolTipText("退出");		
		
		exitButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				System.exit(0);
			}
		});
	}	//工具栏按钮构造方法结束
	
	/**
	 * 以下三个静态方法返回三个切换按钮组件：
	 * 自动换行 切换按钮
	 * 只读模式 切换按钮
	 * 背景音乐 切换按钮
	 */
	public static JToggleButton getAutoLineToggleButton()
	{
		return autoLineToggleButton;
	}

	public static JToggleButton getBgMusicToggleButton()
	{
		return bgMusicToggleButton;
	}
	
	public static JToggleButton getReadOnlyToggleButton()
	{
		return readOnlyToggleButton;
	}

}	//DiaryToolBar类结束
