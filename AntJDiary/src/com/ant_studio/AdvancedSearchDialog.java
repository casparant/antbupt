package com.ant_studio;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ListIterator;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JTree;

public class AdvancedSearchDialog extends JDialog 
{
	
	/**
	 * 高级查找对话框
	 * @author 黄文灏
	 */
	private static final long serialVersionUID = 1L;
	
	private MyVector textVector = new MyVector("我的日记");
	private JLabel searchLabel = new JLabel();
	private JTextField searchTextField;
	private JButton searchButton = new JButton();
	private String findString = "";
	private JTree textTree;
        
	/**
	 * Create the dialog
	 */
	public AdvancedSearchDialog() {
		super();
		setResizable(false);
		getContentPane().setLayout(null);
		setTitle("高级查找");
		setBounds(100, 100, 345, 457);

		searchLabel.setText("查找内容：");
		searchLabel.setBounds(10, 10, 75, 33);
		getContentPane().add(searchLabel);

		searchTextField = new JTextField();
		searchTextField.setBounds(67, 12, 167, 31);
		getContentPane().add(searchTextField);

		searchButton.setMnemonic(KeyEvent.VK_T);
		searchButton.setText("查找(F)");
		searchButton.setBounds(238, 10, 89, 33);
		getContentPane().add(searchButton);
		
		final JTabbedPane tabbedPane = new JTabbedPane();
		tabbedPane.setBounds(10, 49, 317, 366);
		
		
		textTree = new JTree( textVector );
		tabbedPane.removeAll();
		tabbedPane.addTab("目录", null, textTree , "目录");
		
		getContentPane().add(tabbedPane);
		
		searchButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event) 
			{
				findString = searchTextField.getText();
				textVector = AdvancedSearchDialog.getVector(findString);
				textTree = new JTree( textVector );
				tabbedPane.removeAll();
				tabbedPane.addTab("目录", null, textTree , "目录");
			}
			
		});

		

		

	}

	@SuppressWarnings("unchecked")
	protected static MyVector getVector(String str) {
		MyVector findVector = new MyVector("我的日记");
		MyVector diaryVector = JDiary.getModel().getDiaryTree();
		
		Diary tempDiary;
		String tempText;
		
		MyVector yearVector = diaryVector;
		MyVector monthVector ;
		MyVector dateVector ;
		
		ListIterator yearIterator = yearVector.listIterator();
		ListIterator monthIterator;
		ListIterator dateIterator;
		
		while(yearIterator.hasNext()) {    //年循环开始
			
			monthVector = (MyVector)yearIterator.next();
			monthIterator = monthVector.listIterator();
			
			while(monthIterator.hasNext()) {    //月循环开始
				
				dateVector = (MyVector) monthIterator.next();
				dateIterator = dateVector.listIterator();
				
				while(dateIterator.hasNext()) {     //日循环开始
					tempDiary = (Diary) dateIterator.next();
					tempText = tempDiary.getMainText();
					if( tempText.indexOf(str) != -1 )
						findVector.addElement( new DiaryLabel( tempDiary.getWholeDate() ) );
						
				}//日循环结束
			}//月循环结束
		}//年循环结束
		
		
		return findVector;
	}

}
