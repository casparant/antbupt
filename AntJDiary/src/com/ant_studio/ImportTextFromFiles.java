package com.ant_studio;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Observable;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

public class ImportTextFromFiles extends JDialog{
	
	private JButton pasteButton;
	private JButton selectButton;
	private static final long serialVersionUID = 1L;
	
	private JTextArea destinyArea;
	private JScrollPane destinyScrollPane;
	private JTextArea sourceArea;
	private JScrollPane sourceScrollPane;
	@SuppressWarnings("unused")
	private DiaryModel myDiaryModel;
	final JPanel panel = new JPanel();
	final JTabbedPane destinytabbedPane = new JTabbedPane();
	final JTabbedPane sourcetabbedPane = new JTabbedPane();
	final JSplitPane splitPane = new JSplitPane();
	final JButton fileChooseButton = new JButton();
	final JButton confirmButton = new JButton();
	final JButton cancleButton = new JButton();
	JFileChooser fileChooser;
    Container container = new Container();
    File file, selectedFile;
    
    
    protected JScrollPane getsourceScrollPane() 
	{
		if (sourceScrollPane == null) 
		{
			sourceScrollPane = new JScrollPane();
			sourceScrollPane.setViewportView(getSourceArea());
		}
		return sourceScrollPane;
	}
    
    
	protected JTextArea getSourceArea() 
	{
		if (sourceArea == null) 
		{
			sourceArea = new JTextArea();
			sourceArea.setLineWrap(true);
			sourceArea.setAutoscrolls(false);
		}
		return sourceArea;
	}
	
	
	protected JScrollPane getdestinyScrollPane() 
	{
		if (destinyScrollPane == null)
		{
			destinyScrollPane = new JScrollPane();
			destinyScrollPane.setViewportView(getDestinyArea());
		}
		return destinyScrollPane;
	}
	
	
	protected JTextArea getDestinyArea() 
	{
		if (destinyArea == null) 
		{
			destinyArea = new JTextArea();
			destinyArea.setLineWrap(true);
			destinyArea.setWrapStyleWord(true);
		}
		return destinyArea;
	}

	
	public ImportTextFromFiles() {
		super();
		initialize();
	}

	/**
	 * 初始化主界面
	 */
	private void initialize() {
		container = getContentPane();
		
		//主frame
		setTitle("从文件导入文本");
		setName("frame3");
		getContentPane().setLayout(new FlowLayout());
		setBounds(100, 100, 500, 445);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		//读取文件按钮
		fileChooseButton.addActionListener(new ButtonListener());
		fileChooseButton.setText("读取文件");
		getContentPane().add(fileChooseButton);

     	//可滑动分割面板
		splitPane.setPreferredSize(new Dimension(475, 275));
		getContentPane().add(splitPane);
		
		//文件中的文本tabbedPane
		sourcetabbedPane.setPreferredSize(new Dimension(200, 0));
		splitPane.setLeftComponent(sourcetabbedPane);
		sourcetabbedPane.addTab("文件中的文本", null, getsourceScrollPane(), null);

		//将要读取的文本tabbedPane
		splitPane.setRightComponent(destinytabbedPane);
		destinytabbedPane.addTab("将要读取的文本", null, getdestinyScrollPane(), null);

		panel.setLayout(new FormLayout(
			new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				new ColumnSpec("center:70px"),
				FormFactory.RELATED_GAP_COLSPEC,
				new ColumnSpec("60px"),
				FormFactory.RELATED_GAP_COLSPEC,
				new ColumnSpec("50px"),
				FormFactory.RELATED_GAP_COLSPEC,
				new ColumnSpec("60px"),
				FormFactory.RELATED_GAP_COLSPEC,
				new ColumnSpec("center:70px"),
				FormFactory.RELATED_GAP_COLSPEC},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				new RowSpec("23px"),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC}));
		getContentPane().add(panel);

		//底部的label
		panel.add(getSelectButton(), new CellConstraints("2, 2, 1, 1, center, fill"));

		//确定按钮
		confirmButton.setText("确定");
		confirmButton.addActionListener(new ButtonListener());
		panel.add(getPasteButton(), new CellConstraints("10, 2, 1, 1, fill, fill"));
		panel.add(confirmButton, new CellConstraints("4, 4, 1, 1, fill, fill"));
		
		//取消按钮
		cancleButton.addActionListener(new ButtonListener());
		cancleButton.setText("取消");
		panel.add(cancleButton, new CellConstraints("8, 4, 1, 1, fill, fill"));
		cancleButton.addActionListener( new ActionListener()
		{

			public void actionPerformed(ActionEvent event) {
				dispose();
			}
			
		});
	}

	/**
	 * 该类实现界面上所有监听
	 */
	class ButtonListener extends Observable implements ActionListener
	{

		public void actionPerformed(ActionEvent e) {
			JButton button = (JButton)e.getSource();
			
			if(button == fileChooseButton)
			{	
				fileChooser = new JFileChooser();
			
				fileChooser.addChoosableFileFilter(new MyFileFilter());
				int selected = fileChooser.showOpenDialog(container);
				/*如果选择了确定*/
				if(selected == JFileChooser.APPROVE_OPTION)
				{
					file = fileChooser.getSelectedFile();
					
					if(file.isFile())
					{
						/*从文件读入SourceArea的实现*/
						try
						{
							BufferedReader input = new BufferedReader(
									new FileReader(file));
							StringBuffer buffer = new StringBuffer();
							String text;
							
							while((text = input.readLine()) != null)
							{
								buffer.append(text + "\n");
							}
							
							sourceArea.append(buffer.toString());
						}
						catch(IOException ioException)
						{
							JOptionPane.showMessageDialog(
									container, "FILE ERROR", "FILE ERROR", JOptionPane.ERROR_MESSAGE);
						}
					}//从文件读入SourceArea的实现结束
				}//文件选择对话框中打开按钮的监听结束
				/*如果选择了撤销*/
				else if(selected == JFileChooser.CANCEL_OPTION)
				{
					fileChooser.cancelSelection();
				}//对撤销按钮的监听结束
			}
			/*如果选择了导入文件界面中的确定按钮*/
			else if(button == confirmButton)
			{
				String newText = destinyArea.getText();
				myDiaryModel = JDiary.getModel();
			    JTextArea textArea = DiaryTextAreaView.getTextArea();
			    int currentPosition = textArea.getCaretPosition();
			    textArea.insert(newText, currentPosition);
				dispose();
			}
			else if(button == cancleButton)
			{
				dispose();
			}
		}
		
	}
	
	/**
	 * 该类制定了文件过滤器
	 */
	class MyFileFilter extends javax.swing.filechooser.FileFilter
	{

		@Override
		public boolean accept(File file) {
			if(file.isDirectory())
			{
				return true;
			}
			
			String fileName = file.getName();
			int periodIndex = fileName.lastIndexOf('.');
			
			boolean accepted = false;
			
			if(periodIndex > 0 && periodIndex < fileName.length() - 1)
			{
				String extension = fileName.substring(periodIndex + 1).toLowerCase();
				if(extension.equals("java"))
				{	
					accepted = true;
				}
				else if(extension.equals("txt"))
				{
					accepted = true;
				}
			}
			
			return accepted;
		}

		public String getTypeDescription(File file) 
		{
			String extension = getExtension(file);
			String type = null;
			if(extension != null)
			{
				if(extension.equals("java"))
				{
					type = "Java File";
				}
			}
			return type;
		}
		
		private String getExtension(File file)
		{
			String fileName = file.getName();
			int periodIndex = fileName.lastIndexOf('.');
			String extension = null;
			
			if(periodIndex > 0 && periodIndex < fileName.length() - 1)
			{
				extension = fileName.substring(periodIndex + 1).toLowerCase();
			}
			
			return extension;
		}

		@Override
		public String getDescription() 
		{
			return "Java and txt Files(*.java , *.txt)";
		}
	}
	protected JButton getSelectButton() {
		if (selectButton == null) {
			selectButton = new JButton();
			selectButton.addActionListener(new ActionListener() 
			{
				public void actionPerformed(ActionEvent arg0) 
				{
					sourceArea.copy();
				}
			});
			selectButton.setText("复制");
		}
		return selectButton;
	}
	protected JButton getPasteButton() {
		if (pasteButton == null) {
			pasteButton = new JButton();
			pasteButton.addActionListener(new ActionListener() 
			{
				public void actionPerformed(ActionEvent arg0) 
				{
					destinyArea.paste();
				}
			});
			pasteButton.setText("粘贴");
		}
		return pasteButton;
	}
}

