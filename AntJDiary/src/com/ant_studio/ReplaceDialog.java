package com.ant_studio;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.JButton;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class ReplaceDialog extends JDialog 
{

	/**
	 * 替换对话框
	 * author:黄文灏
	 * 
	 */	
	private static final long serialVersionUID = 1L;

	private final JLabel findLabel = new JLabel();
	private final JLabel replaceLabel = new JLabel();
	private final JTextField findTextField = new JTextField();
	private final JTextField replaceTextField = new JTextField();
	private final JButton replaceButton = new JButton();
	private final JButton replaceAllButton = new JButton();
	private final JButton cancelButton = new JButton();	
	
	/**
	 * Create the dialog
	 */
	public ReplaceDialog() 
	{
		super();
		setResizable(false);
		getContentPane().setLayout(null);
		setTitle("替换...");
		setBounds(100, 100, 379, 187);

		findLabel.setText("查找内容：");
		findLabel.setBounds(15, 15, 67, 31);
		getContentPane().add(findLabel);

		replaceLabel.setText("替换为：");
		replaceLabel.setBounds(15, 60, 60, 31);
		getContentPane().add(replaceLabel);

		findTextField.setBounds(85, 15, 122, 30);
		getContentPane().add(findTextField);

		replaceTextField.setBounds(85, 60, 122, 30);
		getContentPane().add(replaceTextField);

	    replaceButton.setMnemonic(KeyEvent.VK_R);
		replaceButton.setText("替换(R)");
		replaceButton.setBounds(249, 19, 99, 35);
		getContentPane().add(replaceButton);
		
		 replaceButton.addActionListener(new ActionListener() 
		    {
		    	public void actionPerformed(ActionEvent arg0) 
		    	{
		    		String findWords = findTextField.getText();
		    		String text = DiaryTextAreaView.getTextArea().getText();
		    		String replaceWords = replaceTextField.getText();
		    		
		    		int start = text.indexOf(findWords);
		    		if(start==-1)
					{
						JOptionPane.showMessageDialog(null, "搜索的字符串没有找到", "提示", JOptionPane.WARNING_MESSAGE);
					}
		    		else
		    		{
		    			DiaryTextAreaView.getTextArea().requestFocusInWindow();
		    			DiaryTextAreaView.getTextArea().select(start, start + findWords.length());
		    			new ReplaceAndFindDialog(findWords,replaceWords).setVisible(true);
		    			dispose();
		    		}
		    	}
		    });

		replaceAllButton.setMnemonic(KeyEvent.VK_A);
		replaceAllButton.setText("全部替换(A)");
		replaceAllButton.setBounds(249, 58, 99, 35);
		getContentPane().add(replaceAllButton);
		
		replaceAllButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String text = DiaryTextAreaView.getTextArea().getText();
				text = text.replaceAll(findTextField.getText(),replaceTextField.getText());
				DiaryTextAreaView.getTextArea().setText(text);
				dispose();
			}
		});

		cancelButton.setMnemonic(KeyEvent.VK_C);
		cancelButton.setText("取消(C)");
		cancelButton.setBounds(249, 99, 99, 35);
		getContentPane().add(cancelButton);
		
		cancelButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				dispose();
			}
		});
	}

}
