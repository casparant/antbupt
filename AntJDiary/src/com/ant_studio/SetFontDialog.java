package com.ant_studio;


import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import javax.swing.border.TitledBorder;

public class SetFontDialog extends JDialog {

    /**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;
	private JTextArea textArea;
	private JComboBox setSizeComboBox;
	private JComboBox SetFontComboBox;
	/**
	 * Create the dialog
	 */
	public SetFontDialog() {
		super();
		setTitle("字体设置");
		getContentPane().setLayout(null);
		setBounds(100, 100, 549, 432);

		SetFontComboBox = new JComboBox();
		SetFontComboBox.setBounds(10, 67, 191, 29);
		getContentPane().add(SetFontComboBox);

		setSizeComboBox = new JComboBox();
		setSizeComboBox.setBounds(10, 139, 191, 29);
		getContentPane().add(setSizeComboBox);

		final JCheckBox CheckBox1 = new JCheckBox();
		CheckBox1.setText("粗体");
		CheckBox1.setBounds(30, 217, 104, 29);
		getContentPane().add(CheckBox1);

		final JCheckBox checkBox2 = new JCheckBox();
		checkBox2.setText("斜体");
		checkBox2.setBounds(30, 252, 104, 29);
		getContentPane().add(checkBox2);

		final JLabel label = new JLabel();
		label.setText("字体设置：");
		label.setBounds(10, 30, 137, 29);
		getContentPane().add(label);

		final JLabel label_1 = new JLabel();
		label_1.setText("字体大小：");
		label_1.setBounds(10, 102, 137, 31);
		getContentPane().add(label_1);

		final JButton confirmButton = new JButton();
		confirmButton.setText("确认(O)");
		confirmButton.setBounds(10, 330, 104, 41);
		getContentPane().add(confirmButton);

		final JButton cancelButton = new JButton();
		cancelButton.setText("取消(C)");
		cancelButton.setBounds(120, 330, 104, 41);
		getContentPane().add(cancelButton);

		final JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(new TitledBorder(null, "预览", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, null));
		panel.setBounds(230, 34, 302, 335);
		getContentPane().add(panel);

		textArea = new JTextArea();
		textArea.setBounds(13, 28, 279, 284);
		panel.add(textArea);
		//
	}

}
