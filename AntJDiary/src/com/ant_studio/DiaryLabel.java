package com.ant_studio;

public class DiaryLabel {

	private String label;
	
	public DiaryLabel(String label)
	{
		this.label = label;
	}
	
	public String toString()
	{
		return label;
	}
	
}
