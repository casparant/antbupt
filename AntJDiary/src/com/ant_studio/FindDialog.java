package com.ant_studio;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

public class FindDialog extends JDialog 
{

	/**
	 * 查找对话框
	 * author：黄文灏
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ButtonGroup upOrDownButtonGroup = new ButtonGroup();
    private 	JRadioButton upRadioButton = new JRadioButton();
	private 	JRadioButton downRadioButton = new JRadioButton();
    private JButton confirmButton = new JButton();
	private JButton cancelButton = new JButton();
	private JLabel findLabel = new JLabel();
	private JTextField findTextField;
	private JPanel upOrDownPanel = new JPanel();
	
	/**
	 * Create the dialog
	 */
	public FindDialog() 
	{
		super();
		setResizable(false);
		getContentPane().setLayout(null);
		setTitle("查找...");
		setBounds(100, 100, 355, 167);

		confirmButton.setMnemonic(KeyEvent.VK_F);
		confirmButton.setText("查找(F)");
		confirmButton.setBounds(232, 28, 84, 32);
		getContentPane().add(confirmButton);

		confirmButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
			    String text1 = findTextField.getText();
			    String text = DiaryTextAreaView.getTextArea().getText();
			    
			    //判断是否存在
			    int start = text.indexOf(text1);
				
			    if(start==-1)
				{
					JOptionPane.showMessageDialog(null, "搜索的字符串没有找到", "提示", JOptionPane.WARNING_MESSAGE);
				}
			    
			    //决定向上还是向下查找
			    //向下查找
			    if(downRadioButton.isSelected())
			    {
			        //便于实现循环查找
			    	int caretPosition = DiaryTextAreaView.getTextArea().getCaretPosition();
			    	String text2 = text.substring(caretPosition,text.length()) + text.substring(0,caretPosition);
			        start = text2.indexOf(text1);
                    
			    	//调整textarea中下标的起始位置
			    	if(start>=(text.length()-caretPosition))start = start - (text.length()-caretPosition);
			    	else start = start + caretPosition;
			    	
					int end = start + text1.length();
					
					if(end>text.length())
					{
						start = text.indexOf(text1);
						end = start + text1.length();
					}
					
					DiaryTextAreaView.getTextArea().requestFocusInWindow();
					DiaryTextAreaView.getTextArea().select(start, end);
					
				 }
			    
			    //向上查找
			    else
			    {
		    	    int offset = DiaryTextAreaView.getTextArea().getSelectionEnd() - DiaryTextAreaView.getTextArea().getSelectionStart();
			    	int caretPosition = DiaryTextAreaView.getTextArea().getCaretPosition() - offset;//使循环查找不会停留在一个字符
			    	String text2 = text.substring(caretPosition,text.length()) + text.substring(0,caretPosition);
			    	start = text2.lastIndexOf(text1);
			    	
			    	if(start>=(text.length()-caretPosition))start = start - (text.length()-caretPosition);
			    	else start = start + caretPosition;
			    	
			    	int end = start + text1.length();
			    	
			    	if(end>text.length())
					{
						start = text.indexOf(text1);
						end = start + text1.length();
					}
					
			    	DiaryTextAreaView.getTextArea().requestFocusInWindow();
					DiaryTextAreaView.getTextArea().select(start, end);
					
				 }
			  }
		});
		
		cancelButton.setMnemonic(KeyEvent.VK_C);
		cancelButton.setMargin(new Insets(2, 14, 2, 14));
		cancelButton.setText("取消(C)");
		cancelButton.setBounds(232, 74, 84, 32);
		getContentPane().add(cancelButton);
		
		cancelButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				dispose();
			}
		});

		findLabel.setFont(new Font("", Font.PLAIN, 14));
		findLabel.setText("查找内容：");
		findLabel.setBounds(10, 21, 72, 32);
		getContentPane().add(findLabel);

		findTextField = new JTextField();
		findTextField.setBounds(73, 22, 124, 33);
		getContentPane().add(findTextField);

		upOrDownButtonGroup.add(upRadioButton);
		upRadioButton.setText("向上");
		upRadioButton.setBounds(60, 100, 54, 23);
		getContentPane().add(upRadioButton);

		upOrDownButtonGroup.add(downRadioButton);
		downRadioButton.setText("向下");
		downRadioButton.setBounds(132, 100, 54, 23);
		downRadioButton.setSelected(true);
		getContentPane().add(downRadioButton);

		upOrDownPanel.setBorder(new TitledBorder(null, "方向选择", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, null));
		upOrDownPanel.setBounds(42, 80, 166, 53);
		getContentPane().add(upOrDownPanel);
	}

}
