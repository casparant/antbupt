package com.ant_studio;

import java.io.*;

public class Diary implements Cloneable
{
	private String year;
	private String month;
	private String date;
	private String mainText;
	private String weather;
	
	public Diary(String year, String month, String date, String weather)
	{
		this.year = year;
		this.month = month;
		this.date = date;
		mainText = "";
		this.weather = weather;
	}
	
	public boolean readDiary(DataInputStream in) throws IOException
	{
		try
		{
			year = new String(in.readUTF());
			month = new String(in.readUTF());
			date = new String(in.readUTF());
			mainText = new String(in.readUTF());
			weather = new String(in.readUTF());
			
			return true;
		}
		catch(EOFException e)
		{
			in.close();
			return false;
		}
	}

	public void writeMovie(DataOutputStream out) throws IOException
	{
		out.writeUTF(year);
		out.writeUTF(month);
		out.writeUTF(date);
		out.writeUTF(mainText);
		out.writeUTF(weather);
	}
	
	public String getYear()
	{
		return year;
	}
	
	public String getMonth()
	{
		return month;
	}
	
	public String getDate()
	{
		return date;
	}
	
	public String getWholeDate()
	{
		return year + month + date;
	}
	
	public String getMainText()
	{
		return mainText;
	}
	
	public String getWeather()
	{
		return weather;
	}
	
	public void setYear(String year)
	{
		this.year = year;
	}
	
	public void setMonth(String month)
	{
		this.month = month;
	}
	
	public void setDate(String date)
	{
		this.date = date;
	}
	
	public void setMainText(String text)
	{
		mainText = new String(text);
	}
	
	public void setWeather(String weather)
	{
		this.weather = weather;
	}	
	
	public Object clone()
	{
		Diary d = null;
		try
		{
			d = (Diary)super.clone();
			d.year = new String(year);
			d.month = new String(month);
			d.date = new String(date);
			d.mainText = new String(mainText);
			d.weather = new String(weather);
		}
		catch(CloneNotSupportedException e)
		{
			e.printStackTrace();
		}
		return d;
	}

	public String toString()
	{
		return date;
	}
	

}
