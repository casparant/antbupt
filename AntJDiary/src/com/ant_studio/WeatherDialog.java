package com.ant_studio;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.*;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.swtdesigner.SwingResourceManager;

public class WeatherDialog extends JDialog 
{
	
	/**
	 * “天气”对话框
	 * @author 张金利
	 */
	private static final long serialVersionUID = 1L;

	private final JButton sunnyButton = new JButton();
	private final JButton cloudyButton = new JButton();
	private final JButton rainyButton = new JButton();
	private final JButton snowyButton = new JButton();
	
	/**
	 * Create the dialog
	 */
	public WeatherDialog(final JButton parentButton) 
	{
		super();
		setLocation(parentButton.getLocationOnScreen().x, parentButton.getLocationOnScreen().y + parentButton.getSize().height);
		setUndecorated(true);
		setResizable(false);
		setName("weatherDialog");
		
		getContentPane().setLayout(new FormLayout(
			new ColumnSpec[] 
			{
				new ColumnSpec("40px")
			},
			new RowSpec[]
			{
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC
			}));

		//晴
		sunnyButton.setBorder(new EmptyBorder(0, 0, 0, 0));
		sunnyButton.setMargin(new Insets(2, 5, 2, 5));
		sunnyButton.setVerticalTextPosition(SwingConstants.BOTTOM);
		sunnyButton.setIcon(SwingResourceManager.getIcon(WeatherDialog.class, "/images/sunny.gif"));
		getContentPane().add(sunnyButton, new CellConstraints());
		
		sunnyButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				parentButton.setText("晴");
				parentButton.setFont(new Font("宋体", Font.BOLD, 14));
				dispose();
			}
		});

		//阴
		cloudyButton.setBorder(new EmptyBorder(0, 0, 0, 0));
		cloudyButton.setMargin(new Insets(2, 5, 2, 5));
		cloudyButton.setIcon(SwingResourceManager.getIcon(WeatherDialog.class, "/images/cloudy.gif"));
		getContentPane().add(cloudyButton, new CellConstraints(1, 2));
		
		cloudyButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				parentButton.setText("阴");
				parentButton.setFont(new Font("宋体", Font.BOLD, 14));
				dispose();
			}
		});

		//雨
		rainyButton.setBorder(new EmptyBorder(0, 0, 0, 0));
		rainyButton.setMargin(new Insets(2, 5, 2, 5));
		rainyButton.setIcon(SwingResourceManager.getIcon(WeatherDialog.class, "/images/rainy.gif"));
		getContentPane().add(rainyButton, new CellConstraints(1, 3));
		
		rainyButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				parentButton.setText("雨");
				parentButton.setFont(new Font("宋体", Font.BOLD, 14));
				dispose();
			}
		});

		//雪
		snowyButton.setBorder(new EmptyBorder(0, 0, 0, 0));
		snowyButton.setMargin(new Insets(2, 5, 2, 5));
		snowyButton.setIcon(SwingResourceManager.getIcon(WeatherDialog.class, "/images/snowy.gif"));
		getContentPane().add(snowyButton, new CellConstraints(1, 4));
		
		snowyButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				parentButton.setText("雪");
				parentButton.setFont(new Font("宋体", Font.BOLD, 14));
				dispose();
			}
		});
		
		dispose();
		pack();
	}
}