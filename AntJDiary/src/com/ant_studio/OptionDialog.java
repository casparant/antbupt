package com.ant_studio;

import java.io.*;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import com.swtdesigner.SwingResourceManager;

public class OptionDialog extends JDialog {

	/**
	 * 选项对话框
	 *author:黄文灏
	 * 
	 */
	private final static long serialVersionUID = 1L;

	private JTabbedPane tabbedPane = new JTabbedPane();
		private JPanel dataSafetyPanel = new JPanel();
			private JPanel modifyPasswordPanel = new JPanel();
				private JLabel oldPasswordLabel = new JLabel();
				private JLabel newPasswordLabel = new JLabel();
				private JLabel confirmPasswordLabel = new JLabel();
				private JPasswordField oldPasswordField = new JPasswordField();
				private JPasswordField newPasswordField = new JPasswordField();
				private JPasswordField confirmPasswordField;
				private JButton modifyPasswordButton = new JButton();
			private JPanel autoSavePanel = new JPanel();
				private JTextField autoSaveTextField = new JTextField();
				private JCheckBox autoSaveCheckBox = new JCheckBox();
				private JLabel autoSaveLabel = new JLabel();
			private JPanel autoBackupPanel = new JPanel();
				private JTextField autoBackupTextField = new JTextField();
				private JCheckBox autoBackupCheckBox = new JCheckBox();
				private JLabel autoBackupLabel = new JLabel();
			private JLabel tipLabel = new JLabel();
		private JPanel displayStylePanel = new JPanel();
			private JPanel fontPanel = new JPanel();
				private JButton systemFontButton = new JButton();
				private JButton editorFontButton = new JButton();
			private JPanel environmentStylePanel = new JPanel();
				private JRadioButton radioButton1 = new JRadioButton();
				private JRadioButton radioButton2 = new JRadioButton();
				private JRadioButton radioButton3 = new JRadioButton();
				private JRadioButton radioButton4 = new JRadioButton();
				private JRadioButton radioButton5 = new JRadioButton();
				private JRadioButton radioButton6 = new JRadioButton();
				private JLabel tipLabel2 = new JLabel();
		private JPanel personalSettingPanel = new JPanel();
			private JPanel backgroudMusicPanel = new JPanel();
			private static JRadioButton[] radioButton = new JRadioButton[15];
			private JPanel othersPanel = new JPanel();
				private static JCheckBox checkBox1 = new JCheckBox();
				private static JCheckBox checkBox2 = new JCheckBox();
				private static JCheckBox checkBox3 = new JCheckBox();
		private JButton resetButton = new JButton();
		private JButton confirmButton = new JButton();
		private JButton cancelButton = new JButton();
		
		private ButtonGroup backgroundMusicButtonGroup = new ButtonGroup();
		private ButtonGroup environmentStyleButtonGroup = new ButtonGroup();

	public OptionDialog() 
	{
		super();
		setResizable(false);
		getContentPane().setLayout(null);
		setTitle("选项");
		setBounds(100, 100, 499, 375);

	    tabbedPane.setBounds(0, 4, 492, 282);
		getContentPane().add(tabbedPane);

		dataSafetyPanel.setLayout(null);
		tabbedPane.addTab("数据安全", SwingResourceManager.getIcon(OptionDialog.class, "/images/optiondialog1.gif"), dataSafetyPanel, null);

		modifyPasswordPanel.setBorder(new TitledBorder(null, "密码修改", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, null));
		modifyPasswordPanel.setLayout(null);
		modifyPasswordPanel.setBounds(10, 24, 222, 191);
		dataSafetyPanel.add(modifyPasswordPanel);

		oldPasswordLabel.setText("旧密码：");
		oldPasswordLabel.setBounds(11, 23, 57, 24);
		modifyPasswordPanel.add(oldPasswordLabel);

		newPasswordLabel.setText("新密码：");
		newPasswordLabel.setBounds(11, 53, 57, 24);
		modifyPasswordPanel.add(newPasswordLabel);

		confirmPasswordLabel.setText("密码确认：");
		confirmPasswordLabel.setBounds(11, 83, 66, 24);
		modifyPasswordPanel.add(confirmPasswordLabel);

		oldPasswordField.setBounds(77, 24, 101, 24);
		modifyPasswordPanel.add(oldPasswordField);

		newPasswordField.setBounds(77, 53, 101, 24);
		modifyPasswordPanel.add(newPasswordField);

		confirmPasswordField = new JPasswordField();
		confirmPasswordField.setBounds(77, 83, 101, 24);
		modifyPasswordPanel.add(confirmPasswordField);

		modifyPasswordButton.setIcon(SwingResourceManager.getIcon(OptionDialog.class, "/images/password.gif"));
		modifyPasswordButton.setText("修改密码");
		modifyPasswordButton.setBounds(51, 126, 115, 35);
		modifyPasswordPanel.add(modifyPasswordButton);
		
		modifyPasswordButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
			    try
			    {
					File f = new File("data");
				    FileInputStream temp = new FileInputStream(f);//将密码以字节形式从文件中读取出来
				    byte[] password = new byte[temp.available()];
				    temp.read(password);
					String oldPassword = new String(oldPasswordField.getPassword());
					byte[] oldPasswordEncoded = DataSecurity.MD5(oldPassword.getBytes());//将输入的密码进行信息摘要
					String newPassword = new String(newPasswordField.getPassword());
					String confirmPassword = new String(confirmPasswordField.getPassword());
					
					if(password.length != 0 && 
							!DataSecurity.IsByteArrayTheSame(oldPasswordEncoded, password))//摘要以后的旧密码与原来的密码进行匹配比较
						JOptionPane.showMessageDialog(null, "旧密码不正确", "提示", JOptionPane.WARNING_MESSAGE);
					else
					{
						if(newPassword.compareTo(confirmPassword)!=0)
						{
							JOptionPane.showMessageDialog(null, "密码输入不一致，请重新输入", "提示", JOptionPane.WARNING_MESSAGE);
						}
						else
						{
							//加密算法:MD5信息摘要
							byte[] afterEncode = DataSecurity.MD5(newPassword.getBytes());
							FileOutputStream temp1 = new FileOutputStream(f);
							temp1.write(afterEncode);//将信息摘要以后的byte数组写进文件
							temp1.flush();
							@SuppressWarnings("unused")
							String keySource = new sun.misc.BASE64Encoder().encodeBuffer(afterEncode);
							DataSecurity.setKey(newPassword);
							JOptionPane.showMessageDialog(null, "密码修改成功", "提示", JOptionPane.WARNING_MESSAGE);
						}
				    }
				}
			    catch(IOException E)
			    {
			    	System.out.println("I/O错误！");
			    }
			    catch (Exception e) 
			    {
					e.printStackTrace();
				}
			}
		});

		autoSavePanel.setBorder(new TitledBorder(null, "自动存盘", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, null));
		autoSavePanel.setLayout(null);
		autoSavePanel.setBounds(238, 21, 248, 94);
		dataSafetyPanel.add(autoSavePanel);

		autoSaveCheckBox.setText("自动存盘");
		autoSaveCheckBox.setBounds(10, 20, 103, 23);
		autoSavePanel.add(autoSaveCheckBox);

		autoSaveLabel.setText("自动存盘时间间隔（分钟）");
		autoSaveLabel.setBounds(10, 53, 168, 23);
		autoSavePanel.add(autoSaveLabel);

		autoSaveTextField.setBounds(184, 55, 45, 20);
		autoSavePanel.add(autoSaveTextField);

		autoBackupPanel.setBorder(new TitledBorder(null, "自动备份", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, null));
		autoBackupPanel.setLayout(null);
		autoBackupPanel.setBounds(238, 121, 248, 94);
		dataSafetyPanel.add(autoBackupPanel);

		autoBackupCheckBox.setText("每次启动时自动备份");
		autoBackupCheckBox.setBounds(10, 20, 161, 23);
		autoBackupPanel.add(autoBackupCheckBox);

		autoBackupLabel.setText("保留备份数（最大为9）");
		autoBackupLabel.setBounds(15, 45, 138, 23);
		autoBackupPanel.add(autoBackupLabel);

		autoBackupTextField.setBounds(153, 49, 41, 20);
		autoBackupPanel.add(autoBackupTextField);

		tipLabel.setText("注：自动存盘和自动备份功能将在今后的版本中实现。");
		tipLabel.setBounds(10, 221, 476, 29);
		dataSafetyPanel.add(tipLabel);

		displayStylePanel.setLayout(null);
		tabbedPane.addTab("显示风格", SwingResourceManager.getIcon(OptionDialog.class, "/images/optiondialog2.gif"), displayStylePanel, null);

	    fontPanel.setBorder(new TitledBorder(null, "字体", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, null));
		fontPanel.setLayout(null);
		fontPanel.setBounds(270, 10, 203, 156);
		displayStylePanel.add(fontPanel);

		systemFontButton.setForeground(new Color(0, 0, 255));
		systemFontButton.setText("单击此处以设置系统字体");
		systemFontButton.setBounds(10, 24, 183, 48);
		fontPanel.add(systemFontButton);

		editorFontButton.setForeground(new Color(0, 0, 255));
		editorFontButton.setText("单击此处以设置编辑器字体");
		editorFontButton.setBounds(10, 78, 183, 48);
		fontPanel.add(editorFontButton);

		environmentStylePanel.setBorder(new TitledBorder(null, "外观风格", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, null));
		environmentStylePanel.setLayout(null);
		environmentStylePanel.setBounds(15, 10, 249, 232);
		displayStylePanel.add(environmentStylePanel);

		environmentStyleButtonGroup.add(radioButton1);
		radioButton1.setText("Swing默认");
		radioButton1.setBounds(10, 20, 121, 23);
		environmentStylePanel.add(radioButton1);

		environmentStyleButtonGroup.add(radioButton2);
		radioButton2.setText("KDE经典");
		radioButton2.setBounds(10, 49, 121, 23);
		environmentStylePanel.add(radioButton2);

		environmentStyleButtonGroup.add(radioButton3);
		radioButton3.setText("Gnome经典");
		radioButton3.setBounds(10, 78, 121, 23);
		environmentStylePanel.add(radioButton3);

		environmentStyleButtonGroup.add(radioButton4);
		radioButton4.setText("蓝天白云");
		radioButton4.setBounds(10, 107, 121, 23);
		environmentStylePanel.add(radioButton4);

		environmentStyleButtonGroup.add(radioButton5);
		radioButton5.setText("重金属");
		radioButton5.setBounds(10, 136, 121, 23);
		environmentStylePanel.add(radioButton5);

		environmentStyleButtonGroup.add(radioButton6);
		radioButton6.setText("休斯顿火箭");
		radioButton6.setBounds(10, 165, 121, 23);
		environmentStylePanel.add(radioButton6);

		tipLabel2.setForeground(new Color(255, 0, 0));
		tipLabel2.setFont(new Font("@华文琥珀", Font.PLAIN, 12));
		tipLabel2.setText("注：该功能将视情况在以后的版本中实现");
		tipLabel2.setBounds(6, 181, 233, 41);
		environmentStylePanel.add(tipLabel2);

		personalSettingPanel.setLayout(null);
		tabbedPane.addTab("个性设置", SwingResourceManager.getIcon(OptionDialog.class, "/images/optiondialog3.gif"), personalSettingPanel, null);

		backgroudMusicPanel.setLayout(null);
		backgroudMusicPanel.setBorder(new TitledBorder(null, "背景音乐", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, null));
		backgroudMusicPanel.setBounds(10, 7, 249, 235);
		personalSettingPanel.add(backgroudMusicPanel);

		for (int i = 0; i < 15; i++)
			radioButton[i] = new JRadioButton();
		
		backgroundMusicButtonGroup.add(radioButton[1]);
		backgroudMusicPanel.add(radioButton[1]);
		radioButton[1].setSelected(true);
		radioButton[1].setText("单簧管波尔卡");
		radioButton[1].setBounds(10, 21, 107, 23);

		backgroundMusicButtonGroup.add(radioButton[2]);
		backgroudMusicPanel.add(radioButton[2]);
		radioButton[1].setSelected(false);
		radioButton[2].setText("阿依达");
		radioButton[2].setBounds(10, 50, 107, 23);

		backgroundMusicButtonGroup.add(radioButton[3]);
		backgroudMusicPanel.add(radioButton[3]);
		radioButton[1].setSelected(false);
		radioButton[3].setText("四十交响曲");
		radioButton[3].setBounds(10, 79, 101, 23);

		backgroundMusicButtonGroup.add(radioButton[4]);
		backgroudMusicPanel.add(radioButton[4]);
		radioButton[1].setSelected(false);
		radioButton[4].setText("野蜂飞舞");
		radioButton[4].setBounds(10, 108, 101, 23);

		backgroundMusicButtonGroup.add(radioButton[5]);
		backgroudMusicPanel.add(radioButton[5]);
		radioButton[1].setSelected(false);
		radioButton[5].setText("土耳其进行曲");
		radioButton[5].setBounds(10, 137, 101, 23);

		backgroundMusicButtonGroup.add(radioButton[6]);
		backgroudMusicPanel.add(radioButton[6]);
		radioButton[1].setSelected(false);
		radioButton[6].setText("1812序曲");
		radioButton[6].setBounds(10, 166, 101, 23);

		backgroundMusicButtonGroup.add(radioButton[7]);
		radioButton[7].setBounds(10, 195, 101, 23);
		radioButton[1].setSelected(false);
		backgroudMusicPanel.add(radioButton[7]);
		radioButton[7].setText("花好月圆");
		
		backgroundMusicButtonGroup.add(radioButton[8]);
		backgroudMusicPanel.add(radioButton[8]);
		radioButton[1].setSelected(false);
		radioButton[8].setText("隐形的翅膀");
		radioButton[8].setBounds(117, 21, 125, 23);

		backgroundMusicButtonGroup.add(radioButton[9]);
		backgroudMusicPanel.add(radioButton[9]);
		radioButton[1].setSelected(false);
		radioButton[9].setText("遇见");
		radioButton[9].setBounds(117, 50, 125, 23);

		backgroundMusicButtonGroup.add(radioButton[10]);
		backgroudMusicPanel.add(radioButton[10]);
		radioButton[1].setSelected(false);
		radioButton[10].setText("fighter");
		radioButton[10].setBounds(117, 79, 125, 23);

		backgroundMusicButtonGroup.add(radioButton[11]);
		radioButton[11].setText("we will rock you");
		radioButton[1].setSelected(false);
		radioButton[11].setBounds(117, 108, 125, 23);
		backgroudMusicPanel.add(radioButton[11]);

		backgroundMusicButtonGroup.add(radioButton[12]);
		radioButton[12].setText("complicated");
		radioButton[1].setSelected(false);
		radioButton[12].setBounds(117, 137, 125, 23);
		backgroudMusicPanel.add(radioButton[12]);

		backgroundMusicButtonGroup.add(radioButton[13]);
		radioButton[13].setText("god is a girl");
		radioButton[1].setSelected(false);
		radioButton[13].setBounds(117, 166, 101, 23);
		backgroudMusicPanel.add(radioButton[13]);

		backgroundMusicButtonGroup.add(radioButton[14]);
		radioButton[14].setText("shinning friends");
		radioButton[1].setSelected(false);
		radioButton[14].setBounds(117, 195, 126, 23);
		backgroudMusicPanel.add(radioButton[14]);
		
		othersPanel.setLayout(null);
		othersPanel.setBorder(new TitledBorder(null, "其他", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, null));
		othersPanel.setBounds(262, 7, 215, 178);
		personalSettingPanel.add(othersPanel);

		checkBox1.setText("最小化还原时要求输入密码");
		checkBox1.setBounds(10, 28, 195, 23);
		othersPanel.add(checkBox1);

		checkBox2.setText("保存日记时清除空白日记");
		checkBox2.setBounds(10, 57, 195, 23);
		othersPanel.add(checkBox2);

		checkBox3.setText("保存日记时加上修改时间");
		checkBox3.setBounds(10, 86, 195, 23);
		othersPanel.add(checkBox3);

		getContentPane().add(resetButton);
		resetButton.setForeground(new Color(128, 0, 0));
		resetButton.setFont(new Font("", Font.PLAIN, 14));
		resetButton.setText("恢复默认设置");
		resetButton.setBounds(51, 292, 130, 39);
		
		resetButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0)
			{
				checkBox1.setSelected(false);
				checkBox2.setSelected(false);
				checkBox3.setSelected(false);
				
				radioButton[1].setSelected(false);
				radioButton[2].setSelected(false);
				radioButton[3].setSelected(false);
				radioButton[4].setSelected(false);
				radioButton[5].setSelected(false);
				radioButton[6].setSelected(false);
				radioButton[7].setSelected(false);
				radioButton[8].setSelected(false);
				radioButton[9].setSelected(false);
				radioButton[10].setSelected(false);
				radioButton[11].setSelected(false);
				radioButton[12].setSelected(false);
				radioButton[13].setSelected(false);
				radioButton[14].setSelected(false);
				
			}
		});

		confirmButton.setFont(new Font("", Font.PLAIN, 14));
		confirmButton.setIcon(SwingResourceManager.getIcon(OptionDialog.class, "/images/okey.gif"));
		confirmButton.setText("确认(O)");
		confirmButton.setBounds(237, 292, 116, 39);
		getContentPane().add(confirmButton);
		
		confirmButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (DiaryToolBar.getBgMusicToggleButton().isSelected())
				{
					Operation.musicPlay(false, getMusicItem());
				}
				dispose();
			}
		});
		

		cancelButton.setFont(new Font("", Font.PLAIN, 14));
		cancelButton.setIcon(SwingResourceManager.getIcon(OptionDialog.class, "/images/cancel.gif"));
		cancelButton.setText("取消(C)");
		cancelButton.setBounds(366, 292, 117, 39);
		getContentPane().add(cancelButton);
		
		cancelButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				dispose();
			}
		});
	}
	
	public static int getMusicItem()
	{
		try
		{
			if(radioButton[1].isSelected())return 1;
			if(radioButton[2].isSelected())return 2;
			if(radioButton[3].isSelected())return 3;
			if(radioButton[4].isSelected())return 4;
			if(radioButton[5].isSelected())return 5;
			if(radioButton[6].isSelected())return 6;
			if(radioButton[7].isSelected())return 7;
			if(radioButton[8].isSelected())return 8;
			if(radioButton[9].isSelected())return 9;
			if(radioButton[10].isSelected())return 10;
			if(radioButton[11].isSelected())return 11;
			if(radioButton[12].isSelected())return 12;
			if(radioButton[13].isSelected())return 13;
			if(radioButton[14].isSelected())return 14;
		}
		catch (Exception e)
		{
			return 1;
		}
		return 0;
	}
		
	public static JCheckBox getCheckBox1()
	{
	    return checkBox1;
	}
	
	public static JCheckBox getCheckBox2()
	{
		return checkBox2;
	}
		
	public static JCheckBox getCheckBox3()
	{
		return checkBox3;
	}
}
