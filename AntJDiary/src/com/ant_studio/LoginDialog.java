package com.ant_studio;

import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.swing.JButton;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

public class LoginDialog extends JDialog 
{	
	private JPasswordField passwordField;
	final JLabel passwordLabel = new JLabel();
	final JButton confirmButton = new JButton();
	final JButton cancelButton = new JButton();    
    
	/**
	 * 登录对话框
	 * author:黄文灏
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	/**
	 * Create the dialog
	 */
	
	public LoginDialog()
	{
		super();
		setTitle("登录");
		getContentPane().setLayout(null);
		setBounds(100, 100, 298, 136);
		setLocationRelativeTo(null);

		
		passwordLabel.setFont(new Font("", Font.PLAIN, 15));
		passwordLabel.setText("请输入您的密码：");
		passwordLabel.setBounds(7, 16, 149, 33);
		getContentPane().add(passwordLabel);

		confirmButton.setText("确认");
		confirmButton.setMargin(new Insets(2, 2, 2, 2));
		confirmButton.addActionListener(new ActionListener()
		{
			@SuppressWarnings("static-access")
			public void actionPerformed(ActionEvent arg0) 
			{
				try
				{					
					File f = new File("data");
					FileInputStream temp = new FileInputStream(f);
				    byte[] password = new byte[temp.available()];
				    temp.read(password);
				    String confirmPassword = new String(passwordField.getPassword());
				    byte[] temp1 = DataSecurity.MD5(confirmPassword.getBytes());

					if(password.length != 0 && !DataSecurity.IsByteArrayTheSame(password, temp1))	
						JOptionPane.showMessageDialog(null, "密码不正确", "提示", JOptionPane.WARNING_MESSAGE);
					else
					{
						try
						{
							JDiary mainWindow = new JDiary();
						    mainWindow.getJFrame().setVisible(true);
						}
						catch (Exception e) 
						{
							e.printStackTrace();
						}
						
						dispose();
					}	
				}
				catch(IOException E)
			    {
			    	System.out.println("I/O错误！");
			    } 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
					    
			}
		});
		confirmButton.setBounds(32, 55, 90, 30);
		getContentPane().add(confirmButton);

		cancelButton.setText("取消");
		cancelButton.setMargin(new Insets(2, 2, 2, 2));
		cancelButton.setBounds(150, 55, 90, 30);
		getContentPane().add(cancelButton);
		
		cancelButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				System.exit(0);
			}
		});

		passwordField = new JPasswordField();
		passwordField.setBounds(135, 15, 125, 27);
		getContentPane().add(passwordField);
	}
}
