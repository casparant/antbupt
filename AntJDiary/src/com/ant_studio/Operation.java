package com.ant_studio;

import java.applet.Applet;
import java.applet.AudioClip;
import java.io.File;

import javax.swing.*;

public class Operation 
{
	private static DiaryModel myDiaryModel = JDiary.getModel();

	public Operation()
	{
		//
	}
	
	public static void autoLine(JToggleButton autoLineToggleButton, JTextArea textArea,
			JCheckBoxMenuItem autoLineCheckBoxMenuItem)
	{
		if (autoLineCheckBoxMenuItem.isSelected())
		{
			autoLineToggleButton.setSelected(true);
			textArea.setLineWrap(true);
			textArea.setWrapStyleWord(true);
		}
		else
		{
			autoLineToggleButton.setSelected(false);
			textArea.setLineWrap(false);
			textArea.setWrapStyleWord(false);
		}
	}
	
	public static void autoLine(JCheckBoxMenuItem autoLineCheckBoxMenuItem, JTextArea textArea,
			JToggleButton autoLineToggleButton)
	{
		if (autoLineToggleButton.isSelected())
		{
			autoLineCheckBoxMenuItem.setSelected(true);
			textArea.setLineWrap(true);
			textArea.setWrapStyleWord(true);
		}
		else
		{
			autoLineCheckBoxMenuItem.setSelected(false);
			textArea.setLineWrap(false);
			textArea.setWrapStyleWord(false);
		}
	}
	
	public static void readOnly(JToggleButton readOnlyToggleButton, JTextArea textArea,
			JCheckBoxMenuItem readOnlyCheckBoxMenuItem)
	{
		if (readOnlyCheckBoxMenuItem.isSelected())
		{
			readOnlyToggleButton.setSelected(true);
			textArea.setEditable(false);
		}
		else
		{
			readOnlyToggleButton.setSelected(false);
			textArea.setEditable(true);
		}
	}
	
	public static void readOnly(JCheckBoxMenuItem readOnlyCheckBoxMenuItem, JTextArea textArea,
			JToggleButton readOnlyToggleButton)
	{
		if (readOnlyToggleButton.isSelected())
		{
			readOnlyCheckBoxMenuItem.setSelected(true);
			textArea.setEditable(false);
		}
		else
		{
			readOnlyCheckBoxMenuItem.setSelected(false);
			textArea.setEditable(true);
		}
	}
	
	public static void bgMusic(JToggleButton bgMusicToggleButton,
			JCheckBoxMenuItem bgMusicCheckBoxMenuItem)
	{
		if (bgMusicCheckBoxMenuItem.isSelected())
		{
			bgMusicToggleButton.setSelected(true);
			musicPlay(false, OptionDialog.getMusicItem());
		}
		else
		{
			bgMusicToggleButton.setSelected(false);
			musicPlay(true, OptionDialog.getMusicItem());
		}
	}
	
	public static void bgMusic(JCheckBoxMenuItem bgMusicCheckBoxMenuItem,
			JToggleButton bgMusicToggleButton)
	{

		if (bgMusicToggleButton.isSelected())
		{
			bgMusicCheckBoxMenuItem.setSelected(true);
			musicPlay(false, OptionDialog.getMusicItem());
		}
		else
		{
			bgMusicCheckBoxMenuItem.setSelected(false);
			musicPlay(true, OptionDialog.getMusicItem());
		}
	}
	
	public static void createNewDiary(boolean flag, JLabel label)
	{
		if (flag)
		{	
			label.setText(Format.date(Integer.parseInt(ChooseDateDialog.getYear()), 
					Integer.parseInt(ChooseDateDialog.getMonth()), 
					Integer.parseInt(ChooseDateDialog.getDay())));
			myDiaryModel.addDiary(
					new Diary(ChooseDateDialog.getYear()+"年", 
						ChooseDateDialog.getMonth()+"月",
						ChooseDateDialog.getDay()+"日",DiaryHeadPanel.getWeather()));
		}
	}
	
	public static void deleteDiary()
	{
		myDiaryModel.deleteCurrentDiary( myDiaryModel.getCurrentDiary());
	}
	
	public static void save()
	{
		myDiaryModel.saveDiarys();
	}
	
	@SuppressWarnings("deprecation")
	public static void musicPlay(boolean isStop, int i)
	{
		try
		{
			final AudioClip audio = Applet.newAudioClip(new File("music/" + i + ".wav").toURL());
			for (int j = 1; j <= 14; j++)
				Applet.newAudioClip(new File("music/" + j + ".wav").toURL()).stop();
			if (!isStop) audio.play();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
