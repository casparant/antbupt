
/**
 * @author LiHuiying
 * This function is written in order to
 * compare two strings. 
 * It is a utilization function.
 */

#include <global.h>
#include <proto.h>
#include "string.h"

/*======================================================================*
                               strcmp
 *======================================================================*/
PUBLIC int strcmp(char* str1, t_32* str2, int len1, int len2)
{
	int i;
	
	if(len1 != len2)
	{
		return 1;
	}
	else
	{
		for(i = 0; i < len1; i++)
			if(str1[i]!=str2[i])
			{
				return 1;
			}
		return 0;
	}
}
