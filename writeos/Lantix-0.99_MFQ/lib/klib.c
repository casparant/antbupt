
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                            klib.c
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                    Forrest Yu, 2005
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#include <global.h>
#include <proto.h>
#include "string.h"


/*======================================================================*
                               is_alphanumeric
 *======================================================================*/
PUBLIC t_bool is_alphanumeric(char ch)
{
	return ((ch >= ' ') && (ch <= '~'));
}


/*======================================================================*
                               itoa
 *======================================================================*/
PUBLIC char * itoa(char * str, int num)/* 数字前面的 0 不被显示出来, 比如 0000B800 被显示成 B800 */
{
	char *	p = str;
	char	ch;
	int	i;
	t_bool	flag = FALSE;
	
	*p++ = '0';
	*p++ = 'x';

	if(num == 0){
		*p++ = '0';
	}
	else{	
		for(i=28;i>=0;i-=4){
			ch = (num >> i) & 0xF;
			if(flag || (ch > 0))
			{
				flag = TRUE;
				ch += '0';
				if(ch > '9'){
					ch += 7;
				}
				*p++ = ch;
			}
		}
	}

	*p = 0;

	return str;
}

/*======================================================================*
  @author: Wang Shuo
    This function convert int to string.
 *======================================================================*
                               itoa1
 *======================================================================*/
PUBLIC char* itoa1(char *arr, int intg)
{
	int i=0, tmp, tmp1;
	int intg1 = intg;
//	if (intg == 0)
//		return "0\0";
	while(intg!=0)
	{
	   intg = intg/10;
	   i++;
	}
	arr[i]='\0';
	if (i==0)
	{
		arr[0] = '0';
		arr[1] = '\0';
	}
	while(intg1!=0)
	{  
	   tmp = intg1;
	   intg1 = intg1/10;
	   tmp1 = tmp-intg1*10;
	   arr[i-1]= tmp1 + '0';
	   i--;
	}

	return arr;
}

/*======================================================================*
                               disp_int
 *======================================================================*/
PUBLIC void disp_int(int input)
{
	char output[16];
	itoa(output, input);
	disp_str(output);
}

/*======================================================================*
                               delay
 *======================================================================*/
PUBLIC void delay(int time)
{
	int i, j, k;
	for(k=0;k<time;k++){
		/*for(i=0;i<10000;i++){	for Virtual PC	*/
		for(i=0;i<10;i++){/*	for Bochs	*/
			for(j=0;j<10000;j++){}
		}
	}
}

/*======================================================================*  @author: Li Huiying
    This finction allocate memory from heap, using first-fit algorithm.
 *======================================================================*
                               malloc
 *======================================================================*/
void *malloc(int size)
{
	int i, j;
	int nr_free = 0;
	
	for(i = 0; i < HEAP_SIZE; i++)
	{
		if (heap_is_used[i] == 0) ++nr_free;
		else nr_free = 0;
		if (nr_free >= size)
		{
			for (j = i-size+1; j <= i; j++)
				heap_is_used[j] = p_proc_ready->pid;
			return &heap[i-size+1];
		}
	}
	return NULL;
}

/*======================================================================*  @author: Li Huiying
    This finction free memory from heap.
 *======================================================================*
                               free
 *======================================================================*/
int free(void* ptr)
{
	int i, j;
	t_32 used_flag;
	
	for (i = 0; i < HEAP_SIZE; i++)
	{
		if (ptr == (void *)heap[i])
		{
			j = i;
			used_flag = heap_is_used[i];
			while (heap_is_used[j] == used_flag)
				heap_is_used[j] = 0;
			return 0;
		}
	}
	return -1;
}

