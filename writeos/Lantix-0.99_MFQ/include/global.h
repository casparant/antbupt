
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                            global.h
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                    Forrest Yu, 2005
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#ifndef _GLOBAL_H_
#define _GLOBAL_H_


#include <type.h>
#include <const.h>

#include <console.h>
#include <floppy.h>
#include <keyboard.h>
#include <proc.h>
#include <protect.h>
#include <tty.h>

/* EXTERN is defined as EXTERN except in global.c */
#ifdef	GLOBAL_VARIABLES_HERE
#undef	EXTERN
#define	EXTERN
#endif

EXTERN	int			ticks;

EXTERN	int			disp_pos;
EXTERN	t_8			gdt_ptr[6];	// 0~15:Limit  16~47:Base
EXTERN	DESCRIPTOR	gdt[GDT_SIZE];
EXTERN	t_8			idt_ptr[6];	// 0~15:Limit  16~47:Base
EXTERN	GATE		idt[IDT_SIZE];

EXTERN	t_32		k_reenter;

EXTERN	TSS			tss;
EXTERN	PROCESS*	p_proc_ready;

EXTERN	int			nr_current_console;

EXTERN	PROCESS		proc_table[];
EXTERN	char		task_stack[];
EXTERN	TASK		task_table[];
EXTERN	TASK		user_proc_table[];
EXTERN	TTY			tty_table[];
EXTERN	CONSOLE		console_table[];

/* @author: Li Huiying */
EXTERN	cmd_bufT	cmd_buffers[];
EXTERN	int			adopted[];
EXTERN	t_32		l1[];
EXTERN	t_32		l2[];
EXTERN	t_32		l3[];
EXTERN	int			parrot;
EXTERN	int			heap[];
EXTERN	t_32		heap_is_used[];
EXTERN	char*		heap_head;
EXTERN	char*		heap_ptr;

EXTERN	t_8			buf[];
EXTERN	t_8			tmp_floppy_area[1024];
EXTERN	t_8			current_DOR;
EXTERN	struct request		request;

EXTERN	t_pf_irq_handler	irq_table[];
EXTERN	t_sys_call			sys_call_table[];

#endif /* _GLOBAL_H_ */
