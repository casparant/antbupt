
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                            string.h
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                    Forrest Yu, 2005
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#ifndef _STRING_H_
#define _STRING_H_

#include <const.h>
/* string.asm */
PUBLIC	void*	memcpy(void* p_dst, void* p_src, int size);
PUBLIC	void	memset(void* p_dst, char ch, int size);
PUBLIC	char*	strcpy(char* p_dst, char* p_src);
PUBLIC	int		strlen(char* p_str);

/* @author: Li Huiying */
/* string.c */
PUBLIC int strcmp(char* str1, t_32* str2, int len1, int len2);
#endif /* _STRING_H_ */
