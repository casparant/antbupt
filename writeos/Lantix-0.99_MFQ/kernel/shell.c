/**
 * @author LiHuiying(Youwillwin)
 * This file includes shell and its supported functions.
 * The command that supported by the shell:
 * version
 * ps
 * help
 * exec
 * kill
 */
#include <global.h>
#include <proto.h>
#include "string.h"

/**
 * Function that supported by the shell
 */
PRIVATE void version();
PRIVATE void unsupported_cmd_handler();
PRIVATE void help();
PRIVATE void ps();
PRIVATE int  exec(char id);
PRIVATE int  kill(char id);
PRIVATE int  isPreemp(int index);
PRIVATE void cat();
PRIVATE void ls();
EXTERN	void cls();

PUBLIC void shell(cmd_bufT* cmd)
{
	int i;
//	printf("Response: ");
	if(!strcmp("version", cmd->buffer, 7, cmd->count))
	{
		version();
	}
	else if(!strcmp("help", cmd->buffer, 4, cmd->count))
	{
		help();
	}
	else if(!strcmp("ps", cmd->buffer, 2, cmd->count))
	{
		ps();
	}
	else if(!strcmp("cat", cmd->buffer, 3, cmd->count))
	{
		cat();
	}
	else if(!strcmp("cpuinfo", cmd->buffer, 7, cmd->count))
	{
		unsigned int tmp = disp_pos;
		printf("\n");
		disp_pos=tty_table->p_console->cursor*2;
	//	printf("The position: %x", tty_table->p_console->cursor);
		cpuinfo();
		disp_pos=tmp;
	}
	else if(!strcmp("cls", cmd->buffer, 3, cmd->count))
	{
		cls();
	}
	else if(!strcmp("ls", cmd->buffer, 2, cmd->count))
	{
		ls();
	}
	else if(!strcmp("exec a", cmd->buffer, 6, cmd->count))
	{
		exec('A');
	}
	else if(!strcmp("exec b", cmd->buffer, 6, cmd->count))
	{
		exec('B');
	}
	else if(!strcmp("exec c", cmd->buffer, 6, cmd->count))
	{
		exec('C');
	}
	else if(!strcmp("exec d", cmd->buffer, 6, cmd->count))
	{
		exec('D');
	}
	else if(!strcmp("exec e", cmd->buffer, 6, cmd->count))
	{
		exec('E');
	}
	else if(!strcmp("exec f", cmd->buffer, 6, cmd->count))
	{
		exec('F');
	}
	else if(!strcmp("exec g", cmd->buffer, 6, cmd->count))
	{
		exec('G');
	}
	else if(!strcmp("kill 1", cmd->buffer, 6, cmd->count))
	{
		kill('A');
	}
	else if(!strcmp("kill 2", cmd->buffer, 6, cmd->count))
	{
		kill('B');
	}
	else if(!strcmp("kill 3", cmd->buffer, 6, cmd->count))
	{
		kill('C');
	}
	else if(!strcmp("kill 4", cmd->buffer, 6, cmd->count))
	{
		kill('D');
	}
	else if(!strcmp("kill 5", cmd->buffer, 6, cmd->count))
	{
		kill('E');
	}
	else if(!strcmp("kill 6", cmd->buffer, 6, cmd->count))
	{
		kill('F');
	}
	else if(!strcmp("kill 7", cmd->buffer, 6, cmd->count))
	{
		kill('G');
	}
	else
	{
		unsupported_cmd_handler();
	}
}

PRIVATE void version()
{
	printf("\nversion: Lantix0.26  (FIFO queue).\n");
	printf("This version adopts FIFO queue scheduling method.\n");
	printf("Author: Li Huiying (Youwillwin) (Scheduling, Shell, Input/Output)\n");
	printf("        Zhang Jinli(Caspar)  (File system driver)\n");
}

PRIVATE void unsupported_cmd_handler()
{
	printf("\nOpps! I don't recongnize it~~~ please retry :-)\n");
}

PRIVATE void help()
{
	printf("\nHello!\n");
	printf("This simple command line is designed to let Lantix interact with its users.\n");
	printf("I did not use parse-tree or other complicated mothod to ensure the robustness\n");
	printf("of this shell because of time shortage, but the commands supported by\n");
	printf("this cmd tool would be essential and useful:\n\n");
	printf("cat        --this is a command simulates 'cat' command in Linux, but the source\n");
	printf("             and destination is limited to keyboard and screen because of \n");
	printf("             incomplete file system.(Actually our file system support is fairly\n");
	printf("             good, and it will be better if time is adequat ;-))\n");
	printf("cls        --this is a command that clears screen. Only tty1 will be cleared\n");
	printf("             since it is the interactive tty.\n");
	printf("cpuinfo    --this is a command that display the results of Assembly command\n");
	printf("             'cpuid',which disseminates information about CPU vendor in ebx,\n");
	printf("             ecx and edx registers\n");
	printf("exec       --this command executes a process. Its parameter should be 'a'\n");
	printf("             through'g',which identify the 7 processes that are available for\n");
	printf("             executing.\n");
	printf("             Example: shell# exec a\n");
	printf("help       --display helpful information as u see~~~\n");
	printf("kill       --this command kills a running process. Its parameter should be \n");
	printf("             decimal pid that identifies the process you want to kill. (Pid can \n");
	printf("             be obtained by 'ps' command.)\n");
	printf("             Example: shell# kill 1\n");
	printf("ls         --\n");
	printf("ps         --this command simulates 'ps' command in Linux, which display\n");
	printf("             information about all running processes. No parameter is needed as\n");
	printf("             in Linux because of the tightened time.\n");
	printf("version    --this command shows the version and the authors of the version.\n");
	printf("             Typically, different version is with different scheduling method.\n");
} 

PRIVATE void ps()
{
	int i,j;
	
	printf("\nExisting kernel and user processes:\n");
	printf("   pid   name      priority\n");
	for(i = 0; i < NR_TASKS+NR_PROCS; i++)
	{
		if(adopted[i])
		{
			printf("   %x      ", proc_table[i].pid);
			for(j=0;j<strlen(proc_table[i].name); j++)
			{
				printf("%c", proc_table[i].name[j]);	
			}
			printf("        %x", proc_table[i].priority);
			printf("\n");
		}
	}
//	printf("===========End PS\n");
}

PRIVATE int exec(char id)
{
	int index;
	int i;
	index = id - 'A' + 1;
	if(adopted[index])
	{
		printf("\nAre u sure? This process is running~~~\n");
		return 1;
	}
	if(inqueue(index)!=0)
		return 1;
	/*lhy1225*/
	t_8 rpl = RPL_USER;
	TASK* p_task = user_proc_table + (index - NR_TASKS);
	char* p_task_stack=task_stack + STACK_SIZE_TOTAL;
	//定位栈
	for(i = 0; i < index; i++)
	{
		p_task_stack-=0x8000;
	}
	proc_table[index].regs.cs		= ((8 * 0) & SA_RPL_MASK & SA_TI_MASK) | SA_TIL | rpl;
	proc_table[index].regs.ds		= ((8 * 1) & SA_RPL_MASK & SA_TI_MASK) | SA_TIL | rpl;
	proc_table[index].regs.es		= ((8 * 1) & SA_RPL_MASK & SA_TI_MASK) | SA_TIL | rpl;
	proc_table[index].regs.fs		= ((8 * 1) & SA_RPL_MASK & SA_TI_MASK) | SA_TIL | rpl;
	proc_table[index].regs.ss		= ((8 * 1) & SA_RPL_MASK & SA_TI_MASK) | SA_TIL | rpl;
	proc_table[index].regs.gs		= (SELECTOR_KERNEL_GS & SA_RPL_MASK) | rpl;
	proc_table[index].regs.eip	= (t_32)p_task->initial_eip;
	proc_table[index].regs.esp	= (t_32)p_task_stack;
	proc_table[index].regs.eflags	= 0x202;
	/*lhy1225*/
	adopted[index] = 1;
	if(isPreemp(index)!=0)
	{
		preempt(index);
	}
	
	return 0;
}

/*
 * This function judges whether the newly executed process is
 * in the higher level than the current executing process,
 * if so, it call scheduler to immediately pick up the new proc.
 */
PRIVATE int isPreemp(int index)
{
	t_32* cl;
	cl = getCurrentLevel();
	if(cl==l1)
	{
		return 0;
	}
	else if(cl==l2&&proc_table[index].priority>=15)
	{
		return 1;
	}
	else if(cl==l3&&proc_table[index].priority>=10)
	{
		return 1;
	}
	else{}
}


PRIVATE int kill(char id)
{
	int index;
	int i, j;
	index = id - 'A' + 1;
	if(!adopted[index])
	{
		printf("\nOpps! No such process in execution~~~\n");
		return 1;
	}
	adopted[index] = 0;
	for(i = 0; i < NR_TASKS+NR_PROCS; i++)
	{
		if(l1[i]==index)
		{
			for(j=i; j < NR_TASKS+NR_PROCS-1; j++)
				l1[j]=l1[j+1];
			l1[NR_TASKS+NR_PROCS-1]=-1;
			return 0;
		}
		else if(l2[i]==index)
		{
			for(j=i; j < NR_TASKS+NR_PROCS-1; j++)
				l2[j]=l2[j+1];
			l2[NR_TASKS+NR_PROCS-1]=-1;
			return 0;
		}
		else if(l3[i]==index)
		{
			for(j=i; j < NR_TASKS+NR_PROCS-1; j++)
				l3[j]=l3[j+1];
			l3[NR_TASKS+NR_PROCS-1]=-1;
			return 0;
		}
		else{}
	}
	
	/*lhy1225*/
	//free heap memory
	for(i = 0; i < HEAP_SIZE; i++)
	{
		if(heap_is_used[i]==proc_table[index].pid)
			heap_is_used[i]=0;
	}
	/*end of lhy*/
	schedule();
	
	return 0;
}

/**
 * @author: LiHuiying
 * This function impelements the cat command,
 * simulating Linux cat but without file-based
 * source or desitination.
 */
PRIVATE void cat()
{
	parrot=1;
}

/*
 * @author: Caspar Zhang
 */
PRIVATE void ls()
{
	int i, j;
	unsigned sizeM, sizeK, size;
		
	request.sector =18;
	printf("\nFILE          LAST MODIFIED         SIZE\n");
	for (i = 0; i < 14; i++)
	{
		request.sector++;	//sector init = 19;
		floppy_init();
		
		for (j = 0; j < 512; j+=32)
			if (buf[j] != 0xE5)
				switch(buf[j+11])
				{
				case 0x0:
				case 0xF:
					continue;
					break;
				case 0x10:
					write(&buf[j], 8);
					printf("/   ");
					printf("  %d-%d-%d  %d:%d:%d\n",
						1980+(buf[j+25]>>1), ((buf[j+25]&1)<<3)|(buf[j+24]>>5), buf[j+24]&0x1F,
						buf[j+23]>>3, (buf[j+23]&0x7<<3)|(buf[j+22]>>5), buf[j+22]&0x1F);
					break;
				case 0x20:
					write(&buf[j], 8);
					printf(".");
					write(&buf[j+8], 3);
					printf("  %d-%d-%d  %d:%d:%d  ",
						1980+(buf[j+25]>>1), ((buf[j+25]&1)<<3)|(buf[j+24]>>5), buf[j+24]&0x1F,
						buf[j+23]>>3, (buf[j+23]&0x7<<3)|(buf[j+22]>>5), buf[j+22]&0x1F);
					if ((buf[j+31]<<4)|(buf[j+30]>>4))
						printf("%dM", (buf[j+31]<<4)|(buf[j+30]>>4));
					if (((buf[j+30]&0xF)<<6)|(buf[j+29]>>2) || (buf[j+31]<<4)|(buf[j+30]>>4)) 
						printf("%dK", ((buf[j+30]&0xF)<<6)|(buf[j+29]>>2));
					printf("%dB\n", (buf[j+29]&3)<<8|buf[j+28]);
					break;
				default:
					continue;
				}
		
	}
}
