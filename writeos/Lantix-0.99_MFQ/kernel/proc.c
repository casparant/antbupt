/**
 * @author LiHuiying(Youwillwin)
 * This file contains the implementation of the
 * multi-level queue scheduling.
 */
#include <global.h>
#include <proto.h>


/**
 * @author LiHuiying(Youwillwin)
 * This function is modified to meet the need
 * of multilevel queue scheduling mothod.
 */
PUBLIC void schedule()
{
	PROCESS*	p;
	int		greatest_ticks = 0;
	int i=0;
	t_32 * cl;

	//First, lower the current running process 
	lower(p_proc_ready->pid);
	cl = getCurrentLevel();
	//choose the first process in each queue, if there is no process
	//in the queue, turn to the next level queue.
	//note:there must be at least one process in the next queue,
	//since current process has just been lowered.
	if(cl==l1)
	{
		if(l1[0]!=-1)
			p_proc_ready=&proc_table[l1[0]];
		else
			p_proc_ready=&proc_table[l2[0]];
	}
	else if(cl==l2)
	{
		if(l2[0]!=-1)
			p_proc_ready = &proc_table[l2[0]];
		else
			p_proc_ready=&proc_table[l3[0]];
		
		//do the preempt.
		if(l1[0]!=-1)
			p_proc_ready=&proc_table[l1[0]];
	}
	else if(cl==l3)
	{
		if(l3[0]!=-1)
			p_proc_ready = &proc_table[l3[0]];
		else
			p_proc_ready=&proc_table[l1[0]];
			
		//do the preempt.
		if(l2[0]!=-1)
			p_proc_ready=&proc_table[l2[0]];
		
		if(l1[0]!=-1)
			p_proc_ready=&proc_table[l1[0]];
	}
}


PUBLIC int sys_get_ticks()
{
	return ticks;
}

/**
 * @author LiHuiying(Youwillwin)
 * this function put a certain process into 
 * the proper level queue according to its
 * priority. The process is always added to
 * the end of the queue by this function.
 */
PUBLIC int inqueue(t_32 pid)
{
	int p, i;
	p = proc_table[pid].priority;
	if(p>=15)
	{
		i = 0;
		while(l1[i]!=-1&&i<NR_TASKS+NR_PROCS){i++;}
		if(i==NR_TASKS+NR_PROCS)
		{
			printf("Process Creation Error!");
			return 1;
		}
		l1[i]=pid;
		return 0;
	}
	else if(p<15 && p>=10)
	{
		i=0;
		while(l2[i]!=-1&&i<NR_TASKS+NR_PROCS){i++;}
		if(i==NR_TASKS+NR_PROCS)
		{
			printf("Process Creation Error!");
			return 1;
		}
		l2[i]=pid;
		return 0;
	}
	else if(p < 10 && p>=5)
	{
		i=0;
		while(l3[i]!=-1&&i<NR_TASKS+NR_PROCS){i++;}
		if(i==NR_TASKS+NR_PROCS)
		{
			printf("Process Creation Error!");
			return 1;
		}
		l3[i]=pid;
		return 0;
	}
	else
	{
		printf("Unknown priority!");
		return 2;
	}
}
/**
 * @author LiHuiying(Youwillwin)
 * Ths function lower a process to next queue,
 * at the same time retain its time ticks.
 * If the process has been dumpped into the third
 * level and has been executed, it will be
 * replaced into his belonged level again according
 * to its priority.
 */
PUBLIC int lower(t_32 pid)
{
	int i, j;
	proc_table[pid].ticks=proc_table[pid].priority;
//	printf("The l1[0]: %x", l1[0]);
	for(i = 0; i < NR_TASKS+NR_PROCS; i++)
	{
		if(l1[i]==pid)
		{
			for(j=i; j < NR_TASKS+NR_PROCS-1; j++)
				l1[j] = l1[j+1];
			l1[NR_TASKS+NR_PROCS-1]=-1;
			j=0;
			while(l2[j]!=-1&&j<NR_TASKS+NR_PROCS){j++;}
			if(j==NR_TASKS+NR_PROCS)
			{
				printf("\nScheduling Error!");
				return 1;
			}
			l2[j]=pid;
		//	printf("/////////The l2[0]: %x  %x\n", l2[0], j);
			return 0;
		}
		else if(l2[i]==pid)
		{
			for(j=i; j < NR_TASKS+NR_PROCS-1; j++)
				l2[j] = l2[j+1];
			l2[NR_TASKS+NR_PROCS-1]=-1;
			j=0;
			while(l3[j]!=-1&&j<NR_TASKS+NR_PROCS){j++;}
			if(j==NR_TASKS+NR_PROCS)
			{
				printf("\nScheduling Error!");
				return 1;
			}
			l3[j]=pid;
			return 0;
		}
		else if(l3[i]==pid)
		{
			for(j=i; j < NR_TASKS+NR_PROCS-1; j++)
				l3[j] = l3[j+1];
			l3[NR_TASKS+NR_PROCS-1]=-1;
			inqueue(pid);
		}
	}
	
	return 0;
}

/**
 * @author LiHuiying(Youwillwin)
 * This function finds out the level
 * which current ready process is in.
 */
t_32* getCurrentLevel()
{
	int i = 0;
	for(i = 0; i < NR_TASKS+NR_PROCS; i++)
	{
		if(l1[i]==p_proc_ready->pid)
			return l3;
		else if(l2[i]==p_proc_ready->pid)
			return l1;
		else if(l3[i]==p_proc_ready->pid)
			return l2;
		else 
		{}
	}
}

/**
 * @author LiHuiying(Youwillwin)
 * This function do the preempt in multi-level queue
 * scheduling. 
 */
void preempt(t_32 pid)
{
	schedule();
}

