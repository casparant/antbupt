
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                            main.c
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                    Forrest Yu, 2005
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#include <global.h>
#include <proto.h>


/*======================================================================*
                            tinix_main
 *======================================================================*/
PUBLIC int tinix_main()
{
	disp_str("-----\"Lantix 0.99\" begins-----\n");

	TASK*		p_task;
	PROCESS*	p_proc		= proc_table;
	char*		p_task_stack	= task_stack + STACK_SIZE_TOTAL;
	t_16		selector_ldt	= SELECTOR_LDT_FIRST;
	int		i, j;
	t_8		privilege;
	t_8		rpl;
	int		eflags;
	
	/**
	 * @author LiHuiying 
	 */
	//initialize three queues
	for(j=0; j < NR_TASKS+NR_PROCS; j++)
	{
		l1[j]=-1;
		l2[j]=-1;
		l3[j]=-1;
	}
	for(i=0;i<NR_TASKS+NR_PROCS;i++){
		if (i < NR_TASKS) {	/* 任务 */
			p_task		= task_table + i;
			privilege	= PRIVILEGE_TASK;
			rpl		= RPL_TASK;
			
			eflags		= 0x1202;	/* IF=1, IOPL=1, bit 2 is always 1 */
		}
		else {			/* 用户进程 */
			p_task		= user_proc_table + (i - NR_TASKS);
			privilege	= PRIVILEGE_USER;
			rpl		= RPL_USER;
			eflags		= 0x202;	/* IF=1, bit 2 is always 1 */
		}

		strcpy(p_proc->name, p_task->name);	/* name of the process */
		p_proc->pid	= i;			/* pid */

		//segment memo
		p_proc->ldt_sel	= selector_ldt;
		memcpy(&p_proc->ldts[0], &gdt[SELECTOR_KERNEL_CS >> 3], sizeof(DESCRIPTOR));
		p_proc->ldts[0].attr1 = DA_C | privilege << 5;	/* change the DPL */
		memcpy(&p_proc->ldts[1], &gdt[SELECTOR_KERNEL_DS >> 3], sizeof(DESCRIPTOR));
		p_proc->ldts[1].attr1 = DA_DRW | privilege << 5;/* change the DPL */
		
		//Registers
		p_proc->regs.cs		= ((8 * 0) & SA_RPL_MASK & SA_TI_MASK) | SA_TIL | rpl;
		p_proc->regs.ds		= ((8 * 1) & SA_RPL_MASK & SA_TI_MASK) | SA_TIL | rpl;
		p_proc->regs.es		= ((8 * 1) & SA_RPL_MASK & SA_TI_MASK) | SA_TIL | rpl;
		p_proc->regs.fs		= ((8 * 1) & SA_RPL_MASK & SA_TI_MASK) | SA_TIL | rpl;
		p_proc->regs.ss		= ((8 * 1) & SA_RPL_MASK & SA_TI_MASK) | SA_TIL | rpl;
		p_proc->regs.gs		= (SELECTOR_KERNEL_GS & SA_RPL_MASK) | rpl;
		p_proc->regs.eip	= (t_32)p_task->initial_eip;
		p_proc->regs.esp	= (t_32)p_task_stack;
		p_proc->regs.eflags	= eflags;

		p_proc->nr_tty		= 0;

		p_task_stack -= p_task->stacksize;
		p_proc++;
		p_task++;
		selector_ldt += 1 << 3;	/*selector+=8*/
	}
	proc_table[0].ticks = proc_table[0].priority = 15;
	proc_table[1].ticks = proc_table[1].priority =  10;
	proc_table[2].ticks = proc_table[2].priority =  10;
	proc_table[3].ticks = proc_table[3].priority =  10;
	proc_table[4].ticks = proc_table[4].priority =  10;
	proc_table[5].ticks = proc_table[5].priority =  5;
	proc_table[6].ticks = proc_table[6].priority =  5;
	proc_table[7].ticks = proc_table[7].priority =  5;

	proc_table[1].nr_tty = 1;
	proc_table[2].nr_tty = 1;
	proc_table[3].nr_tty = 1;
	proc_table[4].nr_tty = 1;
	proc_table[5].nr_tty = 1;
	proc_table[6].nr_tty = 1;
	proc_table[7].nr_tty = 1;

	k_reenter	= 0;
	ticks		= 0;

	p_proc_ready	= proc_table;
	adopted[0]=1;
	l1[0]=p_proc_ready->pid;
	parrot=0;
	heap_head = (char *)heap;
	heap_ptr = (char *)heap;
	
	init_clock();

	restart();

	while(1){}
}

/*======================================================================*
                               TestA
 *======================================================================*/
void TestA()
{
	char* a;
	a = (char*)malloc(sizeof(char)*81);
	printf("\nheap_A: %x\n", a);
	while(1){
		printf("A");
		milli_delay(200);
	}
}


/*======================================================================*
                               TestB
 *======================================================================*/
void TestB()
{
	char* a;
	a = (char*)malloc(sizeof(char)*81);
	printf("\nheap_B: %x\n", a);
	int i = 0;
	while(1){
		printf("B");
		milli_delay(200);
	}
}


/*======================================================================*
                               TestC
 *======================================================================*/
void TestC()
{
	int i = 0;
	while(1){
		printf("C");
		milli_delay(200);
	}
}

void TestD()
{
	int i = 0;
	while(1){
		printf("D");
		milli_delay(200);
	}
}

void TestE()
{
	int i = 0;
	while(1){
		printf("E");
		milli_delay(200);
	}
}

void TestF()
{
	int i = 0;
	while(1){
		printf("F");
		milli_delay(200);
	}
}

void TestG()
{
/*The following declaration is a demostration of Stack Overflow.*/
/*Stack Size is initialzed to 0x8000h*/
//	char wrong[0x80000];
	int i = 0;
	while(1){
		printf("G");
		milli_delay(200);
	}
}

