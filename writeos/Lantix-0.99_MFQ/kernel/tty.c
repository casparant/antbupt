
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                               tty.c
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                    Forrest Yu, 2005
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#include <global.h>
#include <proto.h>

#define TTY_FIRST	(tty_table)
#define TTY_END		(tty_table + NR_CONSOLES)

/* 本文件内函数声明 */
PRIVATE void	init_tty(TTY* p_tty);
PRIVATE void	tty_do_read(TTY* p_tty);
PRIVATE void	tty_do_write(TTY* p_tty);
PRIVATE void	put_key(TTY* p_tty, t_32 key);
//LHY
PRIVATE void	in_buffer(cmd_bufT* cmd_buf, TTY* p_tty);

/*global var within the file*/
int cmdflag = 0;

/*======================================================================*
                           task_tty
 *======================================================================*/
PUBLIC void task_tty()
{
	TTY*	p_tty;
	cmd_bufT* cmd_buf;

	init_keyboard();

	cmd_buf = cmd_buffers;
	
	for (p_tty=TTY_FIRST;p_tty<TTY_END;p_tty++) {
		init_tty(p_tty);
	}

	select_console(0);
	
	//This is a code for shell embeded in tty
	printf("shell# ");
	while (1) 
	{
		if(cmdflag&&!parrot)
		{
			printf("shell# ");
			cmdflag=0;
		}
		for (p_tty=TTY_FIRST;p_tty<TTY_END;p_tty++) {
			tty_do_read(p_tty);
			in_buffer(cmd_buf, p_tty);
			tty_do_write(p_tty);
		}
	}
}

/*======================================================================*
                           init_tty
 *======================================================================*/
PRIVATE void init_tty(TTY* p_tty)
{
	p_tty->inbuf_count = 0;
	p_tty->p_inbuf_head = p_tty->p_inbuf_tail = p_tty->in_buf;

	init_screen(p_tty);
}


/*======================================================================*
                           in_process
 *======================================================================*/
//将解析出来的key分情况放入tty缓冲区或者进行其他操作
PUBLIC void in_process(TTY* p_tty, t_32 key)
{
	if (!(key & FLAG_EXT)) {
		put_key(p_tty, key);
	}
	else {
		int raw_code = key & MASK_RAW;
		switch(raw_code) {
		case ENTER:
			put_key(p_tty, '\n');
			break;
		case BACKSPACE:
			put_key(p_tty, '\b');
			break;
		case UP:
			if ((key & FLAG_SHIFT_L) || (key & FLAG_SHIFT_R)) {	/* Shift + Up */
				scroll_screen(p_tty->p_console, SCROLL_SCREEN_UP);
			}
			break;
		case DOWN:
			if ((key & FLAG_SHIFT_L) || (key & FLAG_SHIFT_R)) {	/* Shift + Down */
				scroll_screen(p_tty->p_console, SCROLL_SCREEN_DOWN);
			}
			break;
		case F1:
		case F2:
		case F3:
		case F4:
		case F5:
		case F6:
		case F7:
		case F8:
		case F9:
		case F10:
		case F11:
		case F12:
			if ((key & FLAG_ALT_L) || (key & FLAG_ALT_R)) {	/* Alt + F1~F12 */
				select_console(raw_code - F1);
			}
			break;
		default:
			break;
		}
	}
}


/*======================================================================*
                              put_key
*======================================================================*/
//把一个32位的key放到tty的buffer里面去
PRIVATE void put_key(TTY* p_tty, t_32 key)
{
	if (p_tty->inbuf_count < TTY_IN_BYTES) {
		*(p_tty->p_inbuf_head) = key;
		p_tty->p_inbuf_head++;
		if (p_tty->p_inbuf_head == p_tty->in_buf + TTY_IN_BYTES) {
			p_tty->p_inbuf_head = p_tty->in_buf;
		}
		p_tty->inbuf_count++;
	}
}

/**
 * @author LiHuiying(youwillwin)
 * This function actually implements a similar function like 
 * 'scanf()';
 * This function also helps to implements Linux command 'cat'.
 */
PRIVATE void in_buffer(cmd_bufT* cmd_buf, TTY* p_tty)
{	
	int i,j,k;
	if(p_tty->inbuf_count)
	{
		if(*(p_tty->p_inbuf_tail)=='\b')
		{
			if(cmd_buf->count>0)
			{
				cmd_buf->count--;
				cmd_buf->buffer[cmd_buf->count]=0;
			}
			else
			{
				p_tty->inbuf_count--;
				if(p_tty->p_inbuf_head == p_tty->in_buf)
				{
					p_tty->p_inbuf_head=p_tty->in_buf+TTY_IN_BYTES;
				}
				p_tty->p_inbuf_head--;
				*(p_tty->p_inbuf_head)=0;
			}
		}
		else
		{
			cmd_buf->buffer[cmd_buf->count] = *(p_tty->p_inbuf_tail);
		
	//		printf("move...%c", cmd_buf->buffer[cmd_buf->count]);
			if(cmd_buf->buffer[cmd_buf->count]=='\n'||cmd_buf->count>=80)
			{
				if(parrot)
				{
					if(cmd_buf->count==1&&cmd_buf->buffer[0]=='q')
					{
						parrot=0;
						cmdflag=1;
						//clean cmd buffer
						for(i = 0; i < 81; i++)
						{
							cmd_buf->buffer[i]=0;
						}
						cmd_buf->count=0;
					}
					else
					{
						cmdflag=0;
						printf("\n");
						for(k=0; k < cmd_buf->count; k++)
						{
							//out_char(&tty_table[proc_table[0].nr_tty], cmd_buf->buffer[k]);
							printf("%x", &cmd_buf->buffer[k]);
						}
						printf("\n");
						//clean cmd buffer
						for(i = 0; i < 81; i++)
						{
							cmd_buf->buffer[i]=0;
						}
						cmd_buf->count=0;
					}
				}
				else
				{
					shell(cmd_buf);
			
				/**
				 * Test buffer read
				 *
					printf("Test buffer: ");
					for(j=0; j<cmd_buf->count; j++)
						printf("%c",cmd_buf->buffer[j]);
					printf("  ");
				 */
					cmdflag=1;
					//clean cmd buffer
					for(i = 0; i < 81; i++)
					{
						cmd_buf->buffer[i]=0;
					}
					cmd_buf->count=0;
				}
			}
			else
				cmd_buf->count++; 
		}
	}
}

/*======================================================================*
                              tty_do_read
*======================================================================*/
//判断是不是在当前控制台
PRIVATE void tty_do_read(TTY* p_tty)
{
	if (is_current_console(p_tty->p_console)) {
		keyboard_read(p_tty);
	}
}


/*======================================================================*
                              tty_do_write
*======================================================================*/
PRIVATE void tty_do_write(TTY* p_tty)
{
	if (p_tty->inbuf_count) {
		char ch = *(p_tty->p_inbuf_tail);
		p_tty->p_inbuf_tail++;
		if (p_tty->p_inbuf_tail == p_tty->in_buf + TTY_IN_BYTES) {
			p_tty->p_inbuf_tail = p_tty->in_buf;
		}
		p_tty->inbuf_count--;
		
		//disp_int(ch);
		out_char(p_tty->p_console, ch);
	}
}


/*======================================================================*
                              tty_write
*======================================================================*/
PUBLIC void tty_write(TTY* p_tty, char* buf, int len)
{
	char* p = buf;
	int i = len;

	while (i) {
		out_char(p_tty->p_console, *p++);
		i--;
	}
}


/*======================================================================*
                              sys_write
*======================================================================*/
PUBLIC int sys_write(char* buf, int len, PROCESS* p_proc)
{
	tty_write(&tty_table[p_proc->nr_tty], buf, len);
	return 0;
}


