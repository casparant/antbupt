
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                            global.c
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                    Forrest Yu, 2005
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#define GLOBAL_VARIABLES_HERE

#include <global.h>
#include <proto.h>


PUBLIC	PROCESS	proc_table[NR_TASKS + NR_PROCS];

PUBLIC	TASK	task_table[NR_TASKS] = {{task_tty, STACK_SIZE_TTY, "tty"}};
PUBLIC	TASK	user_proc_table[NR_PROCS] = {	{TestA, STACK_SIZE_TESTA, "TestA"},
						{TestB, STACK_SIZE_TESTB, "TestB"},
						{TestC, STACK_SIZE_TESTC, "TestC"},
						{TestD, STACK_SIZE_TESTD, "TestD"},
						{TestE, STACK_SIZE_TESTE, "TestE"},
						{TestF, STACK_SIZE_TESTF, "TestF"},
						{TestG, STACK_SIZE_TESTG, "TestG"},
						};

PUBLIC	char	task_stack[STACK_SIZE_TOTAL];
/* @author: Li Huiying */
PUBLIC	int 	heap[HEAP_SIZE];
PUBLIC	t_32 	heap_is_used[HEAP_SIZE];
PUBLIC	char*	heap_ptr;
PUBLIC	char*	heap_head;
/* END of Li Huiying */

PUBLIC	TTY		tty_table[NR_CONSOLES];
PUBLIC	CONSOLE	console_table[NR_CONSOLES];

/* @author: Li Huiying */
PUBLIC	cmd_bufT	cmd_buffers[1];
PUBLIC	int 		adopted[NR_TASKS+NR_PROCS];
PUBLIC	t_32		l1[NR_TASKS+NR_PROCS];
PUBLIC	t_32		l2[NR_TASKS+NR_PROCS];
PUBLIC	t_32		l3[NR_TASKS+NR_PROCS];
PUBLIC	int			parrot;
/* END of Li Huiying */

PUBLIC t_8					buf[512];
PUBLIC t_8					current_DOR = 0x0C;
PUBLIC struct request 		request={READ, 0, 18, 1, buf};

PUBLIC	t_pf_irq_handler	irq_table[NR_IRQ];
PUBLIC	t_sys_call			sys_call_table[NR_SYS_CALL] = {sys_get_ticks, sys_write};
