/*****************************************************************************
                                      floppy.c
 *****************************************************************************
                                            Caspar Zhang : casparant@gmail.com
*****************************************************************************/

#include <global.h>
#include <proto.h>
#include "string.h"

PRIVATE int recalibrate = 0;			// 是否重新校正磁头
PRIVATE int reset = 0;					// 是否需要归位磁头
PRIVATE int seek = 0;					// 是否需要寻道
PRIVATE t_8 cur_spec = -1;				// 当前的磁盘属性
PRIVATE t_8 cur_rate = -1;				// 当前的磁盘传输速率
PRIVATE unsigned char cur_cylinder = 255;	// 当前柱面号
PRIVATE unsigned char sector = 0;		// 扇区号
PRIVATE unsigned char head = 0;			// 磁头号
PRIVATE unsigned char cylinder = 0;		// 柱面号
PRIVATE unsigned char seek_cylinder = 0;	// 寻道至某柱面号
PRIVATE unsigned char command = 0;		// 读写命令
PRIVATE unsigned char reply_buffer[MAX_REPLIES];	// 写数据时的缓冲区
PRIVATE struct floppy_struct floppy = {2880, 18, 2, 80, 0, 0x1B, 0x00, 0xCF};

PRIVATE void setup_DMA(void);
PRIVATE void output_byte(char byte);
PRIVATE int result(void);
PRIVATE void setup_rw_floppy(void);
PRIVATE void transfer(void);
PRIVATE void seek_floppy(void);
PRIVATE void reset_floppy(void);
PRIVATE void recalibrate_floppy(void);

PRIVATE void reset_interrupt(void);
PRIVATE void rw_interrupt(void);
PRIVATE void bad_flp_intr(void);
PRIVATE void seek_interrupt(void);
PRIVATE void recal_interrupt(void);

PUBLIC void floppy_init(void)
{
	reset_floppy();
	recalibrate_floppy();
	floppy_on();
	do_fd_request();
}

//current_DOR = 0x0C, 即00001100, 允许DMA和中断请求， 允许FDC工作
PUBLIC void floppy_on(void)
{
	unsigned int mask = 0x10;		// MOT_EN0位置1，软驱A得以启动

	disable_int();
	mask |= current_DOR;			// mask = 00011100
	if (mask != current_DOR)		// 如果没有启动
	{
		out_byte(FD_DOR, mask);
		current_DOR = mask;
	}
	enable_int();
}

//curent_DOR = 0x1C
PUBLIC void floppy_off(void)
{
	unsigned int mask = ~0x10;		// MOT_EN0位重置，软驱A得以关闭
	
	disable_int();
	mask &= current_DOR;			// mask = 00001100
	if (mask != current_DOR)		// 如果没有关闭
	{
		out_byte(FD_DOR, mask);
		current_DOR = mask;
	}
	enable_int();
}

PRIVATE void output_byte(char byte)
{
	int counter;
	unsigned char status;

	if (reset)
		return;
		
	for(counter = 0 ; counter < 10000 ; counter++) 
	{
		status = in_byte(FD_STATUS) & (STATUS_READY | STATUS_DIR);
		if (status == STATUS_READY) 
		{
			out_byte(FD_DATA, byte);
			return;
		}
	}
	// 如果出错
	reset = 1;
	printf("Unable to send byte to FDC\n");
}

PUBLIC void do_fd_request(void)
{
	unsigned int block;

	seek = 0;							// 不需要寻道
	if (reset)							// 重置并结束
	{
		reset_floppy();
		return;
	}
	if (recalibrate) 					// 校准磁头并结束
	{
		recalibrate_floppy();
		return;
	}

	/* 计算CHS,需要移动到的柱面，和读/写命令 */
	block = request.sector;
	sector = block % floppy.sect + 1;
	block /= floppy.sect;
	head = block % floppy.head;
	cylinder = block / floppy.head;	
	seek_cylinder = cylinder << floppy.stretch;
	if(seek_cylinder != cur_cylinder)	// 如果需要seek的柱面非当前柱面，则寻道标记为1
		seek = 1;
	
	if (request.cmd == READ)
		command = FD_READ;
	else if (request.cmd == WRITE)
		command = FD_WRITE;
	else
		printf("do_fd_request: unknown command");
		
	transfer();
}

PRIVATE void transfer(void)
{
	// spec = 0xCF ; 1100 1111, 高四位为步进速率,低四位为磁头卸载时间
	// 如果没有设定好磁盘参数，那么开始设定
	if (cur_spec != floppy.spec)
	{
		cur_spec = floppy.spec;
		output_byte(FD_SPECIFY);	// 发送设定命令
		output_byte(cur_spec);		// 写入参数,步进速率12*2ms; 磁头卸载时间15*32=480ms
		output_byte(0x03<<1 | 0x0);	// 磁头在DMA模式下装载时间为3*4=12ms
	}
	
	// rate = 0x00; 0x00 表示1.44MB的标准速度500KB/s
	if (cur_rate != floppy.rate)
	{
		cur_rate = floppy.rate;
		out_byte(FD_DCR, cur_rate);
	}
	
	//如果在out_byte函数中出错，那么重新发出请求
	if (reset) 
	{
		do_fd_request();
		return;
	}
	
	//如果不需要寻道
	if (!seek)
	{
		setup_rw_floppy();				// 开始设置DMA，发送各种读写参数
		return;
	}
	seek_interrupt();
	
	if (seek_cylinder)		 			// 需要寻道，如果柱面号不为0则开始寻道
		seek_floppy();
	else								// 如果柱面号为0，则重新校正
		recalibrate_floppy();
}

PRIVATE void setup_rw_floppy(void)
{
	setup_DMA();				//初始化DMA控制器
	output_byte(command);		//发送读写命令
	output_byte(0x00);			//发送驱动器序号信息
	output_byte(cylinder);		//发送柱面号
	output_byte(head);			//发送磁头号
	output_byte(sector);		//发送起始扇区号
	output_byte(2);				//发送扇区字节数信息,512B/Sec
	output_byte(floppy.sect);	//发送扇区数量信息
	output_byte(floppy.gap);	//扇区间隔长度信息, 0x1B
	output_byte(0xFF);			//N=0时扇区大小,此处无用
	rw_interrupt();
	if (reset)
		do_fd_request();
}

PRIVATE void setup_DMA(void)
{
	long addr = (long) request.buffer;	// 存放buffer的入口地址
	
	disable_int();
	
	if (addr >= 0x100000)				// 如果超出1M的地址
	{
		addr = (long) tmp_floppy_area;	// 那就在临时区域
		if (command == FD_WRITE)		// 如果要往盘里写，先把buffer内容拷到临时区域
			memcpy(tmp_floppy_area, request.buffer, 512);
	}
	
	out_byte(0x0A, 0x04|0x02);
	if (command == FD_READ)
	{
		out_byte(0x0C, DMA_READ);
		out_byte(0x0B, DMA_READ);
	}
	else
	{
		out_byte(0x0C, DMA_WRITE);
		out_byte(0x0B, DMA_WRITE);
	}
	out_byte(0x04, addr);
	addr >>= 8;
	out_byte(0x04, addr);
	addr >>= 8;
	out_byte(0x81, addr);
	out_byte(0x05, 0xFF);
	out_byte(0x05, 0x03);
	out_byte(0x0A, 0x00|0x02);

	enable_int();
}


/* 这里的reply_buffer[7]为7个结果位，in_byte(FD_DATA)操作为从FD_DATA中不断获取结果位*/
PRIVATE int result(void)
{
	int i = 0, counter, status;

	if (reset)
		return -1;
		
	for (counter = 0 ; counter < 10000 ; counter++) 
	{
		status = in_byte(FD_STATUS)&(STATUS_DIR|STATUS_READY|STATUS_BUSY);
		if (status == STATUS_READY)
			return i;
		if (status == (STATUS_DIR|STATUS_READY|STATUS_BUSY)) //获取结果位
		{
			if (i >= MAX_REPLIES)
				break;
			reply_buffer[i++] = in_byte(FD_DATA);
		}
	}
	// 如果出错
	reset = 1;
	printf("Getstatus times out\n");
	return -1;
}

/* 寻道 */
PRIVATE void seek_floppy(void)
{
	seek_cylinder = 0;				// 寻道标记
	output_byte(FD_SEEK);			// 发送寻道命令
	output_byte(head<<2 | 0x00);	// 低三位，1位磁头号，2位驱动器号
	output_byte(seek_cylinder);		// 柱面号
	if (reset)						// 如果出错
		do_fd_request();
}

/* 重新校准磁头 */
PRIVATE void recalibrate_floppy(void)
{
	recalibrate = 0;
	cur_cylinder = 0;
	recal_interrupt();
	output_byte(FD_RECALIBRATE);	// 重新校正
	output_byte(0x00);				// 当前驱动器号
	if (reset)						// 如果出错
		do_fd_request();
}


/*
 * reset is done by pulling bit 2 of DOR low for a while.
 */
PRIVATE void reset_floppy(void)
{
	int i;

	reset = 0;
	cur_spec = -1;							// 重置属性
	cur_rate = -1;							// 重置速率
	recalibrate = 1;						// 需要重校

	disable_int();
	out_byte(FD_DOR, current_DOR&~0x04);	// ~0x04=11111011,为0的这一位表示复位。
	for (i=0 ; i<1000 ; i++)				// 一个for a while
		__asm__("nop");
	out_byte(FD_DOR, current_DOR);			// 重新置位
	enable_int();
}

PRIVATE void rw_interrupt(void)
{
	if (result() != 7 || (ST0 & 0xb8) || (ST1 & 0xbf) || (ST2 & 0x73))
	{
		if (ST1 & 0x02) 
		{
			printf("Drive is write protected\n");
			reset = 1;
			floppy_off();
		}
		else
			bad_flp_intr();
		do_fd_request();
		return;
	}
	if (command == FD_READ && (unsigned long)(request.buffer) >= 0x100000)
		memcpy(request.buffer, tmp_floppy_area, 512);
}

PRIVATE void bad_flp_intr(void)
{
	request.errors++;
	if (request.errors > MAX_ERRORS) 
		floppy_off();
	if (request.errors > MAX_ERRORS/2)
		reset = 1;
	else
		recalibrate = 1;
}

/*
 * This is the routine called after every seek (or recalibrate) interrupt
 * from the floppy controller. Note that the "unexpected interrupt" routine
 * also does a recalibrate, but doesn't come here.
 */
PRIVATE void seek_interrupt(void)
{
/* sense drive status */
	output_byte(FD_SENSEI);
	if (result() != 2 || (ST0 & 0xF8) != 0x20 || ST1 != seek_cylinder) 
	{
		bad_flp_intr();
		do_fd_request();
		return;
	}
	cur_cylinder = ST1;
	setup_rw_floppy();
}

/*
 * Special case - used after a unexpected interrupt (or reset)
 */
PRIVATE void recal_interrupt(void)
{
	output_byte(FD_SENSEI);
	if (result()!=2 || (ST0 & 0xE0) == 0x60)
		reset = 1;
	else
		recalibrate = 0;
	do_fd_request();
}

PRIVATE void reset_interrupt(void)
{
	output_byte(FD_SENSEI);
	(void) result();
	output_byte(FD_SPECIFY);
	output_byte(cur_spec);		/* hut etc */
	output_byte(6);			/* Head load time =6ms, DMA */
	do_fd_request();
}
