
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                               proc.c
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                    Forrest Yu, 2005
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#include "type.h"
#include "const.h"
#include "protect.h"
#include "string.h"
#include "proc.h"
#include "tty.h"
#include "console.h"
#include "global.h"
#include "proto.h"


/*======================================================================*
                              schedule
 *======================================================================*/
PUBLIC void schedule()
{
	PROCESS*	p;
	int		greatest_ticks = 0;

	while (!greatest_ticks) {
	//	printf("Total:  %x \n", eprocs);
		for (p=proc_table; p<proc_table+NR_TASKS+NR_PROCS; p++) {
//			printf("pid: %x  ticks: %x   ",  p->pid, p->ticks);  
			if (p->ticks > greatest_ticks) {
				greatest_ticks = p->ticks;
				p_proc_ready = p;
			//	disp_str("**test");
			//	for(i=0; i<5; i++)
				//	printf("%x", p->pid);
			}
		}
//		printf("Selected****: %x \n", p_proc_ready->pid);

		if (!greatest_ticks) {
			for (p=proc_table; p<proc_table+NR_TASKS+NR_PROCS; p++) {
				p->ticks = p->priority;
			//	disp_str("$$re");
			}
		}
	}
}


/*======================================================================*
                           sys_get_ticks
 *======================================================================*/
PUBLIC int sys_get_ticks()
{
	return ticks;
}

