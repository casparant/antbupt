/**
 * @author LiHuiying(Youwillwin)
 *
 */
#include "type.h"
#include "tty.h"
#include "proc.h"
#include "global.h"
#include "proto.h"

/**
 * Function that supported by the shell
 */
void version();
void unsupported_cmd_handler();
void help();
void ps();
int  exec();

int strcmp(char* str1, t_32* str2, int len1, int len2)
{
	int i;
//	str2 = (char*)str2;
	if(len1 != len2)
	{
	//	printf("Length no equal, len1=%x, len2=%x   ", len1, len2);
		return 1;
	}
	else
	{
		for(i = 0; i < len1; i++)
			if(str1[i]!=str2[i])
			{
	//			printf("Char not equal, 1[%x]=%c, 2[%x]=%c    ", i, str1[i], i, str2[i]);
				return 1;
			}
		return 0;
	}
}

void shell(cmd_bufT* cmd)
{
	int i;
//	printf("Response: ");
	if(!strcmp("version", cmd->buffer, 7, cmd->count))
	{
		version();
	}
	else if(!strcmp("help", cmd->buffer, 4, cmd->count))
	{
		help();
	}
	else if(!strcmp("ps", cmd->buffer, 2, cmd->count))
	{
		ps();
	}
	else if(!strcmp("exec", cmd->buffer, 4, cmd->count))
	{
		exec();
	}
	else
	{
	/**
	 * Buffer test code
		printf("Shell buf test: ");
		for(i = 0; i < cmd->count; i++)
			printf("%c", cmd->buffer[i]);
	 */
	 
		unsupported_cmd_handler();
	}
}

void version()
{
	printf("\nversion: LiHuiying0.0.1\n");
}

void unsupported_cmd_handler()
{
	printf("\nOpps! I don't recongnize it~~~ please retry :-)\n");
}

void help()
{
}

void ps()
{
	int i,j;
	
	printf("\nExisting user processes:\n");
	printf("   pid   name      priority\n");
	for(i = 0; i < NR_TASKS+NR_PROCS; i++)
	{
		printf("   %x      ", proc_table[i].pid);
		for(j=0;j<strlen(proc_table[i].name); j++)
		{
			printf("%c", proc_table[i].name[j]);	
		}
		printf("        %x", proc_table[i].priority);
		printf("\n");
	}
//	printf("===========End PS\n");
}

int exec()
{
	TASK* p_task;
	t_8		privilege;
	t_8		rpl;
	int		eflags;
	t_16	selector_ldt	= SELECTOR_LDT_FIRST;
	char*	p_task_stack	= task_stack + STACK_SIZE_TOTAL;
	
	p_task = user_proc_table + (pid_counter - NR_TASKS); 
	privilege = PRIVILEGE_USER; 
	rpl	= RPL_USER; 
	eflags = 0x202;	//IF=1, bit 2 is always 1 

	strcpy(p_proc->name, p_task->name);	/* name of the process */
	p_proc->pid	= pid_counter;			/* pid */
	
	p_proc->ldt_sel	= selector_ldt;
	memcpy(&p_proc->ldts[0], &gdt[SELECTOR_KERNEL_CS >> 3], sizeof(DESCRIPTOR));
	p_proc->ldts[0].attr1 = DA_C | privilege << 5;	/* change the DPL */
	memcpy(&p_proc->ldts[1], &gdt[SELECTOR_KERNEL_DS >> 3], sizeof(DESCRIPTOR));
	p_proc->ldts[1].attr1 = DA_DRW | privilege << 5;/* change the DPL */
	p_proc->regs.cs		= ((8 * 0) & SA_RPL_MASK & SA_TI_MASK) | SA_TIL | rpl;
	p_proc->regs.ds		= ((8 * 1) & SA_RPL_MASK & SA_TI_MASK) | SA_TIL | rpl;
	p_proc->regs.es		= ((8 * 1) & SA_RPL_MASK & SA_TI_MASK) | SA_TIL | rpl;
	p_proc->regs.fs		= ((8 * 1) & SA_RPL_MASK & SA_TI_MASK) | SA_TIL | rpl;
	p_proc->regs.ss		= ((8 * 1) & SA_RPL_MASK & SA_TI_MASK) | SA_TIL | rpl;
	p_proc->regs.gs		= (SELECTOR_KERNEL_GS & SA_RPL_MASK) | rpl;
	p_proc->regs.eip	= (t_32)p_task->initial_eip;
	p_proc->regs.esp	= (t_32)p_task_stack;
	p_proc->regs.eflags	= eflags;

	p_proc->nr_tty		= 0;

	p_task_stack -= p_task->stacksize;
	p_proc++;
	p_task++;
	
	eprocs++;
	
	selector_ldt += 1 << 3;
	proc_table[1].ticks = proc_table[1].priority =  5;
}

