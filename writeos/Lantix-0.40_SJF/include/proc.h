
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                               proc.h
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                    Forrest Yu, 2005
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
#include "protect.h"

typedef struct s_stackframe {	/* proc_ptr points here				¡ü Low			*/
	t_32	gs;		/* ©·						©¦			*/
	t_32	fs;		/* ©§						©¦			*/
	t_32	es;		/* ©§						©¦			*/
	t_32	ds;		/* ©§						©¦			*/
	t_32	edi;		/* ©§						©¦			*/
	t_32	esi;		/* ©Ç pushed by save()				©¦			*/
	t_32	ebp;		/* ©§						©¦			*/
	t_32	kernel_esp;	/* <- 'popad' will ignore it			©¦			*/
	t_32	ebx;		/* ©§						¡üÕ»´Ó¸ßµØÖ·ÍùµÍµØÖ·Ôö³¤*/
	t_32	edx;		/* ©§						©¦			*/
	t_32	ecx;		/* ©§						©¦			*/
	t_32	eax;		/* ©¿						©¦			*/
	t_32	retaddr;	/* return address for assembly code save()	©¦			*/
	t_32	eip;		/*  ©·						©¦			*/
	t_32	cs;		/*  ©§						©¦			*/
	t_32	eflags;		/*  ©Ç these are pushed by CPU during interrupt	©¦			*/
	t_32	esp;		/*  ©§						©¦			*/
	t_32	ss;		/*  ©¿						©ÛHigh			*/
}STACK_FRAME;


typedef struct s_proc {
	STACK_FRAME			regs;			/* process' registers saved in stack frame */

	t_16				ldt_sel;		/* selector in gdt giving ldt base and limit*/
	DESCRIPTOR			ldts[LDT_SIZE];		/* local descriptors for code and data */
								/* 2 is LDT_SIZE - avoid include protect.h */
	int				ticks;			/* remained ticks */
	int				priority;
	t_32				pid;			/* process id passed in from MM */
	char				name[16];		/* name of the process */

	int				nr_tty;
	/**
	 * @author LiHuiying(youwillwin)
	 */
	int isOver;
}PROCESS;


typedef struct s_task {
	t_pf_task	initial_eip;
	int		stacksize;
	char		name[32];
}TASK;


/* Number of tasks & processes */


//yc_temp
#define NR_TASKS   1
#define NR_PROCS   10

/* stacks of tasks */
//////////////////////////////////////////////////////////////////////////yc_temp
//10 tasks A-->J
#define STACK_SIZE_TTY		0x8000
#define STACK_SIZE_TESTA	0x8000
#define STACK_SIZE_TESTB	0x8000
#define STACK_SIZE_TESTC	0x8000
#define STACK_SIZE_TESTD	0x8000
#define STACK_SIZE_TESTE	0x8000
#define STACK_SIZE_TESTF	0x8000
#define STACK_SIZE_TESTG	0x8000
#define STACK_SIZE_TESTH	0x8000
#define STACK_SIZE_TESTI	0x8000
#define STACK_SIZE_TESTJ	0x8000


#define STACK_SIZE_TOTAL	(STACK_SIZE_TTY + \
				STACK_SIZE_TESTA + \
				STACK_SIZE_TESTB + \
				STACK_SIZE_TESTC + \
				STACK_SIZE_TESTD + \
				STACK_SIZE_TESTE + \
				STACK_SIZE_TESTF + \
				STACK_SIZE_TESTG + \
				STACK_SIZE_TESTH + \
				STACK_SIZE_TESTI + \
				STACK_SIZE_TESTJ)
///////////////////////////////////////////////////////////////////////////////

/**
 * @author LiHuiying(Youwillwin)
 */
int etasks;
int eprocs;

PROCESS* p_proc;
int pid_counter;
