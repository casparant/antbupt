
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                            global.c
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                    Forrest Yu, 2005
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#define GLOBAL_VARIABLES_HERE

#include "type.h"
#include "const.h"
#include "protect.h"
#include "proc.h"
#include "tty.h"
#include "console.h"
#include "global.h"
#include "proto.h"


PUBLIC	PROCESS	proc_table[NR_TASKS + NR_PROCS];

PUBLIC	TASK	task_table[NR_TASKS] = {{task_tty, STACK_SIZE_TTY, "tty"}};

//////////////////////////////////////////////////////////////////////////////////////////yc_temp
PUBLIC	TASK	user_proc_table[NR_PROCS] = {
                        {TestA, STACK_SIZE_TESTA, "TestA"},
						{TestB, STACK_SIZE_TESTB, "TestB"},
						{TestC, STACK_SIZE_TESTC, "TestC"},
						{TestD, STACK_SIZE_TESTD, "TestD"},
                        {TestE, STACK_SIZE_TESTE, "TestE"},
						{TestF, STACK_SIZE_TESTF, "TestF"},
						{TestG, STACK_SIZE_TESTG, "TestG"},
						{TestH, STACK_SIZE_TESTH, "TestH"},
                        {TestI, STACK_SIZE_TESTI, "TestI"},
						{TestJ, STACK_SIZE_TESTJ, "TestJ"}
						};
////////////////////////////////////////////////////////////////////////////////////////////


PUBLIC	char	task_stack[STACK_SIZE_TOTAL];

PUBLIC	TTY			tty_table[NR_CONSOLES];
PUBLIC	CONSOLE			console_table[NR_CONSOLES];

//LHY
PUBLIC 	cmd_bufT cmd_buffers[1];

PUBLIC	t_pf_irq_handler	irq_table[NR_IRQ];

PUBLIC	t_sys_call		sys_call_table[NR_SYS_CALL] = {sys_get_ticks, sys_write};
