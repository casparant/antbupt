
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                            main.c
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                    Forrest Yu, 2005
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#include "type.h"
#include "const.h"
#include "protect.h"
#include "string.h"
#include "proc.h"
#include "tty.h"
#include "console.h"
#include "global.h"
#include "proto.h"


/*======================================================================*
                            tinix_main
 *======================================================================*/
PUBLIC int tinix_main()
{
	disp_str("-----\"tinix_main\" begins-----\n");
//	disp_str("MemorySize: ");
	//DispMemSize();


	TASK*		p_task;
    PROCESS*	p_proc		= proc_table;
	char*		p_task_stack	= task_stack + STACK_SIZE_TOTAL;
	t_16		selector_ldt	= SELECTOR_LDT_FIRST;
	int		i;
	t_8		privilege;
	t_8		rpl;
	int		eflags;

	/**
	 * @author LiHuiying
	 */
	for(i=0;i<NR_TASKS+NR_PROCS;i++){
//	for(i=0;i<NR_TASKS;i++){
/////////////////////////////////////////////////////////////////////////////
		if (i < NR_TASKS) {	/* ÈÎÎñ */
			p_task		= task_table + i;
			privilege	= PRIVILEGE_TASK;
			rpl		= RPL_TASK;

			eflags		= 0x1202;	/* IF=1, IOPL=1, bit 2 is always 1 */
		}
		else {			/* ÓÃ»§½ø³Ì */
			p_task		= user_proc_table + (i - NR_TASKS);
			privilege	= PRIVILEGE_USER;
			rpl		= RPL_USER;
			eflags		= 0x202;	/* IF=1, bit 2 is always 1 */
		}
/////////////////////////////////////////////////////////////////////////////


		strcpy(p_proc->name, p_task->name);	/* name of the process */
		p_proc->pid	= i;			/* pid */

		p_proc->ldt_sel	= selector_ldt;
		memcpy(&p_proc->ldts[0], &gdt[SELECTOR_KERNEL_CS >> 3], sizeof(DESCRIPTOR));
		p_proc->ldts[0].attr1 = DA_C | privilege << 5;	/* change the DPL */
		memcpy(&p_proc->ldts[1], &gdt[SELECTOR_KERNEL_DS >> 3], sizeof(DESCRIPTOR));
		p_proc->ldts[1].attr1 = DA_DRW | privilege << 5;/* change the DPL */
		p_proc->regs.cs		= ((8 * 0) & SA_RPL_MASK & SA_TI_MASK) | SA_TIL | rpl;
		p_proc->regs.ds		= ((8 * 1) & SA_RPL_MASK & SA_TI_MASK) | SA_TIL | rpl;
		p_proc->regs.es		= ((8 * 1) & SA_RPL_MASK & SA_TI_MASK) | SA_TIL | rpl;
		p_proc->regs.fs		= ((8 * 1) & SA_RPL_MASK & SA_TI_MASK) | SA_TIL | rpl;
		p_proc->regs.ss		= ((8 * 1) & SA_RPL_MASK & SA_TI_MASK) | SA_TIL | rpl;
		p_proc->regs.gs		= (SELECTOR_KERNEL_GS & SA_RPL_MASK) | rpl;
		p_proc->regs.eip	= (t_32)p_task->initial_eip;
		p_proc->regs.esp	= (t_32)p_task_stack;
		p_proc->regs.eflags	= eflags;

		p_proc->nr_tty		= 0;
		p_proc->isOver = 0;//0 to sign process isn't over, and 1 for over

		p_task_stack -= p_task->stacksize;
		p_proc++;
		p_task++;
		selector_ldt += 1 << 3;
	}//end of for
//////////////////////////////////////////////////////////////yc_temp
	proc_table[0].ticks = proc_table[0].priority = 1;//tty


	proc_table[1].ticks = proc_table[1].priority =  45;//A
	proc_table[2].ticks = proc_table[2].priority =  25;//B
	proc_table[3].ticks = proc_table[3].priority =  35;//C
    proc_table[4].ticks = proc_table[4].priority = 23;//D
	proc_table[5].ticks = proc_table[5].priority =  15;//E
	proc_table[6].ticks = proc_table[6].priority =  10;//F
	proc_table[7].ticks = proc_table[7].priority =  5;//G
	proc_table[8].ticks = proc_table[8].priority =  19;//H
	proc_table[9].ticks = proc_table[9].priority =  14;//I

    proc_table[10].ticks = proc_table[10].priority =  21;//J

    proc_table[0].nr_tty = 0;
	proc_table[1].nr_tty = 0;
	proc_table[2].nr_tty = 0;
	proc_table[3].nr_tty = 0;
	proc_table[4].nr_tty = 0;
	proc_table[5].nr_tty = 0;
	proc_table[6].nr_tty = 0;
    proc_table[7].nr_tty = 0;
	proc_table[8].nr_tty = 0;
	proc_table[9].nr_tty = 0;

	proc_table[10].nr_tty = 0;
////////////////////////////////////////////////////////////////////

	k_reenter	= 0;


	ticks		= 0;

	p_proc_ready	= proc_table + 1;

	init_clock();

	restart();

	while(1){}
}


/*======================================================================*
                               TestA
 *======================================================================*/

/**
 * @author LiHuiying(Youwillwin)
 */
void TestA()
{
	disp_str("A begins->");
	milli_delay(500);
	disp_str("A ends ");

	proc_table[1].isOver=1;
//	delay(500);
	milli_delay(500);
//	TestB();

	//while(1){
	//	printf("<Ticks:%x>", get_ticks());
	//	printf("cmd #");
	//	scanf
      //disp_str("A");
      //milli_delay(2);

	//}

}


/*======================================================================*
                               TestB
 *======================================================================*/
void TestB()
{
	disp_str("B begins--->");
	milli_delay(300);
	disp_str("B ends...\n");
	proc_table[2].isOver=1;
    milli_delay(300);
//	while(1){disp_str("B");milli_delay(2000);}
 //   delay(300);
}


/*======================================================================*
                               TestC
 *======================================================================*/
void TestC()
{
	disp_str("C begins--->");
	milli_delay(300);
	disp_str("C ends...");
	proc_table[3].isOver=1;
//while(1){disp_str("C");  milli_delay(2);}
    milli_delay(300);
    //delay(300);
//	while(1){}
}

/*======================================================================*
                               TestD
 *======================================================================*/
void TestD()
{
	disp_str("D begins--->");
	milli_delay(300);
	disp_str("D ends...\n");
	proc_table[4].isOver=1;
  //  while(1){disp_str("D");  milli_delay(2);}
	milli_delay(300);
//	while(1){}
}
//////////////////////////////////////////////////////
/*======================================================================*
                               TestE
 *======================================================================*/
void TestE()
{
	disp_str("E begins--->");
	milli_delay(300);
	disp_str("E ends...");
	proc_table[5].isOver=1;
 //   while(1){disp_str("E");  milli_delay(2);}
	milli_delay(300);
//	while(1){}
}/*======================================================================*
                               TestF
 *======================================================================*/
void TestF()
{
	disp_str("F begins--->");
	milli_delay(300);
	disp_str("F ends...\n");
	proc_table[6].isOver=1;
//	while(1){disp_str("*");  milli_delay(2);}
	milli_delay(300);
//	while(1){}
}/*======================================================================*
                               TestG
 *======================================================================*/
void TestG()
{
	disp_str("G begins--->");
	milli_delay(300);
	disp_str("G ends...");
	proc_table[7].isOver=1;
//		while(1){disp_str("*");  milli_delay(2);}
	milli_delay(300);
//	while(1){}
}/*======================================================================*
                               TestH
 *======================================================================*/
void TestH()
{
	disp_str("H begins--->");
	milli_delay(300);
	disp_str("H ends...\n");
	proc_table[8].isOver=1;
//		while(1){disp_str("*");  milli_delay(2);}
	milli_delay(300);
//	while(1){}
}/*======================================================================*
                               TestI
 *======================================================================*/
void TestI()
{
	disp_str("I begins--->");
	milli_delay(300);
	disp_str("I ends...");
	proc_table[9].isOver=1;
//	while(1){disp_str("*");  milli_delay(2);}
	milli_delay(300);
//	while(1){}
}/*======================================================================*
                               TestJ
 *======================================================================*/
void TestJ()
{
	disp_str("J begins--->");
	milli_delay(300);
	disp_str("J ends...\n");
	proc_table[10].isOver=1;
//	while(1){disp_str("*");  milli_delay(2);}
	milli_delay(300);
//	while(1){}
}
//////////////////////////////////////////////////////
