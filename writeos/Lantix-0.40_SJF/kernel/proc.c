
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                               proc.c
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                    Forrest Yu, 2005
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#include "type.h"
#include "const.h"
#include "protect.h"
#include "string.h"
#include "proc.h"
#include "tty.h"
#include "console.h"
#include "global.h"
#include "proto.h"

/**
 * @author LiHuiying(Youwillwin)
 //////////////////////////////////////
 //Modified: Yangchao
 //////////////////////////////////////
 * This function implements fifo scheduling.
 */
 //FIFO
/*
PUBLIC void schedule()
{
	if(p_proc_ready==proc_table+NR_TASKS+NR_PROCS-1)
	{
		p_proc_ready = proc_table;
	}
	else
	{
		p_proc_ready++;
	}
}
*/

/*======================================================================*
                              schedule SJF
 *======================================================================*/

PUBLIC void schedule()
{
	PROCESS*	p;
	int		greatest_ticks = 0;

	while (!greatest_ticks) {
	//	printf("Total:  %x \n", eprocs);
		for (p=proc_table; p<proc_table+NR_TASKS+NR_PROCS; p++) {
//			printf("pid: %x  ticks: %x   ",  p->pid, p->ticks);
            if(p->isOver)continue;
			if (p->ticks > greatest_ticks) {
				greatest_ticks = p->ticks;
				p_proc_ready = p;
			//	disp_str("**test");
			//	for(i=0; i<5; i++)
				//	printf("%x", p->pid);
			}
		}

        disp_str("\nnow schedule:");
        disp_str(p_proc_ready->name);
        printf("%d",p_proc_ready->ticks);
        //disp_str(p_proc_ready->ticks);
        disp_str("\n");
//		printf("Selected****: %x \n", p_proc_ready->pid);

		if (!greatest_ticks) {
			for (p=proc_table; p<proc_table+NR_TASKS+NR_PROCS; p++) {
				p->ticks = p->priority;
				//p->isOver=0;
			//	disp_str("$$re");
			}
		}
	}
}

/*======================================================================*
                           sys_get_ticks
 *======================================================================*/
PUBLIC int sys_get_ticks()
{
	return ticks;
}

